﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitiafyTests.Models
{
    public class ContractorCompany
    {
        public string Name { get; internal set; }
        public string ContractPerson { get; internal set; }
        public string CompanyCode { get; internal set; }
        public string ContractorManagerEmail { get; internal set; }
    }
}
