﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Further lisences tests")]
    internal class LicenseTests2 : BaseTests
    {
        private string companyCode = "";

        public LicenseTests2(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "10. Licenses Test 2";

        public override void RunAll()
        {
            this.LoginAsSA();
            this.AddNewWorksite();
            this.SearchForCompany();
            this.AddCompanyAdmin();
            this.LoginForBothCompanyAdminAccounts();
            this.AddPreRegUser();
            this.AddContractCompany();
            this.ReRegistration();
            this.SetLicenceLimit();
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public new void LoginAsSA()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }

        [Test]
        [Description("Creates a new worksite")]
        public void AddNewWorksite()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();

            companiesPage.CreateCompany2("Test 321");
        }

        [Test]
        [Description("Searchs for company and clicks to go through to dashboard")]
        public void SearchForCompany()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany2(_testConfig.CompanyName);
        }

        [Test]
        [Description("Adds CA to second company")]
        public void AddCompanyAdmin()
        {
            var dashboard = TestFactory.New<Dashboard>();

            dashboard.GoToUsers();
            var preRegUsersPage = dashboard.GoToPreRegTab();

            preRegUsersPage.AddUser("Shane", "Cleary", _testConfig.CompanyAdminEmail, "admin");

            dashboard.Logout();
        }

        [Test]
        [Description("Login as admin, verify CA can see both companies listed")]
        public void LoginForBothCompanyAdminAccounts()
        {
            var loginPage = TestFactory.New<LoginPage>();

            loginPage.GoTo();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            var dashboard = loginPage.ChooseCompanyOne();

            dashboard.Logout();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            loginPage.ChooseCompanyTwo();

            dashboard.Logout();
        }

        [Test]
        [Description("Add pre-reg user for 321 that is already registered as self-reg for 123")]
        public void AddPreRegUser()
        {
            var loginPage = TestFactory.New<LoginPage>();

            loginPage.GoTo();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            var dashboard = loginPage.ChooseCompanyTwo();

            dashboard.GoToUsers();
            var preRegUsersPage = dashboard.GoToPreRegTab();

            preRegUsersPage.AddUser("Tom", "Dillon", " initiafy.test+gh0892@gmail.com", "Learner");

            dashboard.Logout();

            loginPage.Login("initiafy.test+gh0892@gmail.com",  _testConfig.UserPassword);

            loginPage.ChooseCompanyOne();

            dashboard.Logout();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            loginPage.ChooseCompanyTwo();

            dashboard.Logout();
        }

        [Test]
        [Description("Add contract company for 321")]
        public void AddContractCompany()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            var dashboard = loginPage.ChooseCompanyTwo();

            dashboard.GoToUsers();
            var companiesPage = dashboard.GoToCompanies();

            companyCode = GetTimestamp(DateTime.Now);
            companiesPage.AddContractCompany("Jerry's", "Jerry Smith", "initiafy.test+09174@gmail.com", companyCode);

            var companySettingsPage = dashboard.GoToCompanySettingsAsAdmin();
            var userRegistration = companySettingsPage.GoToUserRegistration();

            userRegistration.AddLocation("London");
            userRegistration.AddLocation("Manchester");
            userRegistration.AddLocation("Liverpool");
            userRegistration.AddDepartment("IT");
            userRegistration.AddDepartment("Accounts");

            dashboard.Logout();
        }

        [Test]
        [Description("Re-Register for different contract company")]
        public void ReRegistration()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            var contactDetailsRegistrationPage = loginPage.EnterCompanyCode(companyCode);

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail("initiafy.test+gh0974@gmail.com");

            var profilePage = registrationPage.ReRegister();

            profilePage.Logout();

            loginPage.Login("initiafy.test+gh0892@gmail.com",  _testConfig.UserPassword);

            loginPage.ChooseCompanyOne();

            profilePage.Logout();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            loginPage.ChooseCompanyTwo();

            profilePage.Logout();
        }

        [Test]
        [Description("Set the licence limit for the new company")]
        public void SetLicenceLimit()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            var dashboard = loginPage.ChooseCompanyTwo();

            var companySettings = dashboard.GoToCompanySettingsAsAdmin();

            var licensesPage = companySettings.GoToLicenses();

            licensesPage.SetLicenseLimit("10");

            dashboard.Logout();
        }
    }
}