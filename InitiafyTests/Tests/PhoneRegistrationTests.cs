﻿using System;
using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using InitiafyTests.Models;
using NUnit.Framework;
using OpenQA.Selenium;

namespace InitiafyTests.Tests
{
    public class PhoneRegistrationTests : BaseTests
    {
        public override string TestName => "Phone Registration Tests";

        private ContractorCompany _contractorCompany;
        
        public PhoneRegistrationTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
            var companyCode = GetTimestamp(DateTime.Now);
            _contractorCompany = new ContractorCompany()
            {
                Name = "Company phone registration",
                ContractPerson = "Contact Person",
                CompanyCode = companyCode,
                ContractorManagerEmail = $"initiafy.test+{companyCode}@gmail.com",
            };
        }

        public override void RunAll()
        {
            //1- Create a contractor company.
            base.LoginAsSA();
            this.AddContractCompany();
            this.RegisterUsersWithPhone();
        }

       

        [Test]
        [NUnit.Framework.Description("Add contract company")]
        public void AddContractCompany()
        {
            test = extent.CreateTest("Add contract company");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, _testConfig.UserPassword);

            dashboard.GoToUsers();

            var contractCompaniesPage = dashboard.GoToCompanies();

            contractCompaniesPage.AddContractCompany(_contractorCompany.Name, _contractorCompany.ContractPerson, _contractorCompany.CompanyCode, _contractorCompany.ContractorManagerEmail);

            dashboard.Logout();

            test.Log(Status.Pass, $"Contract company {_contractorCompany.Name} created");
        }

        #region Register Users with phone

        private void RegisterUsersWithPhone()
        {
            this.RegisterUserWithPhone("0851673759");
            this.RegisterUserWithPhone("+5562985362585");
        }

        [Test]
        public void RegisterUserWithPhone(string phoneNumber)
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            test = extent.CreateTest("Valid, Register user with company code without any custom fields, using Phone number");

            var login = TestFactory.New<LoginPage>();
            login.GoTo();

            var contactDetailsRegistrationPage = login.EnterCompanyCode(_contractorCompany.CompanyCode);

            var registrationPage = contactDetailsRegistrationPage.RegisterPhoneNumber(phoneNumber);

            var userProfile = registrationPage.RegisterBasic("Jim", "Smith", "UK", "IT", "Password1");

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();

            test.Log(Status.Pass, "User registered successfully through to profile");
        }

        #endregion
    }
}
