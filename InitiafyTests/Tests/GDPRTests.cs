﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Tests for the GDPR Feature")]
    internal class GDPRTests : BaseTests
    {
        public GDPRTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "13. Data Protection Tests";

        public override void RunAll()
        {
            this.SetupCompanyAndAdmin();
            this.EditDataProtection();
            this.CreateContractCompany();
            this.RegisterUserGDPR();
            this.TryRegisterWithoutGDPR();
            this.CheckProfileDataProtection();
            this.TurnOnContractorManagement();
            this.CreateContractCompanyGDPR();
            this.RegisterCompany();
            this.ApproveCompany();
            this.LoginAsCMAndAddUser();
            this.RegisterAsCMAddedUser();
            this.TryRegisterAsCMAddedUserWithoutGDPR();
        }

        [Test]
        [Description("Creates company and Company Admin")]
        public void SetupCompanyAndAdmin()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            companiesPage.CreateCompany("Test 357", "Test 357");

            var dashboard = companiesPage.SearchForCompany("Test 357");

            var dashboardOverview = TestFactory.New<DashboardOverviewPage>();

            dashboardOverview.SetCredits("200");

            dashboard.GoToUsers();

            var preRegUsers = dashboard.GoToPreRegTab();

            preRegUsers.AddUser("Company", "Admin", "initiafy.test.CA257@gmail.com", "admin");

            dashboard.Logout();
        }

        [Test]
        [Description("Creates company and Company Admin")]
        public void EditDataProtection()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsAdmin("initiafy.test.CA257@gmail.com", _testConfig.UserPassword);

            var companySettingsPage = dashboard.GoToCompanySettingsAsAdmin();

            var dataProtectionPage = companySettingsPage.GoToDataProtection();

            dataProtectionPage.UpdateDetails("Test 357", "Shane Cleary 088 123321123", "John Doe 12344321", "Yes", "Yes", "People's Republik of NK", "North Korea", "1");

            dataProtectionPage.Preview();

            dashboard.Logout();
        }

        [Test]
        [Description("Creates Locations and departments and contract company")]
        public void CreateContractCompany()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsAdmin("initiafy.test.CA257@gmail.com",  _testConfig.UserPassword);

            dashboard.GoToUsers();

            var contractCompaniesPage = dashboard.GoToCompanies();

            contractCompaniesPage.AddContractCompany("GDPR1", "Contractor Manager", "initiafy.test+gdpr1@gmail.com", "GDPR28052018");

            var companySettingsPage = dashboard.GoToCompanySettingsAsAdmin();

            var userRegPage = companySettingsPage.GoToUserRegistration();

            userRegPage.AddLocation("Paris");
            userRegPage.AddLocation("Marseille");
            userRegPage.AddLocation("Lyon");
            userRegPage.AddLocation("Toulouse");
            userRegPage.AddLocation("Nice");
            userRegPage.AddLocation("Nantes");
            userRegPage.AddLocation("Strasbourg");

            userRegPage.AddDepartment("IT");
            userRegPage.AddDepartment("Accounts");

            dashboard.Logout();
        }

        [Test]
        [Description("Registers user with GDPR on")]
        public void RegisterUserGDPR()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var contactDetailsRegistrationPage = loginPage.EnterCompanyCode("GDPR28052018");

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail("initiafy.test+XIV@gmail.com");

            var profilePage = registrationPage.RegisterBasic("Louis", "XIV", "Paris", "IT",  _testConfig.UserPassword, false, true);

            profilePage.Logout();
        }

        [Test]
        [Description("Registers user with GDPR on")]
        public void TryRegisterWithoutGDPR()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var contactDetailsRegistrationPage = loginPage.EnterCompanyCode("GDPR28052018");

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail("initiafy.test+VVG@gmail.com");

            registrationPage.RegisterBasic("Vincent", "van Gogh", "Lyon", "IT",  _testConfig.UserPassword);

            registrationPage.VerifyPage();

            loginPage.GoTo();
        }

        [Test]
        [Description("Verifies that Data Protection shows in profile")]
        public void CheckProfileDataProtection()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            loginPage.Login("initiafy.test+XIV@gmail.com",  _testConfig.UserPassword);

            var userProfilePage = TestFactory.New<UserProfilePage>();

            userProfilePage.CheckDataProtection();

            userProfilePage.Logout();
        }

        [Test]
        [Description("Turns on Contractor Management")]
        public void TurnOnContractorManagement()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany("Test 357");

            var companySettings = dashboard.GoToCompanySettings();

            var companyDetails = TestFactory.New<CompanyDetailsPage>();
            companyDetails.AddContactPerson("Shane Cleary");
            companyDetails.AddEmail("a@a.com");
            companyDetails.TurnOnContractorManagement();

            dashboard.Logout();
        }

        [Test]
        [Description("Creates contract company")]
        public void CreateContractCompanyGDPR()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsAdmin("initiafy.test.CA257@gmail.com",  _testConfig.UserPassword);

            dashboard.GoToUsers();

            var contractCompaniesPage = dashboard.GoToCompanies();

            contractCompaniesPage.AddContractCompany("GDPR Company 2", "Contractor Manager", "initiafy.test+gdpr2@gmail.com", "GDPR2");

            dashboard.Logout();
        }

        [Test]
        [Description("Registers Company")]
        public void RegisterCompany()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            loginPage.LoginAsCM("initiafy.test+gdpr2@gmail.com",  _testConfig.UserPassword);

            var companyRegistration = TestFactory.New<CompanyRegistrationPage>();

            var dashboard = companyRegistration.RegisterCompanyGDPR();

            dashboard.Logout();
        }

        [Test]
        [Description("Approve Company")]
        public void ApproveCompany()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsAdmin("initiafy.test.CA257@gmail.com",  _testConfig.UserPassword);

            var contractorManagement = dashboard.GoToContractorManagement();
            var reviewDialog = contractorManagement.SearchForCompanyAndReview("GDPR Company 2");

            reviewDialog.Next();
            reviewDialog.Next();
            reviewDialog.Approve();

            dashboard.Logout();
        }

        [Test]
        [Description("Login as CM and add user")]
        public void LoginAsCMAndAddUser()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsCM("initiafy.test+gdpr2@gmail.com",  _testConfig.UserPassword);

            dashboard.GoToUsers();

            var preRegUsers = TestFactory.New<PreregUsers>();

            preRegUsers.AddUser("Zinedine", "Zidane", "initiafy.test+zz01@gmail.com", "Learner");

            preRegUsers.AddUser("Thierry", "Henry", "initiafy.test+th01@gmail.com", "Learner");

            dashboard.Logout();
        }

        [Test]
        [Description("Register as CM added learner (Re-reg test)")]
        public void RegisterAsCMAddedUser()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            loginPage.Login("initiafy.test+zz01@gmail.com",  _testConfig.UserPassword);

            var registrationPage = TestFactory.New<RegistrationPage>();

            var userProfilePage = registrationPage.RegisterBasic("Zinedine", "Zidane", "Lyon", "IT",  _testConfig.UserPassword, true, true);

            userProfilePage.Logout();
        }

        [Test]
        [Description("Register as CM added learner (Re-reg test)")]
        public void TryRegisterAsCMAddedUserWithoutGDPR()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            loginPage.Login("initiafy.test+th01@gmail.com",  _testConfig.UserPassword);

            var registrationPage = TestFactory.New<RegistrationPage>();

            var userProfilePage = registrationPage.RegisterBasic("Thierry", "Henry", "Lyon", "IT",  _testConfig.UserPassword, true);

            loginPage.GoTo();
        }
    }
}