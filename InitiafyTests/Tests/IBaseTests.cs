﻿using OpenQA.Selenium;

namespace InitiafyTests.Tests
{
    public interface IBaseTests
    {
        void RunAll();
        IWebDriver Driver { get; set; }
        string TestName { get; }
    }
}