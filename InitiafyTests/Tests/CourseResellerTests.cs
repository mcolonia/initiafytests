﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    internal class CourseResellerTests : BaseTests
    {
        public CourseResellerTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "9. Course Reseller Tests";

        public override void RunAll()
        {
            this.TurnOnCourseReseller();
            this.CreateAResellerCourse();
            this.CreateAutoAssignRule();
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public void TurnOnCourseReseller()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);

            var companySettings = dashboard.GoToCompanySettings();

            var companyDetails = TestFactory.New<CompanyDetailsPage>();
            companyDetails.TurnOnCourseReseller();

            dashboard.Logout();
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public void CreateAResellerCourse()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            var coursesPage = dashboard.GoToCourses();

            coursesPage.CreateSellerCourse("Course seller 1", "80", "2", "2 years", "11.00");

            var courseEditor = coursesPage.Search("Course seller 1");

            courseEditor.Publish();
        }

        [Test]
        [Description("Create Auto assign rule for the reseller course")]
        public void CreateAutoAssignRule()
        {
            var dashboard = TestFactory.New<Dashboard>();

            var companySettingsPage = dashboard.GoToCompanySettingsAsAdmin();

            var userRegistration = companySettingsPage.GoToUserRegistration();

            userRegistration.CreateCourseAssignRule("Course seller 1", "2");

            dashboard.Logout();
        }

        [Test]
        [Description("Register as Learner and purchase course")]
        public void PurchaseCourse()
        {
            var loginPage = TestFactory.New<LoginPage>();

            loginPage.Login("", "");
        }
    }
}