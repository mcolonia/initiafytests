﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InitiafyTests
{
    [TestFixture]
    [Description("General Tests, including the setting up of a company, registration page, course builder and registration")]
    internal class TestPack1 : BaseTests
    {
        private string UserEmail = "";
        private string timeStamp = "";
        private string CompanyCode = "";

        public TestPack1(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "1. Create Company and sample data";

        public override void RunAll()
        {
            this.StartReport("CreateCompanyAndSampleData");
            this.LoginAsSA();
            this.RemoveTheTestCompany();
            this.CreateACompany();
            this.SearchForCompany();
            this.SetLicensesForCompany();
            this.CreateCompanyAdmin();
            this.CreateContractCompany();
            this.TryAddingContractCompanyWithMissingInfo();
            this.CreateAutoAssignOptions();
            this.DeleteAutoAssignOption();
            this.UpdateListTitle();
            this.EnterCompanyDetails();
            this.UploadCompanyLogo();
            this.SetCorporateColours();
            this.Logout();
            this.LoginAsCompanyAdmin();
            this.CreateACourse();
            this.EditCourse();
            this.CreateAutoAssignRule();
            this.CreateAdditionalDocuments();
            this.SetReminder();
            this.SetRefresher();
            this.EmailNotifications();
            this.CreatePreRegUser();
            this.TryAddingPreRegUsersWithMissingInfo();
            this.RegisterAsContractor();
            //this.RegisterAsUserWithPhoneWithoutCountryCode();
            this.CreateCustomFields();
            this.RegisterAsContractorWithCustomFields();
            //this.ResetPassword(); //Not working
            this.VerifyEmailAddress();
            this.TakeCourseAsLearner();

            this.LoginAsContractorManager();
            this.RegisterAsContractorManager();
            //this.RegisterAsContractorAddedByCM(); //Not working

            this.DeleteAnAssignedCourse();
            this.DeleteUser();
            this.UploadUsefulDocument();

            this.TurnOnRAG();
            this.ChangeAUserStatusToRed();
            this.ChangeAUserStatusToAmber();
        }

        private void RemoveTheTestCompany()
        {
            test = extent.CreateTest("Create a Customer and Company");

            var companiesPage = TestFactory.New<CompaniesPage>();
            companiesPage.RemoveCompany(_testConfig.CustomerName, _testConfig.CompanyName);

            test.Log(Status.Pass, "Customer and company created");
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public new void LoginAsSA()
        {
            test = extent.CreateTest("Login as SA");

            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);
            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");

            test.Log(Status.Pass, "Logged in as SA");
            //extent.Flush();
        }

        [Test]
        [Description("Creates a customer, and company")]
        public void CreateACompany()
        {
            test = extent.CreateTest("Create a Customer and Company");

            var companiesPage = TestFactory.New<CompaniesPage>();
            companiesPage.CreateCompany(_testConfig.CustomerName, _testConfig.CompanyName);

            test.Log(Status.Pass, "Customer and company created");
        }

        [Test]
        [Description("Searchs for company and clicks to go through to dashboard")]
        public void SearchForCompany()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);

            test = extent.CreateTest("Search for company and go through to dashboard");
            test.Log(Status.Pass, "Company found and clicked through to dashboard");
        }

        [Test]
        [Description("Creates licenses in the Overview page")]
        public void SetLicensesForCompany()
        {
            var dashboardOverview = TestFactory.New<DashboardOverviewPage>();
            dashboardOverview.SetCredits("200");

            test = extent.CreateTest("Set the licenses for a company from the dashboard overview section");
            test.Log(Status.Pass, "Company licenses updated");
        }

        [Test]
        [Description("Creates a company admin")]
        public void CreateCompanyAdmin()
        {
            var dashboard = TestFactory.New<Dashboard>();
            dashboard.GoToUsers();
            var preregUsers = dashboard.GoToPreRegTab();
            preregUsers.AddUser("Shane", "Cleary", _testConfig.CompanyAdminEmail, "Company Admin");

            test = extent.CreateTest("Create a Company Admin");
            test.Log(Status.Pass, "Company Admin created");
        }

        [Test]
        [Description("Creates a contract company")]
        public void CreateContractCompany()
        {
            var dashboard = TestFactory.New<Dashboard>();
            var contractorsPage = dashboard.GoToCompanies();

            //using timestamp to create unique company code
            CompanyCode = GetTimestamp(DateTime.Now);

            contractorsPage.AddContractCompany("Company1", "Shane Cleary", "initiafy.test+z3413z@gmail.com", CompanyCode);

            test = extent.CreateTest("Create a contract company");
            test.Log(Status.Pass, "Contract company created");
        }

        [Test]
        [Description("Checking error messages for data missing or incorrect when adding Contract Company")]
        public void TryAddingContractCompanyWithMissingInfo()
        {
            var contractCompaniesPage = TestFactory.New<ContractorsPage>();
            Driver.Navigate().Refresh();
            //No Company Name
            test = extent.CreateTest("Invalid, Try to create a company without a title and check error message");

            var count = contractCompaniesPage.GetCompanyCount();

            contractCompaniesPage.AddContractCompany("", "Shane Cleary", "initiafy.test+123@gmail.com", "124352352345");

            contractCompaniesPage.InsureTheNumberOfCompaniesHasNotChanged(count);

            test.Log(Status.Pass, "Company not created, error message shown");

            //No company Contact person
            test = extent.CreateTest("Invalid, Try to create a company without Contact Person and check error message");

            contractCompaniesPage.AddContractCompany("Company Test29", "", "initiafy.test+123@gmail.com", "124352352345");

            contractCompaniesPage.InsureTheNumberOfCompaniesHasNotChanged(count);

            test.Log(Status.Pass, "Company not created, error message shown");

            //No contact Email
            test = extent.CreateTest("Invalid, Try to create a company without a contact email address");

            contractCompaniesPage.AddContractCompany("Company Test29", "Shane Cleary", "", "124352352345");

            contractCompaniesPage.InsureTheNumberOfCompaniesHasNotChanged(count);

            test.Log(Status.Pass, "Company not created, error message shown");

            //No Company Code
            test = extent.CreateTest("Invalid, Try to create a company without a company code");

            contractCompaniesPage.AddContractCompany("Company Test29", "Shane Cleary", "initiafy.test+123@gmail.com", "");

            contractCompaniesPage.InsureTheNumberOfCompaniesHasNotChanged(count);

            test.Log(Status.Pass, "Company not created, error message shown");

            //Email address already used
            test = extent.CreateTest("Invalid, Try to create a company with email that is already used");

            contractCompaniesPage.AddContractCompany("Company Test29", "Shane Cleary", "initiafy.test+z3413z@gmail.com", "52345234623452354");

            contractCompaniesPage.InsureTheNumberOfCompaniesHasNotChanged(count);

            test.Log(Status.Pass, "Company not created, error message shown");

            //Invalid Email address
            test = extent.CreateTest("Invalid, Try to create a company with an invalid email address");

            contractCompaniesPage.AddContractCompany("Company Test29", "Shane Cleary", "initiafy.testgmail.com", "52345234623452354");

            contractCompaniesPage.InsureTheNumberOfCompaniesHasNotChanged(count);

            test.Log(Status.Pass, "Company not created, error message shown");
        }

        [Test]
        [Description("Creates auto assign options")]
        public void CreateAutoAssignOptions()
        {
            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();
            var userRegistration = companySettings.GoToUserRegistration();

            test = extent.CreateTest("Valid, Add location (dropdown 1) options");
            userRegistration.AddLocation("Ireland");
            userRegistration.AddLocation("UK");
            test.Log(Status.Pass, "Two location choices added");

            test = extent.CreateTest("Valid, Add department (dropdown 2) options");
            userRegistration.AddDepartment("Accounts");
            userRegistration.AddDepartment("IT");
            userRegistration.AddDepartment("Delete Me");
            test.Log(Status.Pass, "Two department choices added");
        }

        [Test]
        [Description("Deletes an auto assign option")]
        public void DeleteAutoAssignOption()
        {
            var userRegistration = TestFactory.New<UserRegistrationSettingsPage>();

            test = extent.CreateTest("Valid, Delete department option (Delete Me)");
            userRegistration.DeleteDepartment();
            test.Log(Status.Pass, "Department deleted");
        }

        [Test]
        [Description("Updates the title of one of the dropdowns")]
        public void UpdateListTitle()
        {
            var userRegistration = TestFactory.New<UserRegistrationSettingsPage>();

            test = extent.CreateTest("Valid, Update Location (dropdown 1) title to Role");
            userRegistration.UpdateDepartmentTitle("Role");
            test.Log(Status.Pass, "Title for dropdown updated from Location to role");
        }

        [Test]
        [Description("Enters contact person and contact email for company")]
        public void EnterCompanyDetails()
        {
            var companySettings = TestFactory.New<CompanySettingsPage>();
            var companyDetails = companySettings.GoToCompanyDetails();

            test = extent.CreateTest("Valid, Update the contact person in company details and contact email");
            companyDetails.AddContactPerson("Shane Cleary");
            companyDetails.AddEmail("initiafy.test@gmail.com");
            test.Log(Status.Pass, "Contact person and email added");
        }

        [Test]
        [Description("Uploads a company logo")]
        public void UploadCompanyLogo()
        {
            var companyDetails = TestFactory.New<CompanyDetailsPage>();

            test = extent.CreateTest("Valid, Upload a company logo");
            companyDetails.UploadLogo(this.GetExternalFilePath("Original 2.jpg"));
            test.Log(Status.Pass, "Company logo added");
        }

        [Test]
        [Description("Sets the corporate colours for company")]
        public void SetCorporateColours()
        {
            var companyDetails = TestFactory.New<CompanyDetailsPage>();

            test = extent.CreateTest("Valid, Update the corporate colours for a company");
            companyDetails.SetColours1("ff6600");
            companyDetails.SetColours2("0000ff");
            companyDetails.SaveChanges();
            test.Log(Status.Pass, "Company corporate colours updated");
        }

        [Test]
        [Description("Logs out")]
        public void Logout()
        {
            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();
        }

        [Test]
        [Description("Logs in as CA")]
        public void LoginAsCompanyAdmin()
        {
            test = extent.CreateTest("Valid, Login as the company admin");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password = _testConfig.UserPassword;

            loginPage.Login(_testConfig.CompanyAdminEmail, password);

            test.Log(Status.Pass, "Company admin logged in successfully");
        }

        [Test]
        [Description("Creates a new course")]
        public void CreateACourse()
        {
            test = extent.CreateTest("Valid, Create a course");

            var dashbord = TestFactory.New<Dashboard>();
            var coursesPage = dashbord.GoToCourses();
            coursesPage.CreateCourse("Test Course", "80 %", "2", "12 months");
            var couserEditor = coursesPage.Search("Test Course");

            test.Log(Status.Pass, "Course Test Course crated");
        }

        [Test]
        [Description("Creates chapter and slides, adding content and images. Publishes the course")]
        public void EditCourse()
        {
            test = extent.CreateTest("Valid, Creates chapter and slides, adding content and images. Publishes the course");

            var courseEditor = TestFactory.New<CourseEditor>();
            courseEditor.CreateChapter("Chapter1");

            courseEditor.CreateSlide("Slide 1", this.GetExternalFilePath("Builder on Phone.jpg"), "Talking on the Phone");
            courseEditor.CreateSlide("Slide 2", this.GetExternalFilePath("builder with tea canteen facilities.jpg"), "Drinking Tea");
            courseEditor.CreateSlide("Slide 3", this.GetExternalFilePath("Construction Worker Smoking.jpg"), "Having a smoke");
            courseEditor.CreateSlide("Slide 4", this.GetExternalFilePath("forklift driver.jpg"), "Driving the forklift");

            courseEditor.CreateQuestionSlide("Question 1", "The answer is one.");
            courseEditor.CreateQuestionSlide("Question 2", "The answer is one.");

            courseEditor.Publish();

            var dashboard = TestFactory.New<Dashboard>();

            dashboard.Logout();

            test.Log(Status.Pass, "Course Test Course created");
        }

        [Test]
        [Description("Creates an auto assign rule for a course")]
        public void CreateAutoAssignRule()
        {
            test = extent.CreateTest("Valid, Create an autoassign rule for a course");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password = _testConfig.UserPassword;

            loginPage.Login(_testConfig.CompanyAdminEmail, password);

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettingsAsAdmin();
            var userRegistration = companySettings.GoToUserRegistration();

            userRegistration.CreateCourseAssignRule("Test Course", "2");

            test.Log(Status.Pass, "Auto assign rule for Test Course created");
        }

        [Test]
        [Description("Creates two additional documents")]
        public void CreateAdditionalDocuments()
        {
            test = extent.CreateTest("Valid, Create two additional document options");

            var userRegistration = TestFactory.New<UserRegistrationSettingsPage>();
            var additionalDocuments = userRegistration.GoToAdditionalDocuments();

            additionalDocuments.CreateAdditionalDocument("Additional Document 1");
            additionalDocuments.CreateAdditionalDocument("Additional Document 2");

            test.Log(Status.Pass, "Two additional document types created");
        }

        [Test]
        [Description("Sets the reminders for the due date for a course")]
        public void SetReminder()
        {
            test = extent.CreateTest("Valid, Set the reminder for a course");

            var companySettings = TestFactory.New<CompanySettingsPage>();
            var reminderRefreshers = companySettings.GoToRemindersRefreshers();

            reminderRefreshers.SetReminder("4", "4");

            test.Log(Status.Pass, "Reminder set for course, 4,4");
        }

        [Test]
        [Description("Sets the refresher for a course")]
        public void SetRefresher()
        {
            test = extent.CreateTest("Valid, Set up a course refresher");

            var reminderRefreshers = TestFactory.New<RemaindersRefreshers>();
            var refreshers = reminderRefreshers.GoToRefreshers();

            refreshers.CreateRefresher("Test Course");

            test.Log(Status.Pass, "Refresher set for course");
        }

        [Test]
        [Description("Adds email address for Default email address, weekly email address and creates a custom notification")]
        public void EmailNotifications()
        {
            test = extent.CreateTest("Valid, Set up a default email address, a weekly report email address and one custom notification email");

            var companySettings = TestFactory.New<CompanySettingsPage>();
            var emailNotifications = companySettings.GoToEmailNotifications();

            emailNotifications.SetDefaultEmail("initiafy.test+dea@gmail.com");

            emailNotifications.SetWeeklyReportEmail("initiafy.test+wr1@gmail.com");

            emailNotifications.CreateCustomEmailNotification("initiafy.test+de1@gmail.com");

            emailNotifications.SaveChanges();

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();

            test.Log(Status.Pass, "Test passed");
        }

        [Test]
        [Description("Creates a pre reg user")]
        public void CreatePreRegUser()
        {
            test = extent.CreateTest("Valid, Create a pre-reg user as company admin");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password = _testConfig.UserPassword;
            loginPage.Login(_testConfig.CompanyAdminEmail, password);

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.GoToUsers();
            var preRegUsers = dashboard.GoToPreRegTab();
            preRegUsers.AddUser("Tom", "Smith", "initiafy.test+dsf231@gmail.com", "Learner");

            test.Log(Status.Pass, "Pre-reg user created");
        }

        [Test]
        [Description("Checking error messages for data missing or incorrect when adding Contract Company")]
        public void TryAddingPreRegUsersWithMissingInfo()
        {
            var preRegUsersPage = TestFactory.New<PreregUsers>();

            //No first name
            test = extent.CreateTest("Invalid, Create a pre-reg user without a first name");

            preRegUsersPage.AddUser("", "Cleary", "initiafy.test+sfer24332@gmail.com", "Learner");

            preRegUsersPage.CheckErrorMessageCheckYourData();

            preRegUsersPage.CloseNotification();

            test.Log(Status.Pass, "User not created, error message shown");

            //No last name
            test = extent.CreateTest("Invalid, Create a pre-reg user without a last name");

            preRegUsersPage.AddUser("Shane", "", "initiafy.test+sfer24332@gmail.com", "Learner");

            preRegUsersPage.CheckErrorMessageCheckYourData();

            preRegUsersPage.CloseNotification();

            test.Log(Status.Pass, "User not created, error message shown");

            //No Email
            test = extent.CreateTest("Invalid, Create a pre-reg user without email address");

            preRegUsersPage.AddUser("Shane", "Cleary", "", "Learner");

            preRegUsersPage.CheckErrorMessageCheckYourData();

            preRegUsersPage.CloseNotification();

            test.Log(Status.Pass, "User not created, error message shown");

            //Invalid Email 1
            test = extent.CreateTest("Invalid, Create a pre-reg user with invalid email address (initiafy.test+sfer24332gmail.com)");

            preRegUsersPage.AddUser("Shane", "Cleary", "initiafy.test+sfer24332gmail.com", "Learner");

            preRegUsersPage.CheckErrorMessageCheckYourData();

            preRegUsersPage.CloseNotification();

            test.Log(Status.Pass, "User not created, error message shown");

            //Invalid Email 2
            test = extent.CreateTest("Invalid, Create a pre-reg user with invalid email address (initiafy.test+sfer24332gmail)");

            preRegUsersPage.AddUser("Shane", "Cleary", "initiafy.test+sfer24332gmail", "Learner");

            preRegUsersPage.CheckErrorMessageCheckYourData();

            preRegUsersPage.CloseNotification();

            test.Log(Status.Pass, "User not created, error message shown");

            //Add User with Email already used
            test = extent.CreateTest("Invalid, Create a pre-reg user with email already used");

            preRegUsersPage.AddUser("Shane", "Cleary", _testConfig.CompanyAdminEmail, "Learner");

            preRegUsersPage.CheckErrorMessageEmailAlreadyUsed();

            preRegUsersPage.CloseNotification();

            test.Log(Status.Pass, "User not created, error message shown");

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();
        }

        [Test]
        [Description("Registers a user with company code")]
        public void RegisterAsContractor()
        {
            test = extent.CreateTest("Valid, Register user with company code without any custom fields");

            var login = TestFactory.New<LoginPage>();
            login.GoTo();

            var contactDetailsRegistrationPage = login.EnterCompanyCode(CompanyCode); //CompanyCode

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail("initiafy.test+sdf09i@gmail.com");

            var userProfile = registrationPage.RegisterBasic("Jim", "Smith", "UK", "IT", "Password1");

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();

            test.Log(Status.Pass, "User registered successfully through to profile");
        }

        [Test]
        [Description("Registers a user with company code")]
        public void RegisterAsUserWithPhoneWithoutCountryCode()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            test = extent.CreateTest("Valid, Register user with company code without any custom fields, using Phone number");

            var login = TestFactory.New<LoginPage>();
            login.GoTo();

            var contactDetailsRegistrationPage = login.EnterCompanyCode(CompanyCode);

            var registrationPage = contactDetailsRegistrationPage.RegisterPhoneNumber("0851673759");

            var userProfile = registrationPage.RegisterBasic("Jim", "Smith", "UK", "IT", "Password1");

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();

            test.Log(Status.Pass, "User registered successfully through to profile");
        }

        [Test]
        [Description("Creates one type of each custom control and each option of upload")]
        public void CreateCustomFields()
        {
            test = extent.CreateTest("Valid, As Company Admin create one type of each custom field");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password = _testConfig.UserPassword;
            loginPage.Login(_testConfig.CompanyAdminEmail, password);

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettingsAsAdmin();
            var userRegistration = companySettings.GoToUserRegistration();
            var customFields = userRegistration.GoToCustomFields();

            customFields.AddTextField("Text", "Some help text");
            customFields.AddNumberField("Number");
            customFields.AddDropList("Drop List");
            customFields.AddPickList("Pick List");
            customFields.AddDateField("Date");
            customFields.AddCharBoxes("Char Boxes");
            customFields.AddProfilePic("Profile Pic");
            customFields.AddDocumentUpload("Doc 1");
            customFields.AddDocumentUpload("Doc 2 Opt", false);
            customFields.AddDocumentUpload("Doc 3 Opt w/ Expiry", false, true);
            customFields.SaveChanges();

            dashboard.Logout();

            test.Log(Status.Pass, "One type of each custom field created");
        }

        [Test]
        [Description("Registers a user with company code on long registration form with uploads")]
        public void RegisterAsContractorWithCustomFields()
        {
            test = extent.CreateTest("Valid, Register using company code on registration form with one type of each custom field (profile pic and doc upload)");

            var login = TestFactory.New<LoginPage>();
            login.GoTo();

            var contactDetailsRegistrationPage = login.EnterCompanyCode(CompanyCode); //CompanyCode

            string profilePic = this.GetExternalFilePath("Builder on Phone.jpg");
            string document = this.GetExternalFilePath("A Test document.docx");

            timeStamp = GetTimestamp(DateTime.Now);

            UserEmail = "initiafy.test+" + timeStamp + "@gmail.com";

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail(UserEmail);

            var userProfile = registrationPage.RegisterCustomFields1("Jim", "Smith", "UK", "IT", "Password1", profilePic, document);

            userProfile.Logout();

            test.Log(Status.Pass, "User registered and through to profile");
        }

        [Test]
        [Description("Verifies the email address of the user")]
        public void VerifyEmailAddress()
        {
            test = extent.CreateTest("Valid, Verify email address");

            base.VerifyEmailAddress(UserEmail);

            test.Log(Status.Pass, "Verify email address as learner");
        }

        //[Test]
        //[Description("Reset the password for a user and verify they can login with new password")]
        //public void ResetPassword()
        //{
        //    var login = TestFactory.New<LoginPage>();
        //    login.GoTo();
        //    var resetPasswordPage = login.ResetPassword();

        //    var newPasswordPage = resetPasswordPage.ResetPassword(UserEmail);

        //    var passwordResetSuccessPage = newPasswordPage.CreateNewPassword("Password2");

        //    passwordResetSuccessPage.ProceedToLogin();

        //    login.Login(UserEmail, "Password2");
        //}

        [Test]
        [Description("Take a course as a learner")]
        public void TakeCourseAsLearner()
        {
            test = extent.CreateTest("Valid, Login and complete course as learner");

            var login = TestFactory.New<LoginPage>();
            login.GoTo();
            login.Login(UserEmail, "Password1");

            var userProfilePage = TestFactory.New<UserProfilePage>();
            var coursePage = userProfilePage.StartCourse();
            coursePage.CompleteCourse();

            test.Log(Status.Pass, "Course successfully completed");
        }

        [Test]
        [Description("Login as the contractor manager and add a user")]
        public void LoginAsContractorManager()
        {
            test = extent.CreateTest("Valid, Login as Contractor Manager and add a user");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            string password = _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsCM("initiafy.test+z3413z@gmail.com", password);

            dashboard.GoToUsers();
            var preRegUsersPage = TestFactory.New<PreregUsers>();

            preRegUsersPage.AddUser("Tim", "Stone", "initiafy.test+sdf310@gmail.com", "Learner");

            test.Log(Status.Pass, "User added successfully");
        }

        [Test]
        [Description("Register as CM by going to Learner View")]
        public void RegisterAsContractorManager()
        {
            test = extent.CreateTest("Valid, Register as Contractor Manager by going to learner view");

            var dashboard = TestFactory.New<Dashboard>();
            var registrationPage = dashboard.GoToLearnerView();

            string profilePic = this.GetExternalFilePath("Construction Worker Smoking.jpg");
            string document = this.GetExternalFilePath("A Test document.docx");

            var profilePage = registrationPage.RegisterCM("John", "Peters", "Ireland", "IT", profilePic, document);

            var dashboard2 = profilePage.ReturnToAdminView();

            dashboard.Logout();

            test.Log(Status.Pass, "Contractor Manager registered successfully");
        }

        [Test]
        [Description("Register as Learner added by CM (Registering by logging in)")]
        public void RegisterAsContractorAddedByCM()
        {
            test = extent.CreateTest("Valid, Register as contracter learner (learner added by CM)");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            string password = _testConfig.UserPassword;

            loginPage.Login("initiafy.test+sdf310@gmail.com", password);

            var registrationPage = TestFactory.New<RegistrationPage>();

            string profilePic = this.GetExternalFilePath("Builder on Phone.jpg");
            string document = this.GetExternalFilePath("A Test document.docx");

            string timeStamp = GetTimestamp(DateTime.Now);

            UserEmail = "initiafy.test+" + timeStamp + "@gmail.com";

            var userProfile = registrationPage.RegisterCustomFields2("Tim", "Stone", "UK", "IT", "Password1", profilePic, document);

            userProfile.Logout();

            test.Log(Status.Pass, "User logged in and completed registration form successfully");
        }

        [Test]
        [Description("Delete an assigned course for a user as CA")]
        public void DeleteAnAssignedCourse()
        {
            test = extent.CreateTest("Valid, As Company Admin delete an assigned course");

            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            string password = _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToReports();
            var selfRegisteredCoursesPage = dashboard.GoToSelfRegCourses();

            selfRegisteredCoursesPage.DeleteAssignedCourse();

            test.Log(Status.Pass, "Assigned course successfully deleted");
        }

        [Test]
        [Description("Delete a user")]
        public void DeleteUser()
        {
            test = extent.CreateTest("Valid, As Company Admin delete a user");

            var dashboard = TestFactory.New<Dashboard>();

            dashboard.GoToUsers();

            var preRegUsersPage = dashboard.GoToPreRegTab();

            string timeStamp2 = GetTimestamp(DateTime.Now);

            preRegUsersPage.AddUser("Irene", "Timmons", "initiafy.test+" + timeStamp2 + "@gmail.com", "Learner");

            preRegUsersPage.DeleteUser("initiafy.test+" + timeStamp2 + "@gmail.com", "Irene", "Timmons");

            test.Log(Status.Pass, "User successfully added and then deleted");
        }

        [Test]
        [Description("Upload a Useful Document as CA")]
        public void UploadUsefulDocument()
        {
            test = extent.CreateTest("Valid, Upload a useful document as Company Admin");

            var dashboard = TestFactory.New<Dashboard>();
            var userProfilePage = dashboard.GoToUserProfile();

            string usefulDocument = this.GetExternalFilePath("Useful Document.docx");

            userProfilePage.UploadUsefulDocument(usefulDocument);

            userProfilePage.Logout();

            test.Log(Status.Pass, "Useful document successfully added");
        }

        [Test]
        [Description("Turn on the traffic lights feature for company")]
        public void TurnOnRAG()
        {
            test = extent.CreateTest("Valid, As SA enable the traffic lights feature for a company");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var companySettingsPage = companiesPage.SearchForCompanyAndGoToCompanySettings(_testConfig.CompanyName);

            var companyDetailsPage = TestFactory.New<CompanyDetailsPage>();

            companyDetailsPage.TurnOnTrafficLights();

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();

            test.Log(Status.Pass, "Traffic lights feature enabled for company");
        }

        [Test]
        [Description("Change a users status to red and leave comment, then filter user list to red and verify user appears")]
        public void ChangeAUserStatusToRed()
        {
            test = extent.CreateTest("Valid, As Company Admin change a users status to red and leave a comment");
            Driver.Navigate().GoToUrl(UrlHelper.MainUrl(_testConfig.Environment));
            var loginPage = TestFactory.New<LoginPage>();

            string password = _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();
            var preRegUsersPage = dashboard.GoToPreRegTab();
            preRegUsersPage.ChangeUserStatusToRed("Smoking on site!");

            test.Log(Status.Pass, "Users status updated to red, and showing in filtered list");
        }

        [Test]
        [Description("Change a users status to amber and leave comment, then filter user list to amber and verify user appears")]
        public void ChangeAUserStatusToAmber()
        {
            test = extent.CreateTest("Valid, As Company Admin change a users status to amber and leave a comment");

            var dashboard = TestFactory.New<Dashboard>();
            var selfRegUsersPage = dashboard.GoToSelfRegTab();

            selfRegUsersPage.ChangeUserStatusToAmber("Misuse of Equipment!");

            dashboard.Logout();

            test.Log(Status.Pass, "Users status updated to amber, and showing in filtered list");

            extent.Flush();
        }
    }
}