﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace InitiafyTests
{
    public class UserArchivingTests : BaseTests
    {
        public UserArchivingTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "6. Archiving Feature Tests";

        public override void RunAll()
        {
            this.StartReport("User Archiving Tests");
            this.LoginAsSA();
            this.SearchForCompany();
            this.TurnOnAutoArchiving();
            this.ArchiveUser();
            this.UnArchiveUser();
            this.LoginAsArchivedUser();
            this.VerifyUserIsUnarchived();
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public new void LoginAsSA()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }

        [Test]
        [Description("Searches for company and goes through to Dashboard")]
        public void SearchForCompany()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);
        }

        [Test]
        [Description("Turns on the Auto Archiving feature for a company")]
        public void TurnOnAutoArchiving()
        {
            test = extent.CreateTest("Turn on Auto Archiving");

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();
            var companyDetailsPage = TestFactory.New<CompanyDetailsPage>();

            companyDetailsPage.TurnOnUserArchiving();

            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);

            Thread.Sleep(1000);

            dashboard.Logout();

            test.Log(Status.Pass, "Auto Archiving turned on");
        }

        [Test]
        [Description("Logs in as the CA and Archives a user")]
        public void ArchiveUser()
        {
            test = extent.CreateTest("Archive user, ensuring user is removed from user list but included if include ");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            string password = ReadCompanyAdminPassword();
            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();

            var selfRegUsers = dashboard.GoToSelfRegTab();

            selfRegUsers.SearchForUserAndArchive("initiafy.test+gh0892@gmail.com");

            Driver.Navigate().Refresh();

            selfRegUsers = dashboard.GoToSelfRegTab();

            selfRegUsers.VerifyUserDoesNotAppearInSearch("initiafy.test+gh0892@gmail.com");

            Driver.Navigate().Refresh();

            selfRegUsers = dashboard.GoToSelfRegTab();

            selfRegUsers.IncludeArchivedSearch("initiafy.test+gh0892@gmail.com");

            test.Log(Status.Pass, "User archived, does not show in search unless 'include archived users' is ticked");
        }

        [Test]
        [Description("Unarchives archived user")]
        public void UnArchiveUser()
        {
            test = extent.CreateTest("Unarchive user ");

            var selfRegUsers = TestFactory.New<SelfRegUsers>();

            selfRegUsers.UnarchiveUser("initiafy.test+gh0892@gmail.com");

            test.Log(Status.Pass, "User unarchived");
        }

        [Test]
        [Description("Archives Users then logs in as archived user")]
        public void LoginAsArchivedUser()
        {
            test = extent.CreateTest("Login as archived user");

            var dashboard = TestFactory.New<Dashboard>();

            var preRegUsersPage = dashboard.GoToPreRegTab();

            preRegUsersPage.SearchForUserAndArchive("initiafy.test+aaa1@gmail.com");

            dashboard.Logout();

            var loginPage = TestFactory.New<LoginPage>();

            string password =  _testConfig.UserPassword;

            loginPage.Login("initiafy.test+aaa1@gmail.com", password);

            var userProfile = TestFactory.New<UserProfilePage>();

            userProfile.Logout();

            test.Log(Status.Pass, "User can login");
        }

        [Test]
        [Description("Logins in as CA and checks that user is now unarchived after logging in")]
        public void VerifyUserIsUnarchived()
        {
            test = extent.CreateTest("Verify that archived user that logged in is now included in list");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            string password = ReadCompanyAdminPassword();
            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();

            var preRegUsers = dashboard.GoToPreRegTab();

            preRegUsers.SearchForUserAndGoToProfile("initiafy.test+aaa1@gmail.com");

            dashboard.Logout();

            test.Log(Status.Pass, "User is listed");

            extent.Flush();
        }

        private string ReadCompanyAdminPassword()
        {
            string text = System.IO.File.ReadAllText(this.GetExternalFilePath("Admin Password.txt"));

            // Display the file contents to the console. Variable text is a string.
            _testTextReport.LogError($"Admin Password = {text}");

            return text;
        }
    }
}