﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Updating Password, updating account details and Re-registering tests")]
    internal class UpdatingUserDetailsTest : BaseTests
    {
        private string companyCode = "";
        private string userEmail = "";

        public UpdatingUserDetailsTest(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "12. Updating User Details Tests";

        public override void RunAll()
        {
            this.AddACompanyManager();
            this.AddContractCompanyAsCompanyManager();
            this.RegisterAsContractor();
            this.UpdateAccountPassword();
            this.LoginWithUpdatedPassword();
            this.UpdateProfile();
            this.UpdateUserDetailsAsCompanyAdmin();
            //this.ReRegister();
        }

        [Test]
        [Description("Adds a company manager")]
        public void AddACompanyManager()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany2(_testConfig.CompanyName);

            dashboard.GoToUsers();

            var preRegUsers = dashboard.GoToPreRegTab();

            preRegUsers.AddUser("Company", "Manager", "initiafy.test+CM24051@gmail.com", "Manager");

            dashboard.Logout();
        }

        [Test]
        [Description("Logs in as Company Manager and adds a contract company")]
        public void AddContractCompanyAsCompanyManager()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsAdmin("initiafy.test+CM24051@gmail.com",  _testConfig.UserPassword);

            dashboard.GoToUsers();

            var contractCompaniesPage = dashboard.GoToCompanies();

            companyCode = GetTimestamp(DateTime.Now);

            contractCompaniesPage.AddContractCompany("Contract Company 1", "Contractor Manager", "initiafy.test+cc1@gmail.com", companyCode);

            dashboard.Logout();
        }

        [Test]
        [Description("Registers a user with company code on long registration form with uploads")]
        public void RegisterAsContractor()
        {
            var login = TestFactory.New<LoginPage>();
            login.GoTo();

            var contactDetailsRegistrationPage = login.EnterCompanyCode(companyCode);

            string timeStamp = GetTimestamp(DateTime.Now);

            userEmail = "initiafy.test+" + timeStamp + "@gmail.com";

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail(userEmail);

            var userProfile = registrationPage.RegisterBasic("Jim", "Smith", "Liverpool", "Accounts", "Password1");
        }

        [Test]
        [Description("Updates the password for an account as a learner")]
        public void UpdateAccountPassword()
        {
            var userProfilePage = TestFactory.New<UserProfilePage>();

            userProfilePage.UpdatePassword("Password1",  _testConfig.UserPassword);

            userProfilePage.Logout();
        }

        [Test]
        [Description("Checks that user can login with new password")]
        public void LoginWithUpdatedPassword()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            loginPage.Login(userEmail,  _testConfig.UserPassword);
        }

        [Test]
        [Description("Updates the users profile as learner")]
        public void UpdateProfile()
        {
            var userProfilePage = TestFactory.New<UserProfilePage>();

            userProfilePage.UpdateProfileName("Niamh", "Lyons");

            userProfilePage.Logout();
        }

        [Test]
        [Description("Updates the users profile as learner")]
        public void UpdateUserDetailsAsCompanyAdmin()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            loginPage.Login(_testConfig.CompanyAdminEmail, password);

            var dashboard = loginPage.ChooseCompanyTwo();

            dashboard.GoToUsers();

            var selfRegUsers = dashboard.GoToSelfRegTab();

            var modifiedUserDialog = selfRegUsers.SearchForUserAndShowModifiedUserDialog("Niamh", "Lyons");

            modifiedUserDialog.Close();

            dashboard.Logout();

            //modifiedUserDialog.UpdateDetails("Norah", "Lyon", "initiafy.test+nl2405@gmail.com", "01 8411111");
        }

        [Test]
        [Description("Re-registers as learner by updating location and role from profile")]
        public void ReRegister()
        {
            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            loginPage.Login(userEmail,  _testConfig.UserPassword);

            var profilePage = TestFactory.New<UserProfilePage>();

            profilePage.UpdateLocationRole("Alpha", "Role 2");
        }
    }
}