﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Tests for the licensing feature")]
    public class LicenseTests : BaseTests
    {
        private string ContractorManagerEmail = "initiafy.test+vae3@gmail.com";
        private string CompanyCode = "";

        public LicenseTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "2. Licenses test";

        public override void RunAll()
        {
            this.StartReport("License Test");
            this.LoginAsSA();
            this.SearchForCompany();
            this.EditExpiryDateOfLicencePack();
            this.LoginAsExpiredAdmin();
            this.LoginAsExpiredUser();
            this.AddNewLicencePack();
            this.EditLicenseLimit();
            this.AddUsersOverLimit();
            this.TurnOnAutoRenew();
            this.CreateContractCompanyAndSetLicenseLimit();
            this.AddUsersOverLimitAsContractorManager();
            //test.CreateTheyPayContractCompany(); //Not working
            //test.PurchaseCreditsAsTheyPayCM(); //Not working
            this.ManuallyLicenceAUserAsAdmin();
            this.ManuallyLicenceAUserAsCM();
            //test.TryRegisteringWithCompanyCodeForContractorWithNoLicenses(); //Not working

        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public new void LoginAsSA()
        {
            GreenMessage("Logging as SA");

            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }

        [Test]
        [Description("Searchs for company in the SA view and click to go through to company dashboard")]
        public void SearchForCompany()
        {
            GreenMessage("Searching for company and going through to Dashboard");
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);
        }

        [Test]
        [Description("Edits the expiry of the licence pack and checks the new date has been updated for the users")]
        public void EditExpiryDateOfLicencePack()
        {
            test = extent.CreateTest("Edit the expiry date of a licence pack");

            GreenMessage("Editing the Expiry date of the licence pack and verifying users become unlicensed");

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();

            var licensesPage = companySettings.GoToLicenses();
            licensesPage.SetExpiryDate("1st", "June", "2017");

            companySettings.GoToDashboard();

            dashboard.GoToUsers();
            var preRegUsers = dashboard.GoToPreRegTab();

            preRegUsers.CheckUnlicensedLabel();

            var selfRegUsers = dashboard.GoToSelfRegTab();

            selfRegUsers.CheckUnlicensedLabel();

            dashboard.Logout();

            test.Log(Status.Pass, "Licence date expiry updated to 1t June 2017");
        }

        [Test]
        [Description("Logs in as CA verifys that the no remaining licenses tooltip is shown")]
        public void LoginAsExpiredAdmin()
        {
            test = extent.CreateTest("valid, Try to login as expired Company Admin, verify licenses message");

            GreenMessage("Logging in as CA and verifying that the tooltip for no remaining licenses is shown");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.VerifyNoLicensesMessage();

            //dashboard.GoToUsers();
            //dashboard.GoToCourses();

            dashboard.Logout();

            test.Log(Status.Pass, "Company Admin could successfully login, tooltip shown with message");
        }

        [Test]
        [Description("Tries to login as expired user for a company with no remaining licenses, verifys the correct message is shown")]
        public void LoginAsExpiredUser()
        {
            test = extent.CreateTest("valid, Try to login as expired learner, ensure user cannot login and correct message is shown");

            GreenMessage("Checking that correct error message is shown for user logging in with no licence.");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string email = "initiafy.test+sdf310@gmail.com";

            string password =  _testConfig.UserPassword;

            loginPage.Login(email, password);

            loginPage.VerifyNoLicencesMessage();

            test.Log(Status.Pass, "User could not login and correct message shown");
        }

        [Test]
        [Description("Logs in as SA and adds a new licence pack for the company")]
        public void AddNewLicencePack()
        {
            test = extent.CreateTest("valid, Login as SA and add a licence pack for company");

            GreenMessage("Adding new licence pack");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);

            var companySettings = dashboard.GoToCompanySettings();

            var licensesPage = companySettings.GoToLicenses();

            licensesPage.GetMoreLicenses("50");

            test.Log(Status.Pass, "Licence pack with 50 licenses successfully added");
        }

        [Test]
        [Description("Edits the licence limit for a company")]
        public void EditLicenseLimit()
        {
            test = extent.CreateTest("valid, As SA edit the license limit for company");

            GreenMessage("Editing the licence limit for a company");

            var licensesPage = TestFactory.New<LicensesPage>();

            licensesPage.SetLicenseLimit("5");

            var companySettings = TestFactory.New<CompanySettingsPage>();
            var dashboard = companySettings.GoToDashboard();

            dashboard.GoToReports();
            var reportsOverviewPage = dashboard.GoToReportsOverview();

            reportsOverviewPage.CheckRemainingLicenses("5");

            dashboard.Logout();

            test.Log(Status.Pass, "Licence limit set to 5");
        }

        [Test]
        [Description("Adds users and attempts to add 1 user over the licence limit, verifys that the correct error message is shown")]
        public void AddUsersOverLimit()
        {
            test = extent.CreateTest("valid, As CA add users until the license limit is reached ");

            GreenMessage("CA adds users, attempts to add 1 user over the limit, ensures correct error message is shown.");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();

            var preRegUsers = dashboard.GoToPreRegTab();

            preRegUsers.AddUser("Tom", "Dillon", "initiafy.test+aaa1@gmail.com", "Learner");
            preRegUsers.AddUser("Harry", "Dillon", "initiafy.test+aaa2@gmail.com", "Learner");
            preRegUsers.AddUser("Jack", "Dillon", "initiafy.test+aaa3@gmail.com", "Learner");
            preRegUsers.AddUser("Ian", "Dillon", "initiafy.test+aaa4@gmail.com", "Learner");

            preRegUsers.AddUserOverLimit("Mike", "Dillon", "initiafy.test+Over7@gmail.com", "Learner");

            var companySettings = dashboard.GoToCompanySettingsAsAdmin();
            var licensesPage = companySettings.GoToLicenses();

            licensesPage.SetLicenseLimit("50");

            test.Log(Status.Pass, "Company admin could add users until limit reached, correct error message shown");
        }

        [Test]
        [Description("Turns on the auto renew for both users and contractors")]
        public void TurnOnAutoRenew()
        {
            test = extent.CreateTest("valid, As CA turn on the auto renew licenses for both users and contractors");

            GreenMessage("Turning on the Auto renew for users and contractors");

            var licensesPage = TestFactory.New<LicensesPage>();

            licensesPage.TurnOnAutoRenew();

            licensesPage.TurnOnAutoRenewForContractors();

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.Logout();

            test.Log(Status.Pass, "Auto renew turned on and saved");
        }

        [Test]
        [Description("Creates a contract company and then sets the license limit for the company")]
        public void CreateContractCompanyAndSetLicenseLimit()
        {
            test = extent.CreateTest("valid, Create a contract company and set a licence limit of three for contractor");

            GreenMessage("Creating a contract company and then setting the license limit for that company");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();

            var contractorCompaniesPage = dashboard.GoToCompanies();

            CompanyCode = GetTimestamp(DateTime.Now);

            contractorCompaniesPage.AddContractCompany("Company 1804", "Shane Cleary", ContractorManagerEmail, CompanyCode);

            contractorCompaniesPage.SearchAndEdit("Company 1804");

            contractorCompaniesPage.SetLicenseLimit("3");

            dashboard.Logout();

            test.Log(Status.Pass, "License limit set to 3 for the contract company");
        }

        [Test]
        [Description("Logs in as CM and adds users, attempts to add one user over the limit and verifys that correct error message is shown")]
        public void AddUsersOverLimitAsContractorManager()
        {
            test = extent.CreateTest("valid, As Contractor Manager add users until limit is reached, verify error message");

            GreenMessage("Logging in as the CM and then adding users, tries to add one user over the limit and verifies error message is shown.");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsCM(ContractorManagerEmail, password);

            dashboard.GoToUsers();

            var preRegUsers = TestFactory.New<PreregUsers>();

            preRegUsers.AddUser("George", "Hines", "initiafy.test+gh0892@gmail.com", "Learner");

            preRegUsers.AddUser("Fred", "Duncan", "initiafy.test+gh0974@gmail.com", "Learner");

            preRegUsers.AddUserOverLimit("Emma", "Lyons", "initiafy.test+gh00643@gmail.com", "Learner");

            dashboard.Logout();

            test.Log(Status.Pass, "License limit reached and correct error message shown");
        }

        [Test]
        [Description("Creates a They Pay company")]
        public void CreateTheyPayContractCompany()
        {
            test = extent.CreateTest("valid, Create a They Pay company");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);

            var companySettingsPage = dashboard.GoToCompanySettings();

            var companyDetails = TestFactory.New<CompanyDetailsPage>();

            companyDetails.TurnOnContractorPayment();

            companySettingsPage.GoToDashboard();

            var contractCompaniesPage = dashboard.GoToCompanies();

            string code = GetTimestamp(DateTime.Now);
            contractCompaniesPage.AddContractCompany("They Pay 1", "Shane Cleary", "initiafy.test+jk09@gmail.com", "They Pay", code);

            dashboard.Logout();

            test.Log(Status.Pass, "They pay company created");
        }

        [Test]
        [Description("Logs in as CM for They Pay company and purchases licenses, then adds users. Attempts to add one user over the limit and verifys error message")]
        public void PurchaseCreditsAsTheyPayCM()
        {
            test = extent.CreateTest("valid, Login as CM for they pay and purchase licenses, add users until limit is reached");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            //_testTextReport.LogError("Enter the CM Password for They Pay (initiafy.test+jk09@gmail.com)");
            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsCM("initiafy.test+jk09@gmail.com", password);

            dashboard.VerifyPurchaseLicensesMessageForCM();

            dashboard.BuyLicenses("3");

            _testTextReport.LogError("Purchase Licenses, click enter when complete");
            Console.ReadLine();

            dashboard.GoToUsers();

            var preRegUsers = TestFactory.New<PreregUsers>();
            preRegUsers.AddUser("John", "Smith", "initiafy.test+js01@gmail.com");
            preRegUsers.AddUser("Mary", "Smith", "initiafy.test+ms01@gmail.com");
            preRegUsers.AddUserFail("Liam", "Smith", "initiafy.test+ls01@gmail.com");

            dashboard.Logout();

            test.Log(Status.Pass, "Users added until limit, correct error message shown");
        }

        [Test]
        [Description("Manually licenses an unlicensed user in their profile page as CA")]
        public void ManuallyLicenceAUserAsAdmin()
        {
            test = extent.CreateTest("valid, Login as CA and manually assign a licence for unlicensed user in profile");

            GreenMessage("Logging as CA and Manually licensing a user.");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();
            var selfRegUsersPage = dashboard.GoToSelfRegTab();

            var userProfilePage = selfRegUsersPage.SearchForUserAndGoToProfile("initiafy.test+sdf09i@gmail.com");

            userProfilePage.LicenceUser();

            dashboard.Logout();

            test.Log(Status.Pass, "User now licensed");
        }

        [Test]
        [Description("Manually licenses an unlicensed user in their profile page as CM")]
        public void ManuallyLicenceAUserAsCM()
        {
            test = extent.CreateTest("valid, Manually license an unlicensed user as a Contractor Manager");

            GreenMessage("Logging in as CM and manually licensing a user.");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            //_testTextReport.LogError("Enter the CM Password for initiafy.test+z3413z@gmail.com");
            String password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsCM("initiafy.test+z3413z@gmail.com", password);
            dashboard.GoToUsers();

            var preRegUsersPage = TestFactory.New<PreregUsers>();

            var userProfilePage = preRegUsersPage.SearchForUserAndGoToProfile("initiafy.test+sdf310@gmail.com");

            userProfilePage.LicenceUser();

            dashboard.Logout();

            test.Log(Status.Pass, "User now licensed");

            extent.Flush();
        }

        [Test]
        [Description("Tries registering for a contract company using company code for company that has reached licence limit, verifies error message")]
        public void TryRegisteringWithCompanyCodeForContractorWithNoLicenses()
        {
            test = extent.CreateTest("Invalid, Try registering for a company using Company code where no licenses remain for the contract company");

            GreenMessage("Entering company code for a company that has reached the license limit, verifying the error message.");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);
            try
            {
                loginPage.EnterCompanyCodeWith0Licences(CompanyCode);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }

            test.Log(Status.Pass, "User cannot register, correct error message shown");
        }

        [Test]
        [Description("Logs in as user with expired licence, verifies that 1 licence is taken from the parent company")]
        public void LoginAsUnlicensedUserAndCheckLicenceIsTaken()
        {
            test = extent.CreateTest("Valid, Login as expired user and check one license is taken from the pack");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password = ReadCompanyAdminPassword();

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var overviewPage = TestFactory.New<ReportsOverviewPage>();

            overviewPage.CheckRemainingLicenses("1");

            test.Log(Status.Pass, "User logged in and license is taken from pack");
        }

        public string ReadCompanyAdminPassword()
        {
            string text = System.IO.File.ReadAllText(this.GetExternalFilePath("Admin Password.txt"));

            // Display the file contents to the console. Variable text is a string.
            _testTextReport.LogError($"Admin Password = {text}");

            return text;
        }
    }
}