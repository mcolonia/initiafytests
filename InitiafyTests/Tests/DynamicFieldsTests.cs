﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    public class DynamicFieldsTests : BaseTests
    {
        public DynamicFieldsTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "11. Dynamic Fields Tests";

        public override void RunAll()
        {
            this.LoginAndGoToCustomFields();
            this.CreateCustomFields();
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public void LoginAndGoToCustomFields()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();

            loginPage.GoTo();

            loginPage.Login(_testConfig.CompanyAdminEmail,  _testConfig.UserPassword);

            var dashboard = loginPage.ChooseCompanyTwo();

            var companySettings = dashboard.GoToCompanySettingsAsAdmin();

            var userRegistrationSettingsPage = companySettings.GoToUserRegistration();

            var customFieldsTab = userRegistrationSettingsPage.GoToCustomFields();
        }

        [Test]
        [Description("Create the custom fields")]
        public void CreateCustomFields()
        {
            var customFieldsTab = TestFactory.New<CustomFieldsPage>();

            customFieldsTab.AddTextFieldDynamic("TextField1", "Help text", "Accounts");
        }
    }
}