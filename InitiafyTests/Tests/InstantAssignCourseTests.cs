﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Creates an External course and assigns it to users")]
    public class InstantAssignCourseTests : BaseTests
    {
        public InstantAssignCourseTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        [Test]
        [Description("Logs in as CA verifys that the no remaining licenses tooltip is shown")]
        public void LoginAsExpiredAdmin()
        {
            //GreenMessage("Logging in as CA and verifying that the tooltip for no remaining licenses is shown");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.VerifyNoLicensesMessage();

            //dashboard.GoToUsers();
            //dashboard.GoToCourses();

            dashboard.Logout();
        }

        public override void RunAll()
        {
            throw new NotImplementedException();
        }
    }
}