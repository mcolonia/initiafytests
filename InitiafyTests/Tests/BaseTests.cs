﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using InitiafyTests.Helpers;
using InitiafyTests.Tests;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    public abstract class BaseTests : IBaseTests
    {
        public IWebDriver Driver { get; set; }

        public virtual string TestName { get; }

        public static ExtentReports extent;

        public static ExtentTest test;

        protected TestTextReport _testTextReport;

        public TestConfig _testConfig;

        public virtual void RunAll()
        {
        }

        public BaseTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig)
        {
            Driver = driver;
            _testTextReport = testTextReport;
            _testConfig = testConfig;
        }

        public string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        //Console Messages
        public void GreenMessage(String message)
        {
            _testTextReport.LogSuccess(message);
        }

        public void RedMessage(String message)
        {
            _testTextReport.LogError(message);
        }

        public void StartReport(string reportName)
        {
            string reportPath = this.GetExternalFilePath() + reportName + ".html";
            //extent = new ExtentReports(/*reportPath, true*/);

            extent = new ExtentReports();
            var extentHtml = new ExtentHtmlReporter(reportPath);
            extent.AttachReporter(extentHtml);

            extent.AddSystemInfo("Host Name", "Shane");

            //extent.LoadConfig(projectPath + "extent-config.xml");
        }

        public void GetResult()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = "<pre>" + TestContext.CurrentContext.Result.StackTrace + "</pre>";
            var errorMessage = TestContext.CurrentContext.Result.Message;
        }

        [Test]
        [Description("Verifies the email address of the user")]
        public void VerifyEmailAddress(string email)
        {
            var currentURL = Driver.Url;
            string link = UrlHelper.VerifyEmailURL(_testConfig.Environment, email); 
            Driver.Navigate().GoToUrl(link);
            Driver.Navigate().GoToUrl(currentURL);
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public void LoginAsSA()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }
    }
}