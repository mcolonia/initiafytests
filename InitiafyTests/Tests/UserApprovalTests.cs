﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    [TestFixture]
    [Description("User Approval Tests, tests turning on the feature, registering as contractor, getting rejected and approved by CA")]
    internal class UserApprovalTests : BaseTests
    {
        private string CompanyCode = "";
        private string UserEmail = "";
        private string timeStamp = "";

        public UserApprovalTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "7. User Approval Feature Tests";

        public override void RunAll()
        {
            this.StartReport("User Approval");
            this.LoginAsSA();
            this.SearchForCompany();
            this.TurnOnUserApproval();
            this.RegisterAsContractorWithUserApproval();
            this.VerifyEmailAddress();
            //this.TakeCourseUA();
            this.RejectUser();
            this.UserFixIssues();
            this.ApproveUser();
            //test.ReUploadMandatoryDocAsLearner(); //Not working
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public new void LoginAsSA()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }

        [Test]
        [Description("Searches for company and goes through to Dashbaord")]
        public void SearchForCompany()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);
        }

        [Test]
        [Description("Turns on the User Approval feature for the company")]
        public void TurnOnUserApproval()
        {
            test = extent.CreateTest("Turn on User Approval for company");

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();

            var companyDetails = TestFactory.New<CompanyDetailsPage>();
            companyDetails.TurnOnUserApproval();

            var securitySettingsPage = companySettings.GoToSecuritySettings();
            securitySettingsPage.ValidateUsersEmails();

            companySettings.GoToDashboard();

            dashboard.GoToUsers();

            var companiesPage = dashboard.GoToCompanies();

            companiesPage.SearchForCompany("initiafy.test+z3413z@gmail.com");

            CompanyCode = companiesPage.ReadCompanyCode();

            dashboard.Logout();

            test.Log(Status.Pass, "User Approval turned on");
        }

        [Test]
        [Description("Registers with company code for user approval, verifys the confirm password message in profile")]
        public void RegisterAsContractorWithUserApproval()
        {
            test = extent.CreateTest("Register user with User Approval, ensures the confirm email address message is shown");

            var loginPage = TestFactory.New<LoginPage>();

            var contactDetailsRegistrationPage = loginPage.EnterCompanyCode(CompanyCode);

            timeStamp = GetTimestamp(DateTime.Now);

            UserEmail = "initiafy.test+" + timeStamp + "@gmail.com";

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail(UserEmail);

            string profilePic = this.GetExternalFilePath("Builder on Phone.jpg");
            string document = this.GetExternalFilePath("A Test document.docx");

            var userProfilePage = registrationPage.RegisterCustomFields1("Shane", "Cleary", "Ireland", "IT", "Password1", profilePic, document);

            userProfilePage.VerifyVerifyEmailMessage();

            userProfilePage.Logout();

            test.Log(Status.Pass, "User registered and correct message shown");
        }

        [Test]
        [Description("Verifies the email address of the user")]
        public void VerifyEmailAddress()
        {
            test = extent.CreateTest("Verify users email address");
            
            base.VerifyEmailAddress(UserEmail);

            test.Log(Status.Pass, "Users email address verified");
        }

        [Test]
        [Description("Logs in as user again and takes a course, verifies that the certificate is not shown and message is shown at the end of the course")]
        public void TakeCourseUA()
        {
            test = extent.CreateTest("User takes and completes course, verifies that the certificate of completion is not shown and correct message is shown");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            loginPage.Login(UserEmail, "Password1");

            var profilePage = TestFactory.New<UserProfilePage>();

            var coursePage = profilePage.StartCourse();

            coursePage.CompleteCourseUA();

            test.Log(Status.Pass, "Course completed and correct message shown");
        }

        [Test]
        [Description("Logs in as CA and rejects the user leaving a comment")]
        public void RejectUser()
        {
            test = extent.CreateTest("Login as Company Admin and reject the user");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            string password =  _testConfig.UserPassword;
            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var userApprovalPage = dashboard.GoToUserApproval();
            var userApprovalReview = userApprovalPage.SearchAndReview(UserEmail);

            userApprovalReview.RejectUser("Unfortunately you do not meet our requirements");

            dashboard.Logout();

            test.Log(Status.Pass, "User rejected");
        }

        [Test]
        [Description("Logs back in as learner and clicks the 'I fixed it' button to go back to pending")]
        public void UserFixIssues()
        {
            test = extent.CreateTest("Login as learner and clicks the 'I fixed it' button, verifies correct message is shown");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            loginPage.Login(UserEmail, "Password1");

            var profilePage = TestFactory.New<UserProfilePage>();

            profilePage.CheckRejectionMessage();

            profilePage.ClickIFixedItAndVerifyMessage();

            profilePage.Logout();

            test.Log(Status.Pass, "Button pressed and status updated");
        }

        [Test]
        [Description("Logs back in as CA and approves the user")]
        public void ApproveUser()
        {
            test = extent.CreateTest("Login as Company Admin and approves the user");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            string password =  _testConfig.UserPassword;
            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var userApprovalPage = dashboard.GoToUserApproval();
            var userApprovalReview = userApprovalPage.SearchAndReview(UserEmail);

            userApprovalReview.ApproveUser();

            dashboard.Logout();

            test.Log(Status.Pass, "User approved");

            extent.Flush();
        }

        [Test]
        [Description("Logs in as Learner and upload new mandatory doc")]
        public void ReUploadMandatoryDocAsLearner()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            loginPage.Login(UserEmail, "Password1");

            var userProfile = TestFactory.New<UserProfilePage>();

            userProfile.UploadToMyDocuments("");
        }
    }
}