﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace InitiafyTests
{
    public class ModifiedUserDialog : BaseTests
    {
        public ModifiedUserDialog(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement LastNameField => Driver.FindElement(By.XPath("//*[@id='lastName']"));

        private IWebElement FirstNameField => Driver.FindElement(By.CssSelector("#firstName"));

        private IWebElement EmailField => Driver.FindElement(By.XPath("//*[@id='email']"));

        private IWebElement PhoneField => Driver.FindElement(By.XPath("//*[@id='phone']"));

        public IWebElement SaveButton => Driver.FindElement(By.XPath("//*[@class='btn btn-save btn-block ng-scope']"));

        public IWebElement CancelButton => Driver.FindElement(By.XPath("//*[@class='btn btn-cancel btn-block ng-scope']"));

        private IWebElement StatusAndComments => Driver.FindElement(By.CssSelector("a.undecorated-link.collapsed > strong"));

        internal void UpdateDetails(string firstName, string lastName, string email, string phone)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            //waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@name='firstName']")));
            Thread.Sleep(1500);
            StatusAndComments.Click();

            FirstNameField.Clear();
            FirstNameField.SendKeys(firstName);

            LastNameField.Clear();
            LastNameField.SendKeys(lastName);

            EmailField.Clear();
            EmailField.SendKeys(email);

            PhoneField.Clear();
            PhoneField.SendKeys(phone);

            SaveButton.Click();
        }

        internal void Close()
        {
            Thread.Sleep(1500);
            CancelButton.Click();
            Thread.Sleep(1500);
        }
    }
}