﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Tests the They Pay Feature and checks CM can only add users having licenses")]
    public class TheyPayTests : BaseTests
    {
        private string CMEmail = "";

        public TheyPayTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "8. Contractor Payment Feature Tests";

        public override void RunAll()
        {
            this.StartReport("They Pay Tests");
            this.LoginAsSA();
            this.SearchForCompany();
            this.TurnOnContractorPaymentFeature();
            this.LoginAsCA();
            this.AddATheyPayCompany();
            this.AddLicensesToTheyPayCompany();
            this.AddsUsersAsCMToTheyPay();
            this.TurnOffContractorPayment();
        }

        public new void LoginAsSA()
        {
            GreenMessage("Logging in as the SA");

            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }

        [Test]
        [Description("Searchs for company and clicks to go through to dashboard")]
        public void SearchForCompany()
        {
            GreenMessage("Searching for the company to go through to dashboard");

            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);
        }

        [Test]
        [Description("Turns on the Contractor Payment feature")]
        public void TurnOnContractorPaymentFeature()
        {
            test = extent.CreateTest("Turn on Contractor Payment");

            GreenMessage("Turning on the Contractor Payment Feature");

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();

            var companyDetailsPage = TestFactory.New<CompanyDetailsPage>();

            companyDetailsPage.TurnOnContractorPayment();

            dashboard.Logout();

            test.Log(Status.Pass, "Contract Payment turned on");
        }

        [Test]
        [Description("Logs in as CA")]
        public void LoginAsCA()
        {
            GreenMessage("Logging in as the CA");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            loginPage.Login(_testConfig.CompanyAdminEmail, password);
        }

        [Test]
        [Description("Creates a They Pay Contract company")]
        public void AddATheyPayCompany()
        {
            test = extent.CreateTest("Create a They Pay contract company");

            GreenMessage("Creating a They Pay contract company");

            var dashboard = TestFactory.New<Dashboard>();
            dashboard.GoToUsers();
            var companiesPage = dashboard.GoToCompanies();

            string timeStamp = GetTimestamp(DateTime.Now);

            CMEmail = "initiafy.test+" + timeStamp + "@gmail.com";

            companiesPage.AddContractCompany("Shane TP", "Shane Cleary", CMEmail, "TheyPay", timeStamp);

            dashboard.Logout();

            test.Log(Status.Pass, "They Pay contract company created");
        }

        [Test]
        [Description("Adds licenses to the They Pay company as SA")]
        public void AddLicensesToTheyPayCompany()
        {
            test = extent.CreateTest("Update the licences for They Pay as Company Admin");

            GreenMessage("Adding licenses to the They Pay company as SA");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);

            dashboard.GoToUsers();

            var contractorsPage = dashboard.GoToCompanies();

            contractorsPage.GoToContractorDashboard("Shane TP");

            var dashboardOverview = TestFactory.New<DashboardOverviewPage>();
            dashboardOverview.SetCredits("3");

            dashboard.Logout();

            test.Log(Status.Pass, "Licenses added to company");
        }

        [Test]
        [Description("Logs in as CM and adds user, checks he can't add more than licenses")]
        public void AddsUsersAsCMToTheyPay()
        {
            test = extent.CreateTest("As Contractor Manager add users until licenses run out, verify error message");

            GreenMessage("Logging in as the CM and adding users, verifies that user cannot be added when licenses run out");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsCM(CMEmail,  _testConfig.UserPassword);

            dashboard.GoToUsers();

            var preRegUsers = TestFactory.New<PreregUsers>();

            preRegUsers.AddUser("Mary", "Heinz", "initiafy.test+tp01@gmail.com");
            preRegUsers.AddUser("Jim", "Heinz", "initiafy.test+tp02@gmail.com");
            preRegUsers.AddUserOverLimit2("Ian", "Heinz", "initiafy.test+tp03@gmail.com");

            dashboard.Logout();

            test.Log(Status.Pass, "Correct error message shown");
        }

        [Test]
        [Description("Logs in as the SA and turns off the contractor payment Feature")]
        public void TurnOffContractorPayment()
        {
            test = extent.CreateTest("Turn off Contractor Payment");

            GreenMessage("Logging in as the SA and turns off the Contractor Management feature");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);

            var companySettings = dashboard.GoToCompanySettings();

            var companyDetailsPage = TestFactory.New<CompanyDetailsPage>();

            companyDetailsPage.TurnOnContractorPayment();

            dashboard.Logout();

            test.Log(Status.Pass, "Contractor Payment turned off");
        }
    }
}