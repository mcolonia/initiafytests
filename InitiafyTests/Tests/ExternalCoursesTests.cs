﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Creates an External course and assigns it to users")]
    internal class ExternalCoursesTests : BaseTests
    {
        private string companyCode = "";
        private string UserEmail = "";

        public ExternalCoursesTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "4. Create External Course Feature Tests";

        public override void RunAll()
        {
            this.StartReport("External Courses Tests");
            this.LoginAsSA();
            this.SearchForCompany();
            this.TurnOnExternalCourses();
            this.CreateExternalCourse();
            //test.AssignExternalCourse(); //Not working
            this.CreateAutoAssignForExternalCourse();
            this.RegisterAsLearner();
            this.VerifyEmailAddress(UserEmail);
            this.MarkAsCompleted();
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public new void LoginAsSA()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }

        [Test]
        [Description("Searches for company and goes through to Dashboard")]
        public void SearchForCompany()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);
        }

        [Test]
        [Description("Turns on the External Courses feature for a company")]
        public void TurnOnExternalCourses()
        {
            test = extent.CreateTest("Turn on External courses feature for company");

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();
            var companyDetailsPage = TestFactory.New<CompanyDetailsPage>();

            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);

            Thread.Sleep(1000);

            companyDetailsPage.TurnOnExternalCourses();

            dashboard.Logout();

            test.Log(Status.Pass, "External courses feature turned on");
        }

        [Test]
        [Description("Creates an External course as Company Admin")]
        public void CreateExternalCourse()
        {
            test = extent.CreateTest("Create an external course as company admin");

            var loginPage = TestFactory.New<LoginPage>();
            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var coursesPage = dashboard.GoToCourses();

            coursesPage.CreateExternalCourse("An External Course", "6 months");

            dashboard.Logout();

            test.Log(Status.Pass, "External course 'An External Course' created");
        }

        [Test]
        [Description("Create auto assign rule for External course")]
        public void CreateAutoAssignForExternalCourse()
        {
            test = extent.CreateTest("Create an auto assign rule for External course");

            var loginPage = TestFactory.New<LoginPage>();
            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var companySettings = dashboard.GoToCompanySettingsAsAdmin();

            var UserRegistrationSettingsPage = companySettings.GoToUserRegistration();

            UserRegistrationSettingsPage.CreateCourseAssignRule("An External Course", "2");

            companySettings.GoToDashboardAsCA();

            dashboard.GoToUsers();

            var contractCompaniesPage = dashboard.GoToCompanies();

            companyCode = contractCompaniesPage.ReadCompanyCode("Company1");

            dashboard.Logout();

            test.Log(Status.Pass, "Auto assign created");
        }

        [Test]
        [Description("Register as learner and check external course is automatically assigned")]
        public void RegisterAsLearner()
        {
            test = extent.CreateTest("Register as learner and check external course is assigned");

            var login = TestFactory.New<LoginPage>();
            login.GoTo();

            var contactDetailsRegistrationPage = login.EnterCompanyCode(companyCode);

            string profilePic = this.GetExternalFilePath("Builder on Phone.jpg");
            string document = this.GetExternalFilePath("A Test document.docx");

            string timeStamp = GetTimestamp(DateTime.Now);

            UserEmail = "initiafy.test+" + timeStamp + "@gmail.com";

            var registrationPage = contactDetailsRegistrationPage.RegisterEmail(UserEmail);

            var userProfile = registrationPage.RegisterCustomFields1("Emma", "Connors", "UK", "IT", "Password1", profilePic, document);

            userProfile.Logout();

            test.Log(Status.Pass, "External course assigned");
        }

        [Test]
        [Description("Login as CA and mark external course as completed")]
        public void MarkAsCompleted()
        {
            test = extent.CreateTest("Login as company admin and mark external course as completed");

            var loginPage = TestFactory.New<LoginPage>();
            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();

            var selfRegUsersTab = dashboard.GoToSelfRegTab();

            var userProfilePage = selfRegUsersTab.SearchForUserAndGoToProfile(UserEmail);

            userProfilePage.MarkAsCompleted();

            userProfilePage.Logout();

            test.Log(Status.Pass, "External course marked as completed");

            extent.Flush();
        }

        //[Test]
        //[Description("Assign External Course to user")]
        //public void AssignExternalCourse()
        //{
        //
        //    Driver.Manage().Window.Maximize();
        //    Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        //    var loginPage = TestFactory.New<LoginPage>();
        //    loginPage.GoTo();
        //    string password = ReadCompanyAdminPassword();

        //    var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

        //    dashboard.GoToUsers();

        //    var selfRegUsers = dashboard.GoToSelfRegTab();

        //    selfRegUsers.ManuallyAssignCourse("initiafy.test+sdf09i@gmail.com");
        //}
    }
}