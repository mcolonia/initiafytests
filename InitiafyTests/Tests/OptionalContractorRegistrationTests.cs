﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace InitiafyTests
{
    [TestFixture]
    [Description("Optional Contractor Registration Tests, creates contract company w/ company code, logs in as CM and adds users")]
    public class OptionalContractorRegistrationTests : BaseTests
    {
        public OptionalContractorRegistrationTests(IWebDriver _driver, TestTextReport testTextReport, TestConfig testConfig) : base(_driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "3. Optional Contractor Registration Feature Tests";

        public override void RunAll()
        {
            this.StartReport("Optional Contractor Registration Tests");
            this.LoginAsSA();
            this.SearchForCompany();
            this.TurnOnOptionalContractorRegistration();
            this.CreateContractCompanyWithoutCompanyCode();
            this.LoginAsContractorManager();
            this.AddUsersOnContractorDeclarationForm();
            this.TurnOffOptionalContractorRegistration();
        }

        [Test]
        [Description("Logs in as SA and verifys")]
        public new void LoginAsSA()
        {
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);
            //Assert.IsTrue(companiesPage.IsVisible, "Initiafy Login page was not visible");
        }

        [Test]
        [Description("Searches for company and goes through to Dashboard")]
        public void SearchForCompany()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);
        }

        [Test]
        [Description("Turns on the Optional Contractor Registration feature for a company")]
        public void TurnOnOptionalContractorRegistration()
        {
            test = extent.CreateTest("Turn on Optional Contractor Registration feature");

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();
            var companyDetailsPage = TestFactory.New<CompanyDetailsPage>();

            companyDetailsPage.TurnOnOptionalContractorRegistration();

            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);

            Thread.Sleep(1000);
            //Actually turning off Contractor Payment
            //companyDetailsPage.TurnOnContractorPayment();

            dashboard.Logout();

            test.Log(Status.Pass, "Feature successfully turned on for company");
        }

        [Test]
        [Description("Creates a contract company that does not have a company code")]
        public void CreateContractCompanyWithoutCompanyCode()
        {
            test = extent.CreateTest("Create contract company without company code");

            var loginPage = TestFactory.New<LoginPage>();
            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();
            var contractorsPage = dashboard.GoToCompanies();

            contractorsPage.AddContractCompanyOpt("Company2", "Shane Cleary", "initiafy.test+34er32@gmail.com");

            dashboard.Logout();

            test.Log(Status.Pass, "Contract company created");
        }

        [Test]
        [Description("Logs in as CM for Contract Company")]
        public void LoginAsContractorManager()
        {
            test = extent.CreateTest("Login as the Contractor Manager for OCR");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();

            //_testTextReport.LogError("Enter the CM password for Optional Contractor Registration");
            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsCM("initiafy.test+34er32@gmail.com", password);

            test.Log(Status.Pass, "Successfully logged in");
        }

        [Test]
        [Description("Adds users using the Contractor Competence Declaration form")]
        public void AddUsersOnContractorDeclarationForm()
        {
            test = extent.CreateTest("As Contractor Manager and user using the Conpetence Declatation form");

            var dashboard = TestFactory.New<Dashboard>();

            dashboard.GoToUsers();
            var preRegUsers = TestFactory.New<PreregUsers>();

            preRegUsers.AddUser("Tom", "Smith", "initiafy.test+sdde23@gmail.com", "Ireland", "IT", "Learner");

            preRegUsers.FillContractorCompetenceDeclaration();

            dashboard.Logout();

            test.Log(Status.Pass, "User successfully added");
        }

        [Test]
        [Description("Turn off optional contractor registration")]
        public void TurnOffOptionalContractorRegistration()
        {
            test = extent.CreateTest("Turn off the optional contractor registration feature");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var companiesPage = loginPage.Login(_testConfig.SuperAdminEmail, _testConfig.SuperAdminPassword);

            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);

            var companySettings = dashboard.GoToCompanySettings();
            var companyDetailsPage = TestFactory.New<CompanyDetailsPage>();

            companyDetailsPage.TurnOnOptionalContractorRegistration();

            dashboard.Logout();

            test.Log(Status.Pass, "Feature turned off");

            extent.Flush();
        }
    }
}