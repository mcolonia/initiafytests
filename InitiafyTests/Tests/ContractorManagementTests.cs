﻿using AventStack.ExtentReports;
using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    public class ContractorManagementTests : BaseTests
    {
        private string CompanyCode = "";
        private string CMPassword = "";
        private string CMEmail = "";

        public ContractorManagementTests(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => "5. Contractor Management Feature Tests";

        public override void RunAll()
        {
            this.StartReport("Contractor Management Tests");
            this.LoginAsSA();
            this.SearchForCompany();
            this.TurnOnContractorManagement();
            this.CreateContractorRegistrationForm();
            this.AddRejectionReasons();
            this.AddContractCompany();
            this.RegisterContractCompany();
            this.RejectCompany();
            this.CM_FixIssues();
            this.ApproveContractor();
            //test.UpdateMandatoryDocument(); //Not working
            //test.EnsureCompanyIsBackInPending(); //Not working
        }

        [Test]
        [Description("Searchs for company in the SA view and click to go through to company dashboard")]
        public void SearchForCompany()
        {
            var companiesPage = TestFactory.New<CompaniesPage>();
            var dashboard = companiesPage.SearchForCompany(_testConfig.CompanyName);
        }

        [Test]
        [Description("Turns on the Contractor Management feature for the company")]
        public void TurnOnContractorManagement()
        {
            test = extent.CreateTest("Turn on User Approval for company");

            var dashboard = TestFactory.New<Dashboard>();
            var companySettings = dashboard.GoToCompanySettings();

            var companyDetails = TestFactory.New<CompanyDetailsPage>();
            companyDetails.TurnOnContractorManagement();

            dashboard.Logout();

            test.Log(Status.Pass, "Contractor Management turned on");
        }

        [Test]
        [Description("Creates the contractor registration form")]
        public void CreateContractorRegistrationForm()
        {
            test = extent.CreateTest("Create contractor registration form");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var companySettings = dashboard.GoToCompanySettingsAsAdmin();

            var contractorRegistrationPage = companySettings.GoToContractorRegistration();

            contractorRegistrationPage.CreateTextField();

            contractorRegistrationPage.CreateDropDownList();

            contractorRegistrationPage.CreatePickList();

            contractorRegistrationPage.CreateDocumentUpload();

            contractorRegistrationPage.SaveChanges();

            test.Log(Status.Pass, "Contractor registration form created");
        }

        [Test]
        [Description("Adds rejection reasons")]
        public void AddRejectionReasons()
        {
            test = extent.CreateTest("Add rejection reason options");

            var contractorRegistationPage = TestFactory.New<ContractorRegistrationPage>();

            contractorRegistationPage.GoToRejectionReasons();

            contractorRegistationPage.RejectionReasonAdd("Reason 1");
            contractorRegistationPage.RejectionReasonAdd("Reason 2");
            contractorRegistationPage.RejectionReasonAdd("Reason 3");
            contractorRegistationPage.RejectionReasonAdd("Reason 4");

            var dashboard = TestFactory.New<Dashboard>();

            dashboard.Logout();

            test.Log(Status.Pass, "Rejection reasons added");
        }

        [Test]
        [Description("Add contract company")]
        public void AddContractCompany()
        {
            test = extent.CreateTest("Add contract company");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            dashboard.GoToUsers();

            var contractCompaniesPage = dashboard.GoToCompanies();

            CompanyCode = GetTimestamp(DateTime.Now);
            CMEmail = "initiafy.test+" + CompanyCode + "@gmail.com";

            contractCompaniesPage.AddContractCompany("company 2404", "Contact Person", CMEmail, CompanyCode);

            dashboard.Logout();

            test.Log(Status.Pass, "Contract company \"company 2404\" created");
        }

        [Test]
        [Description("Registers Contract Company using company registration page")]
        public void RegisterContractCompany()
        {
            test = extent.CreateTest("Register contract company");

            var loginPage = TestFactory.New<LoginPage>();

            //_testTextReport.LogError("Enter the CM Password");
            CMPassword =  _testConfig.UserPassword;

            loginPage.LoginAsCM(CMEmail, CMPassword);

            var companyRegistrationPage = TestFactory.New<CompanyRegistrationPage>();

            var dashboard = companyRegistrationPage.RegisterCompany();

            dashboard.GoToCompanyProfile();

            var companyDocumentsPage = dashboard.GoToCompanyDocuments();

            dashboard.Logout();

            test.Log(Status.Pass, "Contract company \"company 2404\" registered");
        }

        [Test]
        [Description("Login as CA and reject the company")]
        public void RejectCompany()
        {
            test = extent.CreateTest("Reject company as Company Admin");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var contractorManagementPage = dashboard.GoToContractorManagement();

            var reviewDialog = contractorManagementPage.SearchForCompanyAndReview("company 2404");

            reviewDialog.RejectFirstItem();
            reviewDialog.RejectSecondItem();
            reviewDialog.RejectThirdItem();

            reviewDialog.Next();

            reviewDialog.RejectFirstItem();

            reviewDialog.Next();

            reviewDialog.RejectForAnotherReason("Please fix all of the above issues.");

            reviewDialog.Reject();

            dashboard.Logout();

            test.Log(Status.Pass, "Company rejected");
        }

        [Test]
        [Description("Login as CM and update details 'I fixed it'")]
        public void CM_FixIssues()
        {
            test = extent.CreateTest("Login as Contractor Manager and click the 'I fixed it' button to return to pending");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            var dashboard = loginPage.LoginAsCM(CMEmail, CMPassword);

            dashboard.UpdateDetails();

            dashboard.FixIssues();

            dashboard.Logout();

            test.Log(Status.Pass, "Company returned to pending");
        }

        [Test]
        [Description("Login as CA and approve company")]
        public void ApproveContractor()
        {
            test = extent.CreateTest("Login as Company Admin and approve company");

            var loginPage = TestFactory.New<LoginPage>();
            loginPage.GoTo();
            Assert.IsTrue(loginPage.IsVisible);

            string password =  _testConfig.UserPassword;

            var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

            var contractorManagementPage = dashboard.GoToContractorManagement();

            var reviewDialog = contractorManagementPage.SearchForCompanyAndReview("company 2404");

            reviewDialog.Next();
            reviewDialog.Next();

            reviewDialog.Approve();

            dashboard.Logout();

            test.Log(Status.Pass, "Contract company approved");

            extent.Flush();
        }

        //[Test]
        //[Description("Login as CM and update mandatory company document")]
        //public void UpdateMandatoryDocument()
        //{
        //    var loginPage = TestFactory.New<LoginPage>();
        //    loginPage.GoTo();
        //    Assert.IsTrue(loginPage.IsVisible);

        //    var dashboard = loginPage.LoginAsCM(CMEmail, CMPassword);

        //    dashboard.GoToCompanyProfile();
        //    var companyDocumentsPage = dashboard.GoToCompanyDocuments();

        //    companyDocumentsPage.UploadDocument();

        //    dashboard.Logout();
        //}

        //[Test]
        //[Description("Login as CA and ensure company is back in pending and approve once more")]
        //public void EnsureCompanyIsBackInPending()
        //{
        //    var loginPage = TestFactory.New<LoginPage>();
        //    loginPage.GoTo();
        //    Assert.IsTrue(loginPage.IsVisible);

        //    string password =  _testConfig.UserPassword;

        //    var dashboard = loginPage.LoginAsAdmin(_testConfig.CompanyAdminEmail, password);

        //    var contractorManagementPage = dashboard.GoToContractorManagement();

        //    var reviewDialog = contractorManagementPage.SearchForCompanyAndReview("company 567457");

        //    reviewDialog.Next();
        //    reviewDialog.Next();

        //    reviewDialog.Approve();

        //    dashboard.Logout();
        //}
    }
}