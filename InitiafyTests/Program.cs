﻿using InitiafyTests.Helpers;
using InitiafyTests.Tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace InitiafyTests
{
    internal class Program
    {
        private static TestTextReport _testTextReport;
        private static List<Type> _listOfTests;

        private static void Main(string[] args)
        {
            _listOfTests = new List<Type>();

            _testTextReport = new TestTextReport();

            var testConfig = new TestConfig()
            {
                CustomerName = "Test 123",
                CompanyName = "Test 123",
                SuperAdminEmail = "scleary+sa@initiafy.com",
                SuperAdminPassword = "Password1",
                UserPassword = "test123",
                Environment = EnumTestEnvironment.Test,
                CompanyAdminEmail = "initiafy.test+123Admin@gmail.com"
            };

            TestFactory.SetDependencies(_testTextReport, testConfig);

            _listOfTests.Add(typeof(TestPack1));
            //_listOfTests.Add(TestFactory.New<PhoneRegistrationTests>());
            _listOfTests.Add(typeof(LicenseTests));
            //_dictionaryOfTests.Add(3, TestFactory.New<OptionalContractorRegistrationTests>()); //Human Intervention
            _listOfTests.Add(typeof(ExternalCoursesTests));
            _listOfTests.Add(typeof(ContractorManagementTests));
            _listOfTests.Add(typeof(UserArchivingTests));
            _listOfTests.Add(typeof(UserApprovalTests));
            _listOfTests.Add(typeof(TheyPayTests));
            _listOfTests.Add(typeof(CourseResellerTests));
            _listOfTests.Add(typeof(LicenseTests2));
            _listOfTests.Add(typeof(DynamicFieldsTests));
            _listOfTests.Add(typeof(UpdatingUserDetailsTest));
            _listOfTests.Add(typeof(GDPRTests));

            MainMenu();
        }

        private static void MainMenu()
        {
            _testTextReport.LogSuccess("Select which Tests you would like to run. Press 0 to quit");

            try
            {
                //foreach (var test in _dictionaryOfTests.Values)
                //{
                //    _testTextReport.LogError($"  {test.TestName}");
                //}

                //int option = int.Parse(System.Console.ReadLine());

                //IBaseTests selectedTest;

                //if (_dictionaryOfTests.TryGetValue(option, out selectedTest))
                //{
                //    selectedTest.RunAll();
                //}

                foreach (Type type in _listOfTests)
                {
                    try
                    {
                        TestFactory.Driver = GetChromeDriver();

                        var test = TestFactory.New(type);

                        _testTextReport.LogSuccess($"Test - {test.TestName} has been started.");

                        test.RunAll();

                        TestFactory.Driver.Close();

                        _testTextReport.LogSuccess($"Test - {test.TestName} has been finished.");
                    }
                    catch (Exception ex)
                    {
                        TestFactory.Driver.Close();
                        _testTextReport.LogError(ex.Message + "\n" + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                _testTextReport.LogError(ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                _testTextReport.CloseFile();
            }

            Environment.Exit(0);
        }

        public static IWebDriver GetChromeDriver()
        {
            var outPutDirectory = (new object()).GetExternalFilePath();
            return new ChromeDriver(outPutDirectory);
        }
    }
}