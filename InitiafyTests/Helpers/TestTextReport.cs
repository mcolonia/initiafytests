﻿using System;

namespace InitiafyTests.Helpers
{
    public class TestTextReport
    {
        private System.IO.StreamWriter _reportTextLog;

        public TestTextReport()
        {
            var dateNow = DateTime.UtcNow.ToString("yyyy-mm-ddThh-mm-ss");
            _reportTextLog = new System.IO.StreamWriter($"{this.GetExternalFilePath()}\\testReport-{dateNow}.txt");
        }

        public void LogSuccess(string text)
        {
            _reportTextLog.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LogError(string text)
        {
            _reportTextLog.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void CloseFile()
        {
            _reportTextLog.Close();
        }
    }
}