﻿using InitiafyTests.Tests;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Reflection;

namespace InitiafyTests.Helpers
{
    public static class TestFactory
    {
        public static IWebDriver Driver;

        private static TestTextReport _testTextReport;

        private static TestConfig _testConfig;

        public static void SetDependencies(TestTextReport testTextReport, TestConfig testConfig)
        {
            _testTextReport = testTextReport;
            _testConfig = testConfig;
        }

        public static T New<T>() where T : class
        {
            var type = typeof(T);
            return (T)Activator.CreateInstance(type, Driver, _testTextReport, _testConfig);
        }

        public static IBaseTests New(Type type)
        {
            return (IBaseTests)Activator.CreateInstance(type, Driver, _testTextReport, _testConfig);
        }
    }
}