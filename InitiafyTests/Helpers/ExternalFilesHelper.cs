﻿using System.IO;
using System.Reflection;

namespace InitiafyTests.Helpers
{
    public static class ExternalFilesHelper
    {
        public static string GetExternalFilePath(this object input, string fileName)
        {
            return $"{GetExternalFilePath(input)}{fileName}";
        }

        public static string GetExternalFilePath(this object input)
        {
            return $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\ExternalFiles\\";
        }
    }
}