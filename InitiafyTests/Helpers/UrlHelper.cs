﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace InitiafyTests.Helpers
{
    public static class UrlHelper
    {
        public static string MainUrl(EnumTestEnvironment environmentEnum)
        {
            return $"https://{environmentEnum.ToText()}.initiafy.com";
        }

        public static string VerifyEmailURL(EnumTestEnvironment environmentEnum, string email)
        {
            return $"https://{environmentEnum.ToText()}.initiafy.com/Authentication/ConfirmEmail?email={HttpUtility.UrlEncode(email)}&code=1";
        }
    }
}
