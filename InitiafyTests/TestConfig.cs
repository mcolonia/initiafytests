﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitiafyTests
{
    public class TestConfig
    {

        public string SuperAdminEmail { get; set; }

        public string SuperAdminPassword { get; set; }

        public string UserPassword { get; set; }

        public string CompanyAdminEmail { get; set; }

        public EnumTestEnvironment Environment { get; set; }

        public string CompanyName { get; set; }

        public string CustomerName { get; set; }
    }

    public enum EnumTestEnvironment
    {
        Dev,
        Test,
        Marcelo,
        Kelvin,
        MegaTron,
        LoadBalancer
    }

    public static class TestEnvironmentEnumExtension
    {
        public static string ToText(this EnumTestEnvironment value)
        {
            switch(value){
                case EnumTestEnvironment.Dev:
                    return "dev";
                case EnumTestEnvironment.Marcelo:
                    return "marcelo";
                case EnumTestEnvironment.Kelvin:
                    return "kelvin";
                case EnumTestEnvironment.LoadBalancer:
                    return "lb";
                case EnumTestEnvironment.MegaTron:
                    return "megatron";
                default:
                    return "test";
            }
        }
    }
}
