﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace InitiafyTests
{
    public class UserRegistrationSettingsPage : BaseTests
    {
        public UserRegistrationSettingsPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement DepartmentField => Driver.FindElement(By.XPath("(//input[@type='text'])[3]"));

        private IWebElement Location => Driver.FindElement(By.Id("locationChoices"));

        private IWebElement SubmitLocation => Driver.FindElement(By.XPath("(//button[@type='submit'])[2]"));

        private IWebElement Locations => Driver.FindElement(By.CssSelector("div.choice.ng-binding"));

        private IWebElement Department => Driver.FindElement(By.XPath("(//input[@type='text'])[4]"));

        private IWebElement Departments => Driver.FindElement(By.XPath("//div[@id='autoAssignLists']/div/div[2]/div/div/div"));

        private IWebElement SubmitDepartment => Driver.FindElement(By.XPath("(//button[@type='submit'])[4]"));

        private IWebElement DeleteMe => Driver.FindElement(By.XPath("//div[@id='autoAssignLists']/div/div[2]/div/div[2]/div/span"));

        private IWebElement SaveTitleButton => Driver.FindElement(By.XPath("(//button[@type='submit'])[3]"));

        private IWebElement SelectCourse => Driver.FindElement(By.XPath("//*[@ng-model='selectedCourse']"));

        private IWebElement CourseDueSelect => Driver.FindElement(By.XPath("//*[@ng-model='dueInWeeks']"));

        private IWebElement AssignButton => Driver.FindElement(By.XPath("//button[contains(.,'Assign')]"));

        private IWebElement AdditionalDocumentsLink => Driver.FindElement(By.LinkText("Additional Documents"));

        private IWebElement CustomFieldsTab => Driver.FindElement(By.LinkText("Custom Fields"));

        public IWebElement EditButton => Driver.FindElement(By.XPath("//*[@id='customControls']/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/button[1]"));

        public void AddLocation(string location)
        {
            Location.SendKeys(location);
            Thread.Sleep(500);
            SubmitLocation.Click();
            Thread.Sleep(1000);
        }

        internal void AddDepartment(string department)
        {
            Department.SendKeys(department);
            Thread.Sleep(500);
            SubmitDepartment.Click();
            Thread.Sleep(1000);
        }

        internal void DeleteDepartment()
        {
            try
            {
                DeleteMe.Click();
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void UpdateDepartmentTitle(string v)
        {
            DepartmentField.Clear();
            DepartmentField.SendKeys("Role");

            SaveTitleButton.Click();
        }

        internal void CreateCourseAssignRule(string courseTitle, string numberOfWeeks)
        {
            new SelectElement(SelectCourse).SelectByText(courseTitle);

            try
            {
                new SelectElement(CourseDueSelect).SelectByText(numberOfWeeks);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }

            AssignButton.Click();
        }

        internal AdditionalDocuments GoToAdditionalDocuments()
        {
            AdditionalDocumentsLink.Click();
            return TestFactory.New<AdditionalDocuments>();
        }

        internal CustomFieldsPage GoToCustomFields()
        {
            CustomFieldsTab.Click();

            return TestFactory.New<CustomFieldsPage>();
        }

        internal void EditFieldAndMakeDynamic()
        {
            EditButton.Click();
        }
    }
}