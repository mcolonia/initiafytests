﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using System.Threading;

namespace InitiafyTests
{
    public class PasswordResetSuccessPage : BaseTests
    {
        public PasswordResetSuccessPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement ProceedToLoginButton => Driver.FindElement(By.XPath("html/body/div[3]/div/div[3]/div/div/div[2]/a"));

        internal void ProceedToLogin()
        {
            Thread.Sleep(1500);
            ProceedToLoginButton.Click();
        }
    }
}