﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace InitiafyTests
{
    public class RefreshersPage : BaseTests
    {
        public RefreshersPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement WeeksSelect => Driver.FindElement(By.CssSelector("select[name=\"courseSelect\"] > option[value=\"0\"]"));

        private IWebElement SaveRefresher => Driver.FindElement(By.XPath("//div[@id='refreshers']/div/div[3]/div[2]/div/button"));

        private IWebElement Notification => Driver.FindElement(By.XPath("//div[@id='refreshers']/div/div[2]/div/span"));

        private IWebElement CourseSelect => Driver.FindElement(By.Name("courseSelect"));

        internal void CreateRefresher(string course)
        {
            Thread.Sleep(1000);

            new SelectElement(CourseSelect).SelectByText(course);

            CourseSelect.Click();

            WeeksSelect.Click();

            SaveRefresher.Click();

            try
            {
                Assert.AreEqual("Success", Notification.Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }
        }
    }
}