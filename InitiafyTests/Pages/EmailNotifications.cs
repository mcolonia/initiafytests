﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;

namespace InitiafyTests
{
    public class EmailNotifications : BaseTests
    {
        public EmailNotifications(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement DefaultEmail => Driver.FindElement(By.Id("defaultEmail"));

        private IWebElement WeeklyReportEmail => Driver.FindElement(By.XPath("(//input[@id='defaultEmail'])[2]"));

        private IWebElement AddButton => Driver.FindElement(By.XPath("//div[2]/div/div/button"));

        private IWebElement EmailAddress1 => Driver.FindElement(By.CssSelector("div.form-group.small-offset > div.col-sm-6 > #defaultEmail"));

        private IWebElement SaveButton => Driver.FindElement(By.XPath("//div[2]/div/div[2]/button"));

        internal void SetDefaultEmail(string emailAddress)
        {
            DefaultEmail.Clear();
            DefaultEmail.SendKeys(emailAddress);
        }

        internal void SetWeeklyReportEmail(string emailAddress)
        {
            WeeklyReportEmail.Clear();
            WeeklyReportEmail.SendKeys(emailAddress);
        }

        internal void CreateCustomEmailNotification(string emailAddress)
        {
            AddButton.Click();
            EmailAddress1.Clear();
            EmailAddress1.SendKeys("initiafy.test+de1@gmail.com");
        }

        internal void SaveChanges()
        {
            SaveButton.Click();
        }
    }
}