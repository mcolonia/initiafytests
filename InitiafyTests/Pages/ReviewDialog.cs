﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class ReviewDialog : BaseTests
    {
        public ReviewDialog(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement RejectDropdown1 => Driver.FindElement(By.XPath("//*[@id='reviewUserDlg']//select[1]"));
        private IWebElement RejectDropdown2 => Driver.FindElement(By.XPath("//*[@id='reviewUserDlg']//tr[3]/td[3]/select"));
        private IWebElement RejectDropdown3 => Driver.FindElement(By.XPath("//*[@id='reviewUserDlg']//tr[4]/td[3]/select"));

        private IWebElement NextButton => Driver.FindElement(By.XPath("//button[contains(.,'Next')]"));

        private IWebElement RejectForAnotherReasonTextbox => Driver.FindElement(By.XPath("//*[@id='reviewUserDlg']//textarea"));

        private IWebElement RejectButton => Driver.FindElement(By.XPath("(//button[@type='button'])[8]"));

        public IWebElement ApproveButton => Driver.FindElement(By.XPath("(//button[@type='button'])[9]"));

        internal void RejectFirstItem()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("(//button[@type='button'])[7]")));

            new SelectElement(RejectDropdown1).SelectByText("Reason 1");
        }

        internal void RejectSecondItem()
        {
            new SelectElement(RejectDropdown2).SelectByText("Reason 2");
        }

        internal void RejectThirdItem()
        {
            new SelectElement(RejectDropdown3).SelectByText("Reason 3");
        }

        internal void Next()
        {
            Thread.Sleep(1000);
            NextButton.Click();
        }

        internal void RejectForAnotherReason(string text)
        {
            RejectForAnotherReasonTextbox.SendKeys(text);
        }

        internal void Reject()
        {
            Thread.Sleep(1000);
            RejectButton.Click();

            Thread.Sleep(2500);
        }

        internal void Approve()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            Thread.Sleep(1000);
            ApproveButton.Click();

            waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("(//button[@type='button'])[9]")));

            Thread.Sleep(1500);
        }
    }
}