﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Windows.Forms;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    internal class UserProfilePage : BaseTests
    {
        public UserProfilePage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement updatePasswordDialogButton => Driver.FindElement(By.XPath("//*[@class='btn btn-save btn-block']"));

        private IWebElement oldPasswordField => Driver.FindElement(By.Id("oldPassword"));

        private IWebElement newPasswordField => Driver.FindElement(By.Id("newPassword"));

        private IWebElement confirmPasswordField => Driver.FindElement(By.Id("confirmPassword"));

        private IWebElement StartTrainingButton => Driver.FindElement(By.XPath("//button[contains(.,'Start Training')]"));

        private IWebElement TopMenu => Driver.FindElement(By.Id("dLabel"));

        private IWebElement LogoutOption => Driver.FindElement(By.LinkText("Logout"));

        private IWebElement AdminView => Driver.FindElement(By.LinkText("Admin View"));

        private IWebElement UsefulDocumentsTab => Driver.FindElement(By.PartialLinkText("Useful Documents"));

        private IWebElement UploadFileButton => Driver.FindElement(By.XPath("//*[@id='commonDocuments']/div/button"));

        private IWebElement ChooseAFileButton => Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']"));

        private IWebElement UploadFileSaveButton => Driver.FindElement(By.XPath("//*[@class='btn btn-primary btn-block pull-right']"));

        private IWebElement VerificationMessage => Driver.FindElement(By.CssSelector("div.alert.alert-danger"));

        private IWebElement IFixedItButton => Driver.FindElement(By.XPath("//*[@ng-click='resolveRejection()']"));

        private IWebElement LicenseUserButton => Driver.FindElement(By.XPath("//button[contains(.,'License User')]"));

        private IWebElement VerifyLicensed => Driver.FindElement(By.CssSelector(".badge.success"));

        private IWebElement MyDocumentsTab => Driver.FindElement(By.PartialLinkText("My Documents"));

        private IWebElement UploadFileMyDocumentsButton => Driver.FindElement(By.XPath("//*[@id='myDocuments']/div/button"));

        private IWebElement UploadFileDropdown => Driver.FindElement(By.XPath("//*[@id='uploadControl']"));

        private IWebElement ChooseAFileButtonMyDocuments => Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']"));

        private IWebElement ChooseAFileButtonMyDocuments2 => Driver.FindElement(By.XPath("//*[@class='notranslate ng-binding']"));

        private IWebElement ChooseAFileButtonMyDocuments3 => Driver.FindElement(By.XPath("//*[@ng-model='fileName']']"));

        private IWebElement ChooseAFileButtonMyDocuments4 => Driver.FindElement(By.XPath("//button[contains(.,'Choose a File')]"));

        private IWebElement ChooseAFileButtonMyDocuments5 => Driver.FindElement(By.Name("fileupload"));

        private IWebElement ChooseAFileButtonMyDocuments6 => Driver.FindElement(By.CssSelector("#fileupload"));

        private IWebElement ChooseAFileButtonMyDocuments0 => Driver.FindElement(By.XPath("//*[@class='upload-control ng-scope col-sm-8 col-sm-offset-2']"));

        private IWebElement MarkAsCompletedButton => Driver.FindElement(By.XPath("//button[contains(.,'Mark as Completed')]"));

        private IWebElement UpdatePasswordButton => Driver.FindElement(By.XPath("//button[contains(.,'Update password')]"));

        private IWebElement UpdateProfileButton => Driver.FindElement(By.XPath("//button[contains(.,'Update Profile')]"));

        private IWebElement UpdateProfileFirstNameField => Driver.FindElement(By.XPath("//*[@ng-model='firstName']"));

        private IWebElement UpdateProfileLastNameField => Driver.FindElement(By.XPath("//*[@ng-model='lastName']"));

        private IWebElement UpdateProfilePhoneField => Driver.FindElement(By.XPath("//*[@ng-model='phone']"));

        private IWebElement UpdateProfilePasswordField => Driver.FindElement(By.XPath("//*[@ng-model='password']"));

        private IWebElement UpdateProfileSaveButton => Driver.FindElement(By.XPath("//*[@class='btn btn-save btn-block ng-binding']"));

        private IWebElement UpdateLocationRoleButton => Driver.FindElement(By.XPath("/html/body/div[1]/div/div[1]/div[5]/div[1]/div[2]/div/div[8]/div/button"));

        private IWebElement UpdateLocationDepartmentsButton => Driver.FindElement(By.XPath("/html/body/div[1]/div/div[1]/div[5]/div[1]/div[2]/div/div[8]/div/button"));

        private IWebElement LocationSelect => Driver.FindElement(By.XPath("//*[@id=\"updateUserLocationAndRoleDialog\"]/div/div/div[2]/form/div[2]/div/select"));

        private IWebElement RoleSelect => Driver.FindElement(By.XPath("//*[@id=\"updateUserLocationAndRoleDialog\"]/div/div/div[2]/form/div[3]/div/select"));

        private IWebElement DataProtectionButton => Driver.FindElement(By.XPath("//button[contains(.,'Data Protection')]"));

        private IWebElement DataProtectionDialogCloseButton => Driver.FindElement(By.XPath("//*[@ng-click='closeDialog()']"));

        private IWebElement UpdateButton => Driver.FindElement(By.XPath("//*[@id=\"updateUserLocationAndRoleDialog\"]/div/div/div[3]/div/div[1]/button"));

        internal void UpdatePassword(string oldPassword, string newPassword)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(.,'Update password')]")));
            UpdatePasswordButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("oldPassword")));
            oldPasswordField.SendKeys(oldPassword);
            newPasswordField.SendKeys(newPassword);
            confirmPasswordField.SendKeys(newPassword);

            Thread.Sleep(1000);
            updatePasswordDialogButton.Click();

            //should wait for pop up to close
            waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("oldPassword")));
        }

        internal void UpdateProfileName(string firstName, string lastName)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            UpdateProfileButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-model='firstName']")));
            UpdateProfileFirstNameField.Clear();
            UpdateProfileFirstNameField.SendKeys(firstName);

            UpdateProfileLastNameField.Clear();
            UpdateProfileLastNameField.SendKeys(lastName);

            //UpdateProfilePhoneField.Clear();
            //UpdateProfilePhoneField.SendKeys(phone);

            UpdateProfilePasswordField.Clear();
            UpdateProfilePasswordField.SendKeys( _testConfig.UserPassword);

            UpdateProfileSaveButton.Click();

            waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//*[@ng-model='firstName']")));
        }

        internal void CheckDataProtection()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(.,'Data Protection')]")));
            DataProtectionButton.Click();
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-click='closeDialog()']")));
            DataProtectionDialogCloseButton.Click();
        }

        internal void UpdateLocationRole(string Location, string Role)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            try
            {
                UpdateLocationRoleButton.Click();
            }
            catch (Exception)
            {
                UpdateLocationDepartmentsButton.Click();
            }

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"myLocationAndRoleModalLabel\"]")));

            new SelectElement(LocationSelect).SelectByText(Location);
            new SelectElement(RoleSelect).SelectByText(Role);

            UpdateButton.Click();
        }

        internal void Logout()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("dLabel")));
            Thread.Sleep(1000);
            TopMenu.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Logout")));
            LogoutOption.Click();
        }

        internal CoursePage StartCourse()
        {
            Thread.Sleep(1000);
            StartTrainingButton.Click();

            return TestFactory.New<CoursePage>();
        }

        internal Dashboard ReturnToAdminView()
        {
            TopMenu.Click();
            Thread.Sleep(1000);

            AdminView.Click();
            Thread.Sleep(1000);

            return TestFactory.New<Dashboard>();
        }

        internal void UploadUsefulDocument(string usefulDocument)
        {
            UsefulDocumentsTab.Click();
            Thread.Sleep(1000);

            UploadFileButton.Click();

            //Waiting for the dialog to appear, should find better solution.
            Thread.Sleep(1500);
            Thread.Sleep(2500);
            ChooseAFileButton.Click();

            try
            {
                Thread.Sleep(1000);
                SendKeys.SendWait(usefulDocument);
                SendKeys.SendWait(@"{Enter}");
                Thread.Sleep(3000);
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Didn't work :( " + e.Message);
            }

            UploadFileSaveButton.Click();
        }

        internal void VerifyVerifyEmailMessage()
        {
            try
            {
                Assert.AreEqual("Attention You must verify your email address to continue. Please check your inbox to do so. Click here to resend verification email.", VerificationMessage.Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError("Message not found " + e.Message);
                Console.ReadLine();
            }
        }

        internal void CheckRejectionMessage()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.col-sm-10 > p > strong")));
            try
            {
                Assert.AreEqual("There is a problem with your profile", Driver.FindElement(By.CssSelector("div.col-sm-10 > p > strong")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void MarkAsCompleted()
        {
            MarkAsCompletedButton.Click();
        }

        internal void ClickIFixedItAndVerifyMessage()
        {
            IFixedItButton.Click();

            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[5]/div/div/p")));
            try
            {
                Assert.AreEqual("Great! Your supervisor will review your profile and be in touch soon.", Driver.FindElement(By.XPath("//div[5]/div/div/p")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void LicenceUser()
        {
            LicenseUserButton.Click();
            VerifyLicensed.Click();
        }

        internal void UploadToMyDocuments(string v)
        {
            MyDocumentsTab.Click();

            Thread.Sleep(1000);
            UploadFileMyDocumentsButton.Click();

            Thread.Sleep(1000);
            new SelectElement(UploadFileDropdown).SelectByText("Doc 1");

            try
            {
                Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']")).Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }

            try
            {
                ChooseAFileButtonMyDocuments0.Click();
                ChooseAFileButtonMyDocuments.Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }

            try
            {
                ChooseAFileButtonMyDocuments0.Click();
                ChooseAFileButtonMyDocuments.Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }

            try
            {
                ChooseAFileButtonMyDocuments2.Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }

            try
            {
                ChooseAFileButtonMyDocuments3.Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }

            try
            {
                ChooseAFileButtonMyDocuments4.Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }

            try
            {
                ChooseAFileButtonMyDocuments5.Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }

            try
            {
                ChooseAFileButtonMyDocuments6.Click();
            }
            catch (Exception)
            {
                _testTextReport.LogError("Didn't Work");
                Console.ReadLine();
            }
        }
    }
}