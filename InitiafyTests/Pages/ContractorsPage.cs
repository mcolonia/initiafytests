﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class ContractorsPage : BaseTests
    {
        public ContractorsPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement CompanyCode => Driver.FindElement(By.Id("code"));
        private IWebElement ContactEmail => Driver.FindElement(By.Id("contractorEmail"));
        private IWebElement ContactPerson => Driver.FindElement(By.Id("contactPerson"));
        private IWebElement ContractorName => Driver.FindElement(By.Id("contractorName"));
        private IWebElement PaymentModel => Driver.FindElement(By.Id("contractorType"));
        private IWebElement ButtonAdd => Driver.FindElement(By.XPath("//*[@class='btn btn-add']"));

        private IWebElement CompanySearchField => Driver.FindElement(By.XPath("//*[@ng-model='contractorSearch']"));

        private IWebElement LicenseLimitTextBox => Driver.FindElement(By.XPath("//*[@id='licenseLimit']"));

        private IWebElement EditContractorSaveButton => Driver.FindElement(By.XPath("//*[@class='btn btn-block btn-save']"));

        private IWebElement AllowSelfRegistrationCheckbox => Driver.FindElement(By.XPath("//*[@ng-model='allowSelfRegistration']"));

        private IWebElement CodeTd => Driver.FindElement(By.XPath("//*[@id='inductioncodes']//table/tbody/tr[2]/td[5]"));

        private IWebElement ErrorMessage => Driver.FindElement(By.XPath("//*[@id='inductioncodes']/div/div[1]/div/div/span"));

        private IWebElement CompanyCount => Driver.FindElement(By.XPath("/html/body/div[1]/div/div[2]/div[2]/div[1]/ul/li[3]/a"));

        internal void AddContractCompany(string companyName, string contactPerson, string companyEmail, string code)
        {
            try
            {
                AllowSelfRegistrationCheckbox.Click();
            }
            catch (Exception)
            {
                //_testTextReport.LogError("Optional Contractor Registration must be off");
            }

            ContractorName.Clear();
            ContractorName.SendKeys(companyName);

            ContactPerson.Clear();
            ContactPerson.SendKeys(contactPerson);

            ContactEmail.Clear();
            ContactEmail.SendKeys(companyEmail);

            CompanyCode.Clear();
            CompanyCode.SendKeys(code);

            ButtonAdd.Click();
        }

        internal void AddContractCompanyOpt(string companyName, string contactPerson, string companyEmail)
        {
            ContractorName.SendKeys(companyName);

            ContactPerson.SendKeys(contactPerson);

            ContactEmail.SendKeys(companyEmail);

            ButtonAdd.Click();
        }

        internal string ReadCompanyCode()
        {
            string CompanyCode = CodeTd.Text;

            return CompanyCode;
        }

        internal string ReadCompanyCode(string company)
        {
            Thread.Sleep(2000);

            CompanySearchField.Clear();
            CompanySearchField.SendKeys(company);

            Thread.Sleep(2000);

            string CompanyCode = CodeTd.Text;

            return CompanyCode;
        }

        internal void SearchForCompany(string company)
        {
            Thread.Sleep(2000);

            CompanySearchField.Clear();
            CompanySearchField.SendKeys(company);

            Thread.Sleep(2000);
        }

        internal void SearchAndEdit(string company)
        {
            Thread.Sleep(2000);

            CompanySearchField.Clear();
            CompanySearchField.SendKeys(company);

            Thread.Sleep(1000);
            try
            {
                Driver.FindElement(By.XPath("//*[@title='See registration info: " + company + "']")).Click();
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
                Thread.Sleep(2000);
                Driver.FindElement(By.XPath("//*[@title='See registration info: " + company + "']")).Click();
            }

            Thread.Sleep(1000);
        }

        internal void CheckErrorMessageCheckYourData()
        {
            Thread.Sleep(1000);

            try
            {
                Assert.AreEqual("Error during operation, please check your data and try again.", ErrorMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void CheckErrorMessageValidCompanyCode()
        {
            Thread.Sleep(1000);

            try
            {
                Assert.AreEqual("Contractor was not created. The following characters are not valid for a Company Code", ErrorMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void CheckErrorMessageEmailUsed()
        {
            Thread.Sleep(1000);

            try
            {
                Assert.AreEqual("Sorry, this email is already used.", ErrorMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void GoToContractorDashboard(string company)
        {
            Thread.Sleep(2000);

            CompanySearchField.Clear();
            CompanySearchField.SendKeys(company);

            Driver.FindElement(By.XPath("//td[contains(.,'Shane TP')]")).Click();
        }

        internal void SetLicenseLimit(string limit)
        {
            LicenseLimitTextBox.Clear();
            LicenseLimitTextBox.SendKeys(limit);

            EditContractorSaveButton.Click();

            Thread.Sleep(3000);
        }

        internal void AddContractCompany(string companyName, string contactPerson, string companyEmail, string TheyPay, string code)
        {
            ContractorName.SendKeys(companyName);

            ContactPerson.SendKeys(contactPerson);

            ContactEmail.SendKeys(companyEmail);

            new SelectElement(PaymentModel).SelectByText("They Pay");

            CompanyCode.SendKeys(code);

            ButtonAdd.Click();
        }

        public int GetCompanyCount()
        {
            ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[1]/div/div[2]/div[2]/div[1]/ul/li[3]/a"));

            try
            {
                var text = CompanyCount.Text;

                return int.Parse(text.Replace("Companies (", "").Replace(")", ""));
            }
            catch
            {
                return 0;
            }     
        }

        public void InsureTheNumberOfCompaniesHasNotChanged(int count)
        {
            if (count != GetCompanyCount())
            {
                _testTextReport.LogError("The number of company has changed.");
            }
        }
    }
}