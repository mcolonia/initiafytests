﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace InitiafyTests
{
    public class RemaindersRefreshers : BaseTests
    {
        public RemaindersRefreshers(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public IWebElement Notification => Driver.FindElement(By.XPath("//form[@id='remindersForm']/div/span"));

        public IWebElement RemindEvery => Driver.FindElement(By.XPath("//input[@type='number']"));

        public IWebElement RemindEveryOption => Driver.FindElement(By.XPath("//form[@id='remindersForm']/div[2]/div/div/div/ul/li[5]"));

        public IWebElement SaveButton => Driver.FindElement(By.XPath("//form[@id='remindersForm']/div[4]/div/div/button"));

        public IWebElement StartReminding => Driver.FindElement(By.XPath("(//input[@type='number'])[2]"));

        public IWebElement RefreshersLink => Driver.FindElement(By.LinkText("Refreshers"));

        internal void SetReminder(string startReminding, string remindEvery)
        {
            Thread.Sleep(1500);

            StartReminding.Clear();
            StartReminding.SendKeys("4");

            RemindEvery.Click();
            RemindEveryOption.Click();

            SaveButton.Click();

            try
            {
                Assert.AreEqual("Success", (Notification).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal RefreshersPage GoToRefreshers()
        {
            RefreshersLink.Click();
            return TestFactory.New<RefreshersPage>();
        }
    }
}