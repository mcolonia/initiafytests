﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace InitiafyTests
{
    public class ReportsOverviewPage : BaseTests
    {
        public ReportsOverviewPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement creditsDisplay => Driver.FindElement(By.CssSelector("div.col-md-12 > div.ng-binding"));

        internal void CheckRemainingLicenses(string number)
        {
            //Check that the credits has updated
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if ("Remaining licenses: " + number == creditsDisplay.Text) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }
        }
    }
}