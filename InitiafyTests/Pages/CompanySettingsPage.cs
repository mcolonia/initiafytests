﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class CompanySettingsPage : BaseTests
    {
        public CompanySettingsPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement userRegistration => Driver.FindElement(By.LinkText("User Registration"));

        private IWebElement CompanyDetails => Driver.FindElement(By.LinkText("Company Details"));

        private IWebElement RemindersRefreshersLink => Driver.FindElement(By.LinkText("Reminders & Refreshers"));

        private IWebElement EmailNotificationsLink => Driver.FindElement(By.LinkText("Email Notifications"));

        private IWebElement TopMenu => Driver.FindElement(By.Id("dLabel"));

        private IWebElement AdminView => Driver.FindElement(By.LinkText("Admin View"));

        private IWebElement SecuritySettings => Driver.FindElement(By.LinkText("Security Settings"));

        private IWebElement Licenses => Driver.FindElement(By.LinkText("Licenses"));

        private IWebElement BackChevron => Driver.FindElement(By.CssSelector(".glyphicon.back-shevron"));

        private IWebElement ContractorRegistrationLink => Driver.FindElement(By.LinkText("Contractor Registration"));

        private IWebElement CompanyDashboardOption => Driver.FindElement(By.LinkText("Company Dashboard"));

        private IWebElement DataProtection => Driver.FindElement(By.LinkText("Data Protection"));

        internal UserRegistrationSettingsPage GoToUserRegistration()
        {
            //A hack to get to user registration by clicking the Page Up button (out of reach otherwise)
            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);

            Thread.Sleep(1000);

            userRegistration.Click();
            return TestFactory.New<UserRegistrationSettingsPage>();
        }

        internal LicensesPage GoToLicenses()
        {
            //Scroll up
            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);
            Thread.Sleep(1000);
            Licenses.Click();
            return TestFactory.New<LicensesPage>();
        }

        internal DataProtectionPage GoToDataProtection()
        {
            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);
            Thread.Sleep(1000);
            DataProtection.Click();
            return TestFactory.New<DataProtectionPage>();
        }

        internal CompanyDetailsPage GoToCompanyDetails()
        {
            Thread.Sleep(1000);
            CompanyDetails.Click();
            return TestFactory.New<CompanyDetailsPage>();
        }

        internal SecuritySettingsPage GoToSecuritySettings()
        {
            //Scroll up
            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);
            Thread.Sleep(1000);
            SecuritySettings.Click();
            return TestFactory.New<SecuritySettingsPage>();
        }

        internal ContractorRegistrationPage GoToContractorRegistration()
        {
            ContractorRegistrationLink.Click();
            return TestFactory.New<ContractorRegistrationPage>();
        }

        internal RemaindersRefreshers GoToRemindersRefreshers()
        {
            RemindersRefreshersLink.Click();

            return TestFactory.New<RemaindersRefreshers>();
        }

        internal EmailNotifications GoToEmailNotifications()
        {
            EmailNotificationsLink.Click();
            return TestFactory.New<EmailNotifications>();
        }

        internal Dashboard GoToDashboard()
        {
            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);

            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".glyphicon.back-shevron")));
            BackChevron.Click();

            return TestFactory.New<Dashboard>();
        }

        internal Dashboard GoToDashboardAsCA()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("dLabel")));
            TopMenu.Click();
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Company Dashboard")));
            CompanyDashboardOption.Click();

            return TestFactory.New<Dashboard>();
        }
    }
}