﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Windows.Forms;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class CourseEditor : BaseTests
    {
        public CourseEditor(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement AddNewChapterButton => Driver.FindElement(By.CssSelector("button.btn.btn-add"));

        private IWebElement ChapterField => Driver.FindElement(By.Id("chapterTitle"));

        private IWebElement CKEditor => Driver.FindElement(By.CssSelector(".cke_wysiwyg_frame.cke_reset"));

        private IWebElement NewSlideIcon => Driver.FindElement(By.CssSelector("img.chapters-list-icon.ng-scope"));

        private IWebElement Notification => Driver.FindElement(By.CssSelector("span.ng-binding"));

        private IWebElement SlideTitle => Driver.FindElement(By.Name("slideTitle"));

        private IWebElement UploadImageIcon => Driver.FindElement(By.CssSelector("span.cke_button_icon.cke_button__addimage_icon"));

        private IWebElement CreateQuestionSlideIcon => Driver.FindElement(By.XPath("(//img[@alt='Add question'])[2]"));

        private IWebElement SlideSaveButton => Driver.FindElement(By.XPath("(//button[@type='button'])[7]"));

        private IWebElement PublishButton => Driver.FindElement(By.XPath("//button[contains(.,'Publish')]"));

        public override string TestName => throw new NotImplementedException();

        internal void CreateChapter(string title)
        {
            Thread.Sleep(3000);

            ChapterField.Clear();
            ChapterField.SendKeys(title);

            AddNewChapterButton.Click();

            try
            {
                Assert.AreEqual("Chapter Chapter 1 was created.", Notification.Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void CreateSlide(string slideTitle, string slideImage, string slideText)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            Thread.Sleep(1000);

            NewSlideIcon.Click();

            //Waiting for the slide to appear
            Thread.Sleep(2000);
            Thread.Sleep(2000);

            SlideTitle.Clear();
            SlideTitle.SendKeys(slideTitle);

            //tab down to editor
            Driver.FindElement(By.Name("slideTitle")).SendKeys(OpenQA.Selenium.Keys.Tab);

            //Wait for the slowness of the editor
            Thread.Sleep(2000);

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".cke_wysiwyg_frame.cke_reset")));
            IWebElement detailFrame = CKEditor;

            Driver.SwitchTo().Frame(detailFrame);

            var body = Driver.FindElement(By.TagName("body")); // then you find the body

            var executor = Driver as IJavaScriptExecutor;
            executor.ExecuteScript("arguments[0].innerHTML = '<span>" + slideText + "<span>'", body);

            Driver.SwitchTo().ParentFrame();

            //try to upload a file
            try
            {
                Thread.Sleep(1000);
                UploadImageIcon.Click();
                Thread.Sleep(1000);
                SendKeys.SendWait(slideImage);
                SendKeys.SendWait(@"{Enter}");
                Thread.Sleep(3000);
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Didn't work :( " + e.Message);
            }

            SlideSaveButton.Click();

            detailFrame = null;
            //Thread.Sleep(4000);

            waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Name("slideTitle")));
        }

        internal void CreateQuestionSlide(string questionTitle, string question)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            CreateQuestionSlideIcon.Click();
            Thread.Sleep(2000);

            SlideTitle.Clear();
            SlideTitle.SendKeys(questionTitle);

            //tab down to editor
            Driver.FindElement(By.Name("slideTitle")).SendKeys(OpenQA.Selenium.Keys.Tab);

            Thread.Sleep(2000);

            //Code for writing to the text editor
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".cke_wysiwyg_frame.cke_reset")));
            IWebElement detailFrame = CKEditor;

            Driver.SwitchTo().Frame(detailFrame);

            var body = Driver.FindElement(By.TagName("body")); // then you find the body

            var executor = Driver as IJavaScriptExecutor;
            executor.ExecuteScript("arguments[0].innerHTML = '<span>" + question + "<span>'", body);

            Driver.SwitchTo().ParentFrame();

            SlideSaveButton.Click();

            detailFrame = null;

            waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Name("slideTitle")));
        }

        internal void Publish()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(.,'Publish')]")));
            PublishButton.Click();
        }
    }
}