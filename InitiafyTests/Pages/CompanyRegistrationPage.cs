﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Windows.Forms;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class CompanyRegistrationPage : BaseTests
    {
        public CompanyRegistrationPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => throw new NotImplementedException();
        private IWebElement LocationSelect => Driver.FindElement(By.Id("locationSelect"));
        private IWebElement DepartmentSelect => Driver.FindElement(By.Id("departmentSelect"));
        private IWebElement PhoneField => Driver.FindElement(By.Id("phone"));
        private IWebElement TextField => Driver.FindElement(By.XPath(".//*[@placeholder='Text Field']"));
        private IWebElement DropList => Driver.FindElement(By.XPath(".//*[@ng-options='i for i in control.parameters.items']"));
        private IWebElement CheckBox1 => Driver.FindElement(By.XPath(".//*[@class='col-xs-12 im-checkbox ng-scope']/div/label[1]"));
        private IWebElement Agreement => Driver.FindElement(By.XPath("//label[@for='eulaAcceptedCheckbox']"));
        private IWebElement SubmitButton => Driver.FindElement(By.XPath("//button[@type='submit']"));
        private IWebElement GDPRCheckBox => Driver.FindElement(By.XPath("//label[@for='gdprAcceptedCheckbox']"));

        internal Dashboard RegisterCompany()
        {
            new SelectElement(LocationSelect).SelectByText("Not Applicable");

            new SelectElement(DepartmentSelect).SelectByText("Not Applicable");

            //Custom Fields
            try
            {
                TextField.SendKeys("Some sample text");

                new SelectElement(DropList).SelectByText("Drop 1");

                CheckBox1.Click();

                //Pic, and doc uploads
                try
                {
                    Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']")).Click();
                    Thread.Sleep(1000);
                    SendKeys.SendWait(this.GetExternalFilePath("A Test document.docx"));
                    SendKeys.SendWait(@"{Enter}");

                    Thread.Sleep(1500);
                }
                catch (Exception)
                {
                    _testTextReport.LogError("Couldn't upload Company Document");
                }
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Something went wrong filling out the custom fields. " + e.Message);
            }

            Agreement.Click();

            SubmitButton.Click();

            try
            {
                WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("strong")));
                Assert.AreEqual("Your company is currently awaiting approval. Once you've been approved, you will receive an email with further instructions.", Driver.FindElement(By.CssSelector("strong")).Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }

            return TestFactory.New<Dashboard>();
        }

        internal Dashboard RegisterCompanyGDPR()
        {
            new SelectElement(LocationSelect).SelectByText("Not Applicable");

            new SelectElement(DepartmentSelect).SelectByText("Not Applicable");

            GDPRCheckBox.Click();

            Agreement.Click();

            SubmitButton.Click();

            try
            {
                WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("strong")));
                Assert.AreEqual("Your company is currently awaiting approval. Once you've been approved, you will receive an email with further instructions.", Driver.FindElement(By.CssSelector("strong")).Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }

            return TestFactory.New<Dashboard>();
        }
    }
}