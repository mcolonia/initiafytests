﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;

namespace InitiafyTests
{
    public class SecuritySettingsPage : BaseTests
    {
        public SecuritySettingsPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public IWebElement UpdateButton => Driver.FindElement(By.XPath("//*[@id='securitySettingsForm']/div[4]/button"));

        public IWebElement ValidUsersEmailsCheckBox => Driver.FindElement(By.XPath("//label[@for='validateEmailCheckbox']"));

        internal void ValidateUsersEmails()
        {
            ValidUsersEmailsCheckBox.Click();
            UpdateButton.Click();
        }
    }
}