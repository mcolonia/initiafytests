﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Threading;

namespace InitiafyTests
{
    public class AdditionalDocuments : BaseTests
    {
        public AdditionalDocuments(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public override string TestName => throw new NotImplementedException();
        private IWebElement AddButton => Driver.FindElement(By.XPath("//*[@id='additionalDocuments']/div/div[3]/div/div/form/div/div[1]/button"));

        private IWebElement AddControlAddButton => Driver.FindElement(By.XPath("(//button[@type='button'])[11]"));

        private IWebElement Notification => Driver.FindElement(By.XPath("//div[@id='additionalDocuments']/div/div[2]/div/div/span"));

        private IWebElement SaveButton => Driver.FindElement(By.XPath("//div[@id='additionalDocuments']/div/div[3]/div/div/form/div/div[2]/button"));

        private IWebElement TitleField => Driver.FindElement(By.XPath("(//input[@name='title'])[2]"));

        internal void CreateAdditionalDocument(string documentName)
        {
            try
            {
                WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

                Thread.Sleep(1500);

                //waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='btn btn-block btn-add ng-scope']")));
                AddButton.Click();

                //Wait for pop up, then add the additional document
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("(//input[@name='title'])[2]")));

                TitleField.Clear();
                TitleField.SendKeys(documentName);

                AddControlAddButton.Click();

                Thread.Sleep(1000);

                //Wait for dialog to close then save changes
                Thread.Sleep(1000);

                SaveButton.Click();

                try
                {
                    Assert.AreEqual("Success", (Notification).Text);
                }
                catch (AssertionException e)
                {
                    _testTextReport.LogError(e.Message);
                }

                Thread.Sleep(1500);
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Something went wrong \n" + e.Message);
                Console.ReadLine();
            }
        }
    }
}