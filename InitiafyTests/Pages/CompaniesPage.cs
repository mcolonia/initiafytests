﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace InitiafyTests
{
    public class CompaniesPage : BaseTests
    {
        public CompaniesPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement row => Driver.FindElement(By.CssSelector("td.clickable.ng-binding"));

        private IWebElement removeCompanyButton => Driver.FindElement(By.XPath("//*[@ng-click='deleteCompany(company, $event)']"));

        private IWebElement confirmDeleteCompany => Driver.FindElement(By.XPath("//*[@id='confirmationDialog']/div/div/div[3]/div/div[1]/button"));

        private IWebElement searchBox => Driver.FindElement(By.XPath("//*[@ng-model='companySearch']"));

        private IWebElement addCompany => Driver.FindElement(By.XPath("//button[contains(.,'Add Company')]"));

        private IWebElement companyName => Driver.FindElement(By.Name("companyName"));

        private IWebElement CustomerName => Driver.FindElement(By.XPath("//*[@placeholder='Customer']"));

        private IWebElement CustomerType => Driver.FindElement(By.XPath("//select"));

        private bool IsVisible => Driver.Title.Contains("Home Page - Initiafy");

        private IWebElement CompanyEditIcon => Driver.FindElement(By.XPath("//*[@title='Edit company: Test 123']"));

        internal void CreateCompany(string customerName, string CompanyTitle)
        {
            CustomerName.SendKeys(customerName);
            companyName.SendKeys(CompanyTitle);
            //Select licence type
            new SelectElement(CustomerType).SelectByText("Enterprise");
            addCompany.Click();
        }

        internal void RemoveCompany(string customerName, string CompanyTitle)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            bool HasItFoundTheCompany = false;
            searchBox.SendKeys(CompanyTitle);

            //Waiting for the company to appear in the search
            for (int second = 0; ; second++)
            {
                if (second >= 20) Assert.Fail("timeout");
                try
                {
                    if (CompanyTitle == row.Text)
                    {
                        HasItFoundTheCompany = true;
                        break;
                    }
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }

            if (HasItFoundTheCompany)
            {
                try
                {
                    removeCompanyButton.Click();
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    confirmDeleteCompany.Click();
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }
                catch(Exception ex)
                {
                    RedMessage(ex.Message);
                }
            }            
        }

        internal void CreateCompany2(string CompanyTitle)
        {
            CustomerName.SendKeys("Test 12");

            Driver.FindElement(By.XPath("//*[@val='Test 123']")).Click();

            companyName.SendKeys(CompanyTitle);
            //Select licence type
            new SelectElement(CustomerType).SelectByText("Enterprise");
            addCompany.Click();
        }

        internal Dashboard SearchForCompany2(string CompanyName)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            searchBox.SendKeys(CompanyName);

            //Waiting for the company to appear in the search
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (CompanyName == row.Text) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }

            Driver.FindElement(By.XPath("//td[contains(.,'Test 321')]")).Click();
            //row.Click();

            return TestFactory.New<Dashboard>();
        }

        internal Dashboard SearchForCompany(string CompanyName)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            searchBox.Clear();
            searchBox.SendKeys(CompanyName);

            //Waiting for the company to appear in the search
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (CompanyName == row.Text) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }

            row.Click();

            return TestFactory.New<Dashboard>();
        }

        internal CompanySettingsPage SearchForCompanyAndGoToCompanySettings(string CompanyName)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            Thread.Sleep(1000);
            searchBox.SendKeys(CompanyName);

            //Waiting for the company to appear in the search
            for (int second = 0; ; second++)
            {
                if (second >= 60) Assert.Fail("timeout");
                try
                {
                    if (CompanyName == row.Text) break;
                }
                catch (Exception)
                { }
                Thread.Sleep(1000);
            }

            CompanyEditIcon.Click();

            return TestFactory.New<CompanySettingsPage>();
        }
    }
}