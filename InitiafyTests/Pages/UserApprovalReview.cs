﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class UserApprovalReview : BaseTests
    {
        public UserApprovalReview(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public IWebElement NextButton => Driver.FindElement(By.XPath("//button[contains(.,'Next')]"));

        public IWebElement RejectButton => Driver.FindElement(By.XPath("(//button[@type='button'])[8]"));

        public IWebElement RejectForAnotherReason => Driver.FindElement(By.XPath("//*[@ng-model='$parent.rejectionSummary']"));

        public IWebElement ApproveButton => Driver.FindElement(By.XPath("(//button[@type='button'])[9]"));

        internal void RejectUser(string comment)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#reviewUserDlg > div.modal-dialog > div.modal-content > div.row.row-header > div.col-md-12.text-center > p.text-center")));
            try
            {
                Assert.AreEqual("User Approval Review", Driver.FindElement(By.CssSelector("#reviewUserDlg > div.modal-dialog > div.modal-content > div.row.row-header > div.col-md-12.text-center > p.text-center")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }

            NextButton.Click();
            Thread.Sleep(1500);

            NextButton.Click();
            Thread.Sleep(1500);

            NextButton.Click();
            Thread.Sleep(1500);

            RejectForAnotherReason.SendKeys(comment);

            RejectButton.Click();

            _testTextReport.LogError("Waiting for Dialog to close");
            Thread.Sleep(5000);
        }

        internal void ApproveUser()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#reviewUserDlg > div.modal-dialog > div.modal-content > div.row.row-header > div.col-md-12.text-center > p.text-center")));
            try
            {
                Assert.AreEqual("User Approval Review", Driver.FindElement(By.CssSelector("#reviewUserDlg > div.modal-dialog > div.modal-content > div.row.row-header > div.col-md-12.text-center > p.text-center")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }

            Thread.Sleep(1000);
            NextButton.Click();
            Thread.Sleep(2500);

            NextButton.Click();
            Thread.Sleep(1500);

            NextButton.Click();
            Thread.Sleep(1500);

            ApproveButton.Click();

            _testTextReport.LogError("Waiting for Dialog to close");
            Thread.Sleep(5000);
        }
    }
}