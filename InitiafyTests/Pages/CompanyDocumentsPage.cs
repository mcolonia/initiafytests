﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    internal class CompanyDocumentsPage : BaseTests
    {
        public CompanyDocumentsPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement ChooseAFileButton => Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']"));

        private IWebElement DocumentSelect => Driver.FindElement(By.XPath("//*[@id='uploadControl']"));

        private IWebElement UploadFileButton => Driver.FindElement(By.XPath("//div[@id='companyDocuments']/div/button']"));

        internal void UploadDocument()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            try
            {
                Thread.Sleep(1000);
                waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@id='companyDocuments']/div/button']")));
                UploadFileButton.Click();

                waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//*[@id='uploadControl']]")));
                DocumentSelect.Click();

                waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']")));
                new SelectElement(DocumentSelect).SelectByText("Company Doc");

                Thread.Sleep(1000);
                ChooseAFileButton.Click();
            }
            catch (Exception ex)
            {
                _testTextReport.LogError($"That failed, Upload file and click enter {ex.Message}");
                Console.ReadLine();
            }
        }
    }
}