﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class ContractorRegistrationPage : BaseTests
    {
        public ContractorRegistrationPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement ControlType => Driver.FindElement(By.Name("controlType"));

        private IWebElement AddControlButton => Driver.FindElement(By.XPath("//*[@class='btn btn-add ng-scope']"));

        private IWebElement ControlTitle => Driver.FindElement(By.Name("title"));

        private IWebElement HelpTextField => Driver.FindElement(By.Name("helpText"));

        private IWebElement SaveAddControlButton => Driver.FindElement(By.XPath("(//button[@type='button'])[6]"));

        private IWebElement DropListItem => Driver.FindElement(By.Name("dropListItem"));

        private IWebElement ButtonPlus => Driver.FindElement(By.XPath("//*[@class='btn btn-add btn-plus']"));

        private IWebElement PickListItem => Driver.FindElement(By.Name("pickListItem"));

        private IWebElement SaveFormButton => Driver.FindElement(By.XPath("//*[@ng-click='saveCustomControls()']"));

        private IWebElement RejectionReasonsTab => Driver.FindElement(By.LinkText("Rejection Reasons"));

        private IWebElement RejectionReasonTextbox => Driver.FindElement(By.Id("locationChoices"));

        private IWebElement RejectionReasonAddButton => Driver.FindElement(By.XPath("//*[@type='submit']"));

        public override string TestName => throw new NotImplementedException();

        internal void CreateTextField()
        {
            new SelectElement(ControlType).SelectByText("Text");

            AddControlButton.Click();

            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));
            ControlTitle.Clear();
            ControlTitle.SendKeys("Text Field");

            HelpTextField.Clear();
            HelpTextField.SendKeys("Text Field");

            SaveAddControlButton.Click();

            Thread.Sleep(1500);
        }

        internal void CreateDropDownList()
        {
            new SelectElement(ControlType).SelectByText("Drop list");

            AddControlButton.Click();

            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));
            ControlTitle.Clear();
            ControlTitle.SendKeys("Drop List");

            DropListItem.Clear();
            DropListItem.SendKeys("Drop 1");
            ButtonPlus.Click();

            DropListItem.Clear();
            DropListItem.SendKeys("Drop 2");
            ButtonPlus.Click();

            SaveAddControlButton.Click();

            Thread.Sleep(1500);
        }

        internal void CreatePickList()
        {
            new SelectElement(ControlType).SelectByText("Pick list");

            AddControlButton.Click();

            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));
            ControlTitle.Clear();
            ControlTitle.SendKeys("Pick List");

            PickListItem.Clear();
            PickListItem.SendKeys("Item 1");
            ButtonPlus.Click();

            PickListItem.Clear();
            PickListItem.SendKeys("Item 2");
            ButtonPlus.Click();

            SaveAddControlButton.Click();

            Thread.Sleep(1500);
        }

        internal void RejectionReasonAdd(string RejectionReason)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("locationChoices")));

            RejectionReasonTextbox.Clear();
            RejectionReasonTextbox.SendKeys(RejectionReason);
            RejectionReasonAddButton.Click();

            Thread.Sleep(1000);
        }

        internal void GoToRejectionReasons()
        {
            RejectionReasonsTab.Click();
        }

        internal void CreateDocumentUpload()
        {
            new SelectElement(ControlType).SelectByText("Upload");

            AddControlButton.Click();

            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));
            ControlTitle.Clear();
            ControlTitle.SendKeys("Company Doc");

            SaveAddControlButton.Click();

            Thread.Sleep(1500);
        }

        internal void SaveChanges()
        {
            SaveFormButton.Click();

            //WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            //waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-bind-html='notificationMessage']")));
            //Assert.AreEqual("Success", Driver.FindElement(By.XPath("//*[@ng-bind-html='notificationMessage']")).Text);
        }
    }
}