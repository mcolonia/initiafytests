﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using System.Threading;

namespace InitiafyTests
{
    internal class UserApprovalPage : BaseTests
    {
        public UserApprovalPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public IWebElement ReviewButton => Driver.FindElement(By.XPath("//button[contains(.,'Review')]"));

        public IWebElement SearchBox => Driver.FindElement(By.XPath("//input[@type='text']"));

        internal UserApprovalReview SearchAndReview(string userEmail)
        {
            SearchBox.Clear();
            SearchBox.SendKeys(userEmail);

            Thread.Sleep(1000);

            ReviewButton.Click();

            return TestFactory.New<UserApprovalReview>();
        }
    }
}