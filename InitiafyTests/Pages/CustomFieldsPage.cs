﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class CustomFieldsPage : BaseTests
    {
        public CustomFieldsPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement SaveCustomControlButton => Driver.FindElement(By.XPath("(//button[@type='button'])[6]"));
        private IWebElement AddCustomControlButton => Driver.FindElement(By.XPath("//div[@id='customControls']/div/div[2]/div[2]/div/form/div/div/div/span/button"));
        private IWebElement Helptext => Driver.FindElement(By.Name("helpText"));
        private IWebElement Title => Driver.FindElement(By.Name("title"));
        private IWebElement ControlType => Driver.FindElement(By.Name("controlType"));
        private IWebElement DropListItem => Driver.FindElement(By.Name("dropListItem"));
        private IWebElement DropListItemAddButton => Driver.FindElement(By.XPath("//div[@id='customControls']/div/div[6]/div/div/div[2]/form/div[4]/div/div[2]/button"));
        private IWebElement PickListItem => Driver.FindElement(By.Name("pickListItem"));
        private IWebElement PickListItemAddButton => Driver.FindElement(By.XPath("//div[@id='customControls']/div/div[6]/div/div/div[2]/form/div[4]/div/div[2]/button"));
        private IWebElement BoxesCount => Driver.FindElement(By.Name("boxesCount"));

        private IWebElement SaveFormButton => Driver.FindElement(By.XPath("//div[@id='customControls']/div/div[2]/div[2]/div/form/div/div[2]/button"));

        private IWebElement Department => Driver.FindElement(By.XPath("//*[@class='select2-selection__choice__remove'][1]"));

        private IWebElement Departments => Driver.FindElement(By.Id("select2-roleSelect-results"));

        internal void AddTextField(string title, string helptext)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));

            new SelectElement(ControlType).SelectByText("Text");

            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.Clear();
            Title.SendKeys(title);
            Helptext.Clear();
            Helptext.SendKeys(helptext);

            SaveCustomControlButton.Click();
        }

        internal void AddTextFieldDynamic(string title, string helptext, string department)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));

            new SelectElement(ControlType).SelectByText("Text");

            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.Clear();
            Title.SendKeys(title);
            Helptext.Clear();
            Helptext.SendKeys(helptext);

            Department.Click();

            //new SelectElement(Departments).SelectByText(department);
        }

        internal void AddNumberField(string title)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));

            new SelectElement(ControlType).SelectByText("Number");

            Thread.Sleep(500);
            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.Clear();
            Title.SendKeys(title);

            SaveCustomControlButton.Click();
        }

        internal void AddDropList(string title)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));
            new SelectElement(ControlType).SelectByText("Drop list");

            Thread.Sleep(500);
            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.Clear();
            Title.SendKeys("This is a drop List");

            DropListItem.SendKeys("Drop item 1");
            DropListItemAddButton.Click();
            Thread.Sleep(1000);

            DropListItem.SendKeys("Drop item 2");
            Thread.Sleep(2000);
            DropListItemAddButton.Click();

            DropListItem.SendKeys("Drop item 3");
            Thread.Sleep(1000);
            DropListItemAddButton.Click();

            SaveCustomControlButton.Click();
        }

        internal void AddPickList(string title)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));
            new SelectElement(ControlType).SelectByText("Pick list");

            Thread.Sleep(500);
            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.SendKeys(title);
            PickListItem.Clear();
            PickListItem.SendKeys("Pick item 1");
            PickListItemAddButton.Click();
            PickListItem.SendKeys("Pick item 2");
            PickListItemAddButton.Click();
            PickListItem.SendKeys("Pick item 3");
            PickListItemAddButton.Click();

            SaveCustomControlButton.Click();
        }

        internal void AddDateField(string title)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));
            new SelectElement(ControlType).SelectByText("Date");

            Thread.Sleep(500);
            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.SendKeys(title);

            SaveCustomControlButton.Click();
        }

        internal void AddCharBoxes(string title)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));
            new SelectElement(ControlType).SelectByText("Character boxes");

            Thread.Sleep(500);
            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.Clear();
            Title.SendKeys(title);

            BoxesCount.Clear();
            BoxesCount.SendKeys("5");

            SaveCustomControlButton.Click();
        }

        internal void AddProfilePic(string title)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));
            new SelectElement(ControlType).SelectByText("Upload");

            Thread.Sleep(500);
            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));

            Title.SendKeys(title);

            SaveCustomControlButton.Click();
        }

        internal void AddDocumentUpload(string title, bool isMandatory = true, bool isExpiryDateRequired = false)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("controlType")));

            new SelectElement(ControlType).SelectByText("Upload");

            Thread.Sleep(500);
            AddCustomControlButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Name("title")));
            Title.SendKeys(title);

            new SelectElement(Driver.FindElement(By.Name("uploadType"))).SelectByText("User Document");

            if (isMandatory == false)
            {
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='customControls']/div/div[6]/div/div/div[2]/form/div[7]/div/div/label")));
                Driver.FindElement(By.XPath("//div[@id='customControls']/div/div[6]/div/div/div[2]/form/div[7]/div/div/label")).Click();
            }

            if (isExpiryDateRequired == true)
            {
                //waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("isExpiry")));
                //driver.FindElement(By.Id("isExpiry")).Click();

                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='customControls']/div/div[6]/div/div/div[2]/form/div[4]/div[2]/div/div/label")));
                Driver.FindElement(By.XPath("//div[@id='customControls']/div/div[6]/div/div/div[2]/form/div[4]/div[2]/div/div/label")).Click();
            }

            SaveCustomControlButton.Click();
        }

        internal void SaveChanges()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='customControls']/div/div[2]/div[2]/div/form/div/div[2]/button")));

            Thread.Sleep(500);
            SaveFormButton.Click();
        }
    }
}