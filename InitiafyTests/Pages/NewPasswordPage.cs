﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace InitiafyTests
{
    public class NewPasswordPage : BaseTests
    {
        public NewPasswordPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement confirmButton => Driver.FindElement(By.XPath("//button[@type='submit']"));

        private IWebElement confirmPasswordField => Driver.FindElement(By.Id("confirmPassword"));

        private IWebElement passwordField => Driver.FindElement(By.Id("password"));

        internal PasswordResetSuccessPage CreateNewPassword(string newPassword)
        {
            passwordField.Clear();
            passwordField.SendKeys(newPassword);

            confirmPasswordField.Clear();
            confirmPasswordField.SendKeys(newPassword);

            confirmButton.Click();

            try
            {
                Assert.AreEqual("Congratulations!", Driver.FindElement(By.CssSelector("h4")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }

            Thread.Sleep(1000);

            return TestFactory.New<PasswordResetSuccessPage>();
        }
    }
}