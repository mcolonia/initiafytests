﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using System;

namespace InitiafyTests
{
    public class ResetPasswordPage : BaseTests
    {
        public ResetPasswordPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public IWebElement EmailField => Driver.FindElement(By.Id("email"));

        public IWebElement ResetPasswordButton => Driver.FindElement(By.XPath("//button[@type='submit']"));

        internal NewPasswordPage ResetPassword(string userEmail)
        {
            EmailField.Clear();
            EmailField.SendKeys(userEmail);
            ResetPasswordButton.Click();

            _testTextReport.LogError("Check email and copy reset link here.");
            String link = Console.ReadLine();
            Driver.Navigate().GoToUrl(link);

            return TestFactory.New<NewPasswordPage>();
        }
    }
}