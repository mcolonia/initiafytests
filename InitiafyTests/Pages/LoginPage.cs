﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    internal class LoginPage : BaseTests
    {
        public LoginPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement Submit => Driver.FindElement(By.XPath("//*[@class='btn btn-login control']"));

        private IWebElement Password => Driver.FindElement(By.Id("field2"));

        private IWebElement EmailField => Driver.FindElement(By.XPath("//*[@ng-model='emailOrPhone']"));

        private IWebElement CompanyCode => Driver.FindElement(By.Id("code"));

        private IWebElement RegisterButton => Driver.FindElement(By.XPath("//*[@ng-click='validateCode()']"));

        private IWebElement ForgotPasswordLink => Driver.FindElement(By.LinkText("Forgot Your password?"));

        private IWebElement NoLicensesMessage => Driver.FindElement(By.XPath("//*[@id='loginForm']/div[2]/div/div/span"));

        private IWebElement NoLicensesMessageRegistrationField => Driver.FindElement(By.XPath("//*[@id='codeForm']/div[3]/div/div/span"));

        private IWebElement ChooseButtonOne => Driver.FindElement(By.XPath("//*[@id='ChooseCompanyDialog']//div[1]/div[3]/button"));

        private IWebElement ChooseButtonTwo => Driver.FindElement(By.XPath("//*[@id='ChooseCompanyDialog']//div[2]/div[3]/button"));

        internal void GoTo()
        {
            Driver.Navigate().GoToUrl(UrlHelper.MainUrl(_testConfig.Environment));
        }

        public bool IsVisible
        {
            get
            {
                return Driver.Title.Contains("Login to Initiafy - Initiafy");
            }
            set
            {
            }
        }

        internal CompaniesPage Login(string email, string pass)
        {
            try
            {
                this.Driver.Navigate().Refresh();
                EmailField.SendKeys(email);
                Thread.Sleep(TimeSpan.FromSeconds(1));
                Password.SendKeys(pass);
                Thread.Sleep(TimeSpan.FromSeconds(0.5));
                Submit.Click();
                Thread.Sleep(TimeSpan.FromSeconds(2));
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Problem logging in" + e.Message);
            }

            return TestFactory.New<CompaniesPage>();
        }

        internal ContactDetailsRegistrationPage EnterCompanyCode(string companyCode)
        {
            try
            {
                this.Driver.Navigate().Refresh();
                CompanyCode.Clear();
                CompanyCode.SendKeys(companyCode);

                RegisterButton.Click();
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Something went wrong entering the company code " + e.Message);
                Thread.Sleep(1000);
            }

            return TestFactory.New<ContactDetailsRegistrationPage>();
        }

        internal Dashboard ChooseCompanyTwo()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='ChooseCompanyDialog']//div[1]/div[3]/button")));
            ChooseButtonTwo.Click();

            return TestFactory.New<Dashboard>();
        }

        internal Dashboard ChooseCompanyOne()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='ChooseCompanyDialog']//div[1]/div[3]/button")));
            ChooseButtonOne.Click();

            return TestFactory.New<Dashboard>();
        }

        internal ResetPasswordPage ResetPassword()
        {
            ForgotPasswordLink.Click();
            return TestFactory.New<ResetPasswordPage>();
        }

        internal Dashboard LoginAsCM(string email, string pass)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(6));

            EmailField.SendKeys(email);
            Password.SendKeys(pass);
            Submit.Click();

            try
            {
                //Thread.Sleep(2500);
                //Click button on pop up
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-click='ok()']")));
                Driver.FindElement(By.XPath("//*[@ng-click='ok()']")).Click();
            }
            catch (Exception e)
            {
                _testTextReport.LogError("No Dialog! Check if first time logging in " + e.Message);
            }

            return TestFactory.New<Dashboard>();
        }

        internal void VerifyNoLicencesMessage()
        {
            Thread.Sleep(1000);
            string value = NoLicensesMessage.Text;
            Assert.IsTrue(value.Contains("Access unavailable at this time"), value + " doesn't contain the string");
        }

        internal Dashboard LoginAsAdmin(string email, string pass)
        {
            try
            {
                WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

                EmailField.SendKeys(email);
                Password.SendKeys(pass);
                Submit.Click();
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Error with login " + e.Message + "Click enter to continue!");
                Console.ReadLine();
            }

            return TestFactory.New<Dashboard>();
        }

        internal void EnterCompanyCodeWith0Licences(string code)
        {
            try
            {
                CompanyCode.Clear();
                CompanyCode.SendKeys(code);

                RegisterButton.Click();

                string value = NoLicensesMessageRegistrationField.Text;
                Assert.IsTrue(value.Contains("Access unavailable at this time. Please contact your supervisor."), value + " doesn't contain the string");
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Something went wrong entering the company code " + e.Message);
                Console.ReadLine();
            }
        }
    }//end class
}