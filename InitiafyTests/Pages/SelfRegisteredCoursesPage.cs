﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace InitiafyTests
{
    internal class SelfRegisteredCoursesPage : BaseTests
    {
        public SelfRegisteredCoursesPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public IWebElement BinIcon => Driver.FindElement(By.XPath("(//img[@alt='Delete course'])[4]"));

        public IWebElement ConfirmDeleteButton => Driver.FindElement(By.XPath("(//button[@type='button'])[14]"));

        public IWebElement Notification => Driver.FindElement(By.XPath("//div[@id='contractors-courses']/div/div/div/div/span"));

        internal void DeleteAssignedCourse()
        {
            Thread.Sleep(1000);
            BinIcon.Click();
            Thread.Sleep(1000);

            ConfirmDeleteButton.Click();

            try
            {
                Assert.AreEqual("Deleted", Notification.Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
            }
        }
    }
}