﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace InitiafyTests
{
    public class LicensesPage : BaseTests
    {
        public LicensesPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement EditLicenceSaveButton => Driver.FindElement(By.XPath("//*[@ng-click='updateLicensePack(licensePack)']"));

        private IWebElement LicenceEditButton => Driver.FindElement(By.XPath("//*[@id='licensePacksList']//tbody/tr[1]//button[2]"));

        private IWebElement Year => Driver.FindElement(By.Name("date_[year]"));

        private IWebElement DateDay => Driver.FindElement(By.Name("date_[day]"));

        private IWebElement DateMonth => Driver.FindElement(By.Name("date_[month]"));

        private IWebElement ExpiredStatusIcon => Driver.FindElement(By.XPath("//*[@title='Expired']"));

        private IWebElement GetMoreLicensesButton => Driver.FindElement(By.XPath("//*[@ng-click='addLicensePack()']"));

        private IWebElement LicenseNumberSelect => Driver.FindElement(By.XPath("//*[@ng-model='numberOfLicensesToAdd']"));

        private IWebElement LicensePackSaveButton => Driver.FindElement(By.XPath("//*[@ng-click='saveAddLicensePack()']"));

        private IWebElement LicenseLimitEditButton => Driver.FindElement(By.XPath("//*[@title='Edit']"));

        private IWebElement LicenseLimitTextBox => Driver.FindElement(By.XPath("//*[@ng-model='companyLicenseLimit']"));

        private IWebElement LicenseLimitSaveButton => Driver.FindElement(By.XPath("//*[@ng-click='saveCompanyLicenseLimit()']"));

        private IWebElement AutoRenewSwitch => Driver.FindElement(By.XPath("//*[@title='Turns auto-renew on/off']"));

        private IWebElement ApplyToContractorsSwitch => Driver.FindElement(By.XPath("//*[@title='Turns apply to contractors on/off']"));

        private IWebElement SaveButton => Driver.FindElement(By.XPath("//*[@ng-click='saveAutoRenewConfig()']"));

        internal void SetExpiryDate(string day, string month, string year)
        {
            LicenceEditButton.Click();

            new SelectElement(DateDay).SelectByText(day);
            new SelectElement(DateMonth).SelectByText(month);
            new SelectElement(Year).SelectByText(year);

            Thread.Sleep(1000);

            EditLicenceSaveButton.Click();

            //ExpiredStatusIcon.Click();
        }

        internal void GetMoreLicenses(string numberOfLicenses)
        {
            GetMoreLicensesButton.Click();

            LicenseNumberSelect.Clear();
            LicenseNumberSelect.SendKeys(numberOfLicenses);

            LicensePackSaveButton.Click();
        }

        internal void SetLicenseLimit(string licenceLimit)
        {
            LicenseLimitEditButton.Click();

            LicenseLimitTextBox.Clear();
            LicenseLimitTextBox.SendKeys(licenceLimit);

            LicenseLimitSaveButton.Click();
        }

        internal void TurnOnAutoRenew()
        {
            AutoRenewSwitch.Click();
        }

        internal void TurnOnAutoRenewForContractors()
        {
            ApplyToContractorsSwitch.Click();
            SaveButton.Click();
        }
    }
}