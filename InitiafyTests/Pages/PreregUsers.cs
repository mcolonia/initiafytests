﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Windows.Forms;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class PreregUsers : BaseTests
    {
        public PreregUsers(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement UserType => Driver.FindElement(By.Name("roleSelect"));
        private IWebElement Email => Driver.FindElement(By.Id("userEmail"));
        private IWebElement FirstName => Driver.FindElement(By.Id("firstName"));
        private IWebElement LastName => Driver.FindElement(By.Id("lastName"));
        private IWebElement addButton => Driver.FindElement(By.XPath("//*[@class='btn btn-add ng-scope']"));
        private IWebElement Location => Driver.FindElement(By.Name("locationSelect"));
        private IWebElement Role => Driver.FindElement(By.Name("departmentSelect"));
        private IWebElement StatusFilter => Driver.FindElement(By.XPath("(//button[@type='button'])[5]"));

        private IWebElement UserIcon => Driver.FindElement(By.XPath("//td[12]/img"));
        private IWebElement StatusAndComments => Driver.FindElement(By.CssSelector("a.undecorated-link.collapsed > strong"));
        private IWebElement CommentField => Driver.FindElement(By.XPath("//*[@id='txtPersonalNoteComment']"));
        private IWebElement RagStatusRed => Driver.FindElement(By.CssSelector("a > i.rag-status.red"));
        private IWebElement AddComment => Driver.FindElement(By.XPath("//div[@id='collapse-notes']/div/div/div[2]/div[2]/div[3]/button"));
        private IWebElement SaveButton => Driver.FindElement(By.XPath("//div[@id='userCustomControlsDialog']/div/div/div[3]/div[2]/div/button"));

        private IWebElement RAGFilterOptionRed => Driver.FindElement(By.CssSelector("i.rag-status.red"));
        private IWebElement RAGFilterOptionGreen => Driver.FindElement(By.CssSelector("i.rag-status.green"));

        private IWebElement StatusAndCommentsClose => Driver.FindElement(By.XPath("//*[@id='heading-notes']/h4/a"));

        private IWebElement PhoneField => Driver.FindElement(By.CssSelector("div.col-sm-12 > #phone"));
        private IWebElement DropList => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[3]/div/div/div/div/select"));
        private IWebElement PickListItem1 => Driver.FindElement(By.XPath("//div[@id='collapse-addContractor-0']/div/div[4]/div/div/div/div/div/label"));
        private IWebElement NumberField => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[2]/div/div/div/div/input"));
        private IWebElement DayField => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[5]/div/div/div/div/div/select/option[2]"));
        private IWebElement Month => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[5]/div/div/div/div/div/select[2]"));
        private IWebElement Year => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[5]/div/div/div/div/div/select[3]"));
        private IWebElement CharBox1 => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[6]/div/div/div/div/input"));
        private IWebElement CharBox2 => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[6]/div/div/div/div/input[2]"));
        private IWebElement CharBox3 => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[6]/div/div/div/div/input[3]"));

        private IWebElement CharBox4 => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[6]/div/div/div/div/input[4]"));
        private IWebElement CharBox5 => Driver.FindElement(By.XPath("//form/div/div[2]/div/div[6]/div/div/div/div/input[5]"));

        private IWebElement DeclarationCheckBox => Driver.FindElement(By.CssSelector("div.col-sm-12.im-checkbox > div > label"));
        private IWebElement ButtonSave => Driver.FindElement(By.Id("btnSave"));
        private IWebElement TextField => Driver.FindElement(By.XPath("//form/div/div[2]/div/div/div/div/div/div/input"));

        private IWebElement ChooseAFileButton1 => Driver.FindElement(By.XPath("//*[@type='file'][1]"));
        private IWebElement ChooseAFileButton2 => Driver.FindElement(By.XPath("//*[@data-field-name='Doc 1']"));
        private IWebElement ChooseAFileButton3 => Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button]"));
        private IWebElement ChooseAFileButton4 => Driver.FindElement(By.CssSelector("[id$=default-create-firstname]"));

        private IWebElement ChooseAFileButton5 => Driver.FindElement(By.XPath("//*[contains(@id,'file-upload')]"));

        private IWebElement LimitMessage => Driver.FindElement(By.XPath("//*[@ng-bind-html='notificationMessage']"));

        private IWebElement LicenseDateLabel => Driver.FindElement(By.XPath("//*[@class='ml-5 badge success']"));
        private IWebElement UnlicensedBadge => Driver.FindElement(By.CssSelector(".badge.danger"));

        private IWebElement UserPopupCancelButton => Driver.FindElement(By.XPath("//*[@class='btn btn-cancel btn-block ng-scope']"));

        private IWebElement SearchBox => Driver.FindElement(By.XPath("//*[@ng-model='userSearch']"));

        private IWebElement ArchiveButton => Driver.FindElement(By.XPath("//button[contains(.,'Archive')]"));

        private IWebElement DeleteUserButton => Driver.FindElement(By.XPath("//button[contains(.,'Delete')]"));

        private IWebElement ErrorMessage => Driver.FindElement(By.XPath("//*[@ng-bind-html='notificationMessage']"));

        public IWebElement CloseNotificationButton => Driver.FindElement(By.XPath("//*[@ng-click='closeNotification()']"));

        public void AddUser(string firstName, string lastName, string email, string userType)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Users (")));

            FirstName.Clear();
            FirstName.SendKeys(firstName);

            LastName.Clear();
            LastName.SendKeys(lastName);

            Email.Clear();
            Email.SendKeys(email);

            if (userType == "admin")
            {
                userType = "Company Admin";
            }

            new SelectElement(UserType).SelectByText(userType);

            addButton.Click();

            Thread.Sleep(1500);
        }

        internal void FillContractorCompetenceDeclaration()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            try
            {
                Thread.Sleep(2000);

                TextField.Clear();
                TextField.SendKeys("text");

                NumberField.Clear();
                NumberField.SendKeys("12341234");

                new SelectElement(DropList).SelectByText("Drop item 1");

                PickListItem1.Click();

                DayField.Click();
                new SelectElement(Month).SelectByText("September");
                new SelectElement(Year).SelectByText("2015");

                CharBox1.Clear();
                CharBox1.SendKeys("1");

                CharBox2.Clear();
                CharBox2.SendKeys("2");

                CharBox3.Clear();
                CharBox3.SendKeys("3");

                CharBox4.Clear();
                CharBox4.SendKeys("4");

                CharBox5.Clear();
                CharBox5.SendKeys("5");
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Problem filling out the custom fields " + e.Message + "\n Click enter to continue");
                Console.ReadLine();
            }

            //Uploads
            try
            {
                Thread.Sleep(1000);
                //ChooseAFileButton5.Click();
                //ChooseAFileButton4.Click();
                ChooseAFileButton3.Click();
                //ChooseAFileButton2.Click();
                //ChooseAFileButton1.Click();
                Driver.FindElement(By.XPath("//*[@data-field-name='Doc 1']"));

                Thread.Sleep(2000);
                SendKeys.SendWait(this.GetExternalFilePath("Builder on Phone.jpg"));
                SendKeys.SendWait(@"{ Enter}");
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Couldn't upload Document " + e.Message);
                _testTextReport.LogError("Upload a document and then click enter.");
                Console.ReadLine();
            }

            DeclarationCheckBox.Click();

            ButtonSave.Click();

            Thread.Sleep(2000);
        }

        internal void AddUserOverLimit(string firstName, string lastName, string email, string userType)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Users (")));

            FirstName.Clear();
            FirstName.SendKeys(firstName);

            LastName.Clear();
            LastName.SendKeys(lastName);

            Email.Clear();
            Email.SendKeys(email);

            new SelectElement(UserType).SelectByText(userType);

            addButton.Click();

            Thread.Sleep(1000);

            try
            {
                Assert.AreEqual("\"Company has no remaining licenses.\"", LimitMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void AddUserOverLimit2(string firstName, string lastName, string email)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Users (")));

            FirstName.Clear();
            FirstName.SendKeys(firstName);

            LastName.Clear();
            LastName.SendKeys(lastName);

            Email.Clear();
            Email.SendKeys(email);

            addButton.Click();

            Thread.Sleep(1000);

            try
            {
                Assert.AreEqual("\"Company has no remaining licenses.\"", LimitMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void AddUser(string firstName, string lastName, string email, string location, string role, string userType)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Users (")));

            FirstName.Clear();
            FirstName.SendKeys(firstName);

            LastName.Clear();
            LastName.SendKeys(lastName);

            Email.Clear();
            Email.SendKeys(email);

            new SelectElement(Location).SelectByText(location);

            new SelectElement(Role).SelectByText(role);

            new SelectElement(UserType).SelectByText(userType);

            addButton.Click();
        }

        internal void AddUser(string firstName, string lastName, string email)
        {
            FirstName.Clear();
            FirstName.SendKeys(firstName);

            LastName.Clear();
            LastName.SendKeys(lastName);

            Email.Clear();
            Email.SendKeys(email);

            addButton.Click();

            Thread.Sleep(1500);
        }

        internal void AddUserFail(string firstName, string lastName, string email)
        {
            FirstName.Clear();
            FirstName.SendKeys(firstName);

            LastName.Clear();
            LastName.SendKeys(lastName);

            Email.Clear();
            Email.SendKeys(email);

            addButton.Click();

            Thread.Sleep(1500);

            try
            {
                Assert.AreEqual("\"Company has no remaining licenses.\"", LimitMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
                Console.ReadLine();
            }
        }

        internal void ChangeUserStatusToRed(string comment)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            SearchBox.SendKeys("initiafy.test+dsf231@gmail.com");

            Thread.Sleep(1500);

            UserIcon.Click();

            Thread.Sleep(1500);

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("a.undecorated-link.collapsed > strong")));
            StatusAndComments.Click();

            Thread.Sleep(1000);

            CommentField.Clear();
            CommentField.SendKeys(comment);

            RagStatusRed.Click();
            AddComment.Click();

            StatusAndCommentsClose.Click();
            Thread.Sleep(500);

            SaveButton.Click();

            Thread.Sleep(1500);

            //Click on the RAG Drop List - choose red
            StatusFilter.Click();
            RAGFilterOptionRed.Click();

            Thread.Sleep(1500);

            StatusFilter.Click();
            RAGFilterOptionGreen.Click();
        }

        internal void CheckUserLicenceDate()
        {
            UserIcon.Click();

            Thread.Sleep(1500);

            Assert.AreEqual("18 Apr 2019", LicenseDateLabel.Text);
            //LicenseDateLabel.Click();
        }

        internal void CheckUnlicensedLabel()
        {
            UserIcon.Click();

            Thread.Sleep(1500);

            UnlicensedBadge.Click();

            UserPopupCancelButton.Click();

            Thread.Sleep(2000);
        }

        internal UserProfilePage SearchForUserAndGoToProfile(string userEmail)
        {
            SearchBox.SendKeys(userEmail);
            Driver.FindElement(By.XPath("//td[contains(.,'" + userEmail + "')]")).Click();

            return TestFactory.New<UserProfilePage>();
        }

        internal void SearchForUserAndArchive(string userEmail)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            SearchBox.Clear();
            SearchBox.SendKeys(userEmail);
            Driver.FindElement(By.XPath("//*[@title='Archive user: Tom Dillon']")).Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(.,'Archive')]")));
            ArchiveButton.Click();

            Thread.Sleep(1000);
        }

        internal void DeleteUser(string email, string firstName, string lastName)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            SearchBox.Clear();
            SearchBox.SendKeys(email);
            Driver.FindElement(By.XPath("//*[@title='Delete user: " + firstName + " " + lastName + "']")).Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(.,'Delete')]")));
            DeleteUserButton.Click();

            Thread.Sleep(1000);
        }

        internal void CheckErrorMessageCheckYourData()
        {
            try
            {
                WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-bind-html='notificationMessage']")));
                Assert.AreEqual("Error during operation, please check your data and try again.", ErrorMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void CheckErrorMessageEmailAlreadyUsed()
        {
            try
            {
                WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-bind-html='notificationMessage']")));
                Assert.AreEqual("\"Sorry, this email is already used.\"", ErrorMessage.Text);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message);
            }
        }

        internal void CloseNotification()
        {
            try
            {
                CloseNotificationButton.Click();
            }
            catch (Exception)
            {
            }
        }
    }
}