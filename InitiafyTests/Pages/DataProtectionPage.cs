﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using System.Threading;

namespace InitiafyTests
{
    public class DataProtectionPage : BaseTests
    {
        public DataProtectionPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement CompanyContactDetailsField => Driver.FindElement(By.Name("companyContactDetails"));

        private IWebElement CompanyLegalNameField => Driver.FindElement(By.Name("companyLegalName"));

        private IWebElement DataLegalReasonsCheckBoxYes => Driver.FindElement(By.XPath("//label[@for='legalObligationsYes']"));

        private IWebElement DPOContactDetailsField => Driver.FindElement(By.Name("dpoContactDetails"));

        private IWebElement EditButton => Driver.FindElement(By.XPath("//button[contains(.,'Edit')]"));

        private IWebElement JurisdictionField => Driver.FindElement(By.Name("jurisdictions"));

        private IWebElement LearnersCompleteRegistrationYesCheckBox => Driver.FindElement(By.XPath("//label[@for='requiredForContractYes']"));

        private IWebElement RetentionPeriodField => Driver.FindElement(By.Name("retentionPeriodMonths"));

        private IWebElement SaveButton => Driver.FindElement(By.XPath("//button[contains(.,'Save')]"));

        private IWebElement ShareUsersDataWithField => Driver.FindElement(By.Name("subProcessors"));

        private IWebElement PreviewButton => Driver.FindElement(By.XPath("//button[contains(.,'Preview')]"));

        private IWebElement PreviewCloseButton => Driver.FindElement(By.XPath("//button[contains(.,'Close')]"));

        internal void UpdateDetails(string CompanyLegalName, string CompanyContactDetails, string DPOContactDetails, string CheckBox1, string CheckBox2, string CompaniesShareDataWith, string Jurisdiction, string RententionPeriod)
        {
            EditButton.Click();

            CompanyLegalNameField.Clear();
            CompanyLegalNameField.SendKeys(CompanyLegalName);

            CompanyContactDetailsField.Clear();
            CompanyContactDetailsField.SendKeys(CompanyContactDetails);

            DPOContactDetailsField.SendKeys(DPOContactDetails);

            LearnersCompleteRegistrationYesCheckBox.Click();

            DataLegalReasonsCheckBoxYes.Click();

            ShareUsersDataWithField.SendKeys(CompaniesShareDataWith);

            JurisdictionField.SendKeys(Jurisdiction);

            RetentionPeriodField.SendKeys(RententionPeriod);

            SaveButton.Click();
        }

        internal void Preview()
        {
            Thread.Sleep(1000);
            PreviewButton.Click();
            Thread.Sleep(1000);
            PreviewCloseButton.Click();
            Thread.Sleep(1000);
        }
    }
}