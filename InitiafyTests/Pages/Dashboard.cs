﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class Dashboard : BaseTests
    {
        public Dashboard(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement Users => Driver.FindElement(By.PartialLinkText("Users ("));

        private IWebElement Companies => Driver.FindElement(By.PartialLinkText("Companies ("));

        private IWebElement preregistration => Driver.FindElement(By.PartialLinkText("Pre-registration ("));

        private IWebElement EditButton => Driver.FindElement(By.XPath("//button[contains(.,'Edit')]"));

        private IWebElement Courses => Driver.FindElement(By.PartialLinkText("Courses ("));

        private IWebElement TopMenu => Driver.FindElement(By.Id("dLabel"));

        private IWebElement CompanySettingsOption => Driver.FindElement(By.LinkText("Company Settings"));

        private IWebElement LearnerView => Driver.FindElement(By.LinkText("Learner View"));

        private IWebElement ReportsHeading => Driver.FindElement(By.PartialLinkText("Reports ("));

        private IWebElement SelfRegCoursesTab => Driver.FindElement(By.PartialLinkText("Self-registered courses"));

        private IWebElement UserProfileOption => Driver.FindElement(By.LinkText("User Profile"));

        private IWebElement SelfRegUsersTab => Driver.FindElement(By.PartialLinkText("Self-registration ("));

        private IWebElement UserApprovalHeading => Driver.FindElement(By.PartialLinkText("User Approval ("));

        private IWebElement NoLicensesMessage => Driver.FindElement(By.XPath("//*[@class='tooltip-inner']"));

        private IWebElement OverviewTab => Driver.FindElement(By.PartialLinkText("Overview"));

        private IWebElement BuyLicensesButton => Driver.FindElement(By.XPath("//*[@ng-model='totalAmount']"));

        private IWebElement LicensesNumberField => Driver.FindElement(By.XPath("//*[@ng-model='creditsToBuy']"));

        private IWebElement CompanyProfileHeading => Driver.FindElement(By.LinkText("Company Profile"));

        private IWebElement CompanyDocumentsTab => Driver.FindElement(By.PartialLinkText("Company Documents"));

        private IWebElement ContractorManagementHeading => Driver.FindElement(By.PartialLinkText("Contractor Management ("));

        private IWebElement UpdateDetailsButton => Driver.FindElement(By.LinkText("Update Details"));

        private IWebElement IFixedItButton => Driver.FindElement(By.XPath("//*[@ng-click='resolveContractorRejection()']"));

        public override string TestName => throw new NotImplementedException();

        public void GoToUsers()
        {
            try
            {
                Thread.Sleep(1000);
                Users.Click();
            }
            catch (Exception e)
            {
                _testTextReport.LogError($"Error {e.Message}");
            }
        }

        internal void VerifyNoLicensesMessage()
        {
            string value = NoLicensesMessage.Text;
            Assert.IsTrue(value.Contains("Hello Test 123, Welcome to Initiafy! You currently have no licenses. To add users, create courses and run reports you will need to buy licenses, please contact us by phone and email to buy licenses."), value + " doesn't contain the string");
        }

        public PreregUsers GoToPreRegTab()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));

            try
            {
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Users (")));
                preregistration.Click();
            }
            catch (Exception e)
            {
                _testTextReport.LogError($"Error {e.Message}");
            }

            return TestFactory.New<PreregUsers>();
        }

        public ContractorsPage GoToCompanies()
        {
            Companies.Click();
            return TestFactory.New<ContractorsPage>();
        }

        public CompanySettingsPage GoToCompanySettings()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            EditButton.Click();
            return TestFactory.New<CompanySettingsPage>();
        }

        internal void Logout()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("dLabel")));
            Driver.FindElement(By.Id("dLabel")).Click();
            Thread.Sleep(1000);
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Logout")));
            Driver.FindElement(By.LinkText("Logout")).Click();
            Thread.Sleep(1000);
        }

        internal ReportsOverviewPage GoToReportsOverview()
        {
            OverviewTab.Click();
            return TestFactory.New<ReportsOverviewPage>();
        }

        internal UserApprovalPage GoToUserApproval()
        {
            UserApprovalHeading.Click();
            return TestFactory.New<UserApprovalPage>();
        }

        public CoursesPage GoToCourses()
        {
            Courses.Click();
            return TestFactory.New<CoursesPage>();
        }

        internal CompanySettingsPage GoToCompanySettingsAsAdmin()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("dLabel")));
            TopMenu.Click();
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Company Settings")));
            CompanySettingsOption.Click();

            return TestFactory.New<CompanySettingsPage>();
        }

        internal CompanyDocumentsPage GoToCompanyDocuments()
        {
            CompanyDocumentsTab.Click();

            return TestFactory.New<CompanyDocumentsPage>();
        }

        internal ContractorManagementPage GoToContractorManagement()
        {
            ContractorManagementHeading.Click();
            return TestFactory.New<ContractorManagementPage>();
        }

        internal void GoToCompanyProfile()
        {
            CompanyProfileHeading.Click();
        }

        internal RegistrationPage GoToLearnerView()
        {
            TopMenu.Click();
            Thread.Sleep(1500);

            LearnerView.Click();

            return TestFactory.New<RegistrationPage>();
        }

        internal void GoToReports()
        {
            ReportsHeading.Click();
        }

        internal SelfRegisteredCoursesPage GoToSelfRegCourses()
        {
            SelfRegCoursesTab.Click();

            return TestFactory.New<SelfRegisteredCoursesPage>();
        }

        internal UserProfilePage GoToUserProfile()
        {
            Thread.Sleep(500);
            TopMenu.Click();
            Thread.Sleep(1500);
            UserProfileOption.Click();

            return TestFactory.New<UserProfilePage>();
        }

        internal SelfRegUsers GoToSelfRegTab()
        {
            SelfRegUsersTab.Click();
            Thread.Sleep(1000);
            return TestFactory.New<SelfRegUsers>();
        }

        internal void FixIssues()
        {
            IFixedItButton.Click();
        }

        internal void UpdateDetails()
        {
            UpdateDetailsButton.Click();
        }

        internal void VerifyPurchaseLicensesMessageForCM()
        {
            string value = NoLicensesMessage.Text;
            Assert.IsTrue(value.Contains("Hello They Pay 1, Welcome to Initiafy! You currently have no licenses. To start using initiafy, you will need to buy licenses, please select the number of licenses you want to buy and hit Buy Licenses. Remember, one license is used for each worker."), value + " doesn't contain the string");
        }

        internal void BuyLicenses(string licenses)
        {
            LicensesNumberField.Clear();
            LicensesNumberField.SendKeys("3");

            BuyLicensesButton.Click();
        }
    }
}