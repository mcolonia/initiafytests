﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    internal class CoursePage : BaseTests
    {
        public CoursePage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement Answer1 => Driver.FindElement(By.CssSelector("label"));

        private IWebElement NextQuestionButton => Driver.FindElement(By.XPath("(//button[@type='button'])[2]"));

        private IWebElement ForwardChevron => Driver.FindElement(By.XPath("//*[@class='glyphicon glyphicon-chevron-right light-up']"));

        private IWebElement Answer2 => Driver.FindElement(By.XPath("//div[@id='slideCarouselTest']/div/div[2]/div[3]/div/div/label"));

        private IWebElement FinishTestButton => Driver.FindElement(By.XPath("(//button[@type='button'])[3]"));

        private IWebElement ShareMySuccessCancelButton => Driver.FindElement(By.XPath("(//button[@type='button'])[11]"));

        private IWebElement ViewCertificateButton => Driver.FindElement(By.LinkText("View certificate"));

        private IWebElement BackToProfileButton => Driver.FindElement(By.LinkText("Back to Profile"));

        internal void CompleteCourse()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));

            ForwardChevron.Click();
            Thread.Sleep(1000); //does not slow the test as default wait is 5 seconds. Stops all the clicks hitting at once

            ForwardChevron.Click();
            Thread.Sleep(1000);

            ForwardChevron.Click();
            Thread.Sleep(1000);

            ForwardChevron.Click();
            Thread.Sleep(2000);

            Answer1.Click();

            NextQuestionButton.Click();

            Thread.Sleep(2000);

            Answer2.Click();

            FinishTestButton.Click();

            try
            {
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("(//button[@type='button'])[11]")));
                //Assert.AreEqual("Share My Success", Driver.FindElement(By.CssSelector("#courseRatingDialog > div.modal-dialog > div.modal-content > div.row.row-header > div.col-md-12.text-center > p.text-center")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
                Console.ReadLine();
            }

            ShareMySuccessCancelButton.Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.LinkText("View certificate")));

            Thread.Sleep(1500);
            ViewCertificateButton.Click();

            Logout();
        }

        internal void CompleteCourseUA()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));

            ForwardChevron.Click();
            Thread.Sleep(1000); //does not slow the test as default wait is 5 seconds. Stops all the clicks hitting at once

            ForwardChevron.Click();
            Thread.Sleep(1000);

            ForwardChevron.Click();
            Thread.Sleep(1000);

            ForwardChevron.Click();
            Thread.Sleep(2000);

            Answer1.Click();

            NextQuestionButton.Click();

            Thread.Sleep(2000);

            Answer2.Click();

            FinishTestButton.Click();

            try
            {
                waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#courseRatingDialog > div.modal-dialog > div.modal-content > div.row.row-header > div.col-md-12.text-center > p.text-center")));
                Assert.AreEqual("Share My Success", Driver.FindElement(By.CssSelector("#courseRatingDialog > div.modal-dialog > div.modal-content > div.row.row-header > div.col-md-12.text-center > p.text-center")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError(e.Message);
                Console.ReadLine();
            }

            ShareMySuccessCancelButton.Click();

            Thread.Sleep(1000);

            try
            {
                Assert.AreEqual("You have completed the course. Your supervisor has been informed and should be in touch shortly.", Driver.FindElement(By.CssSelector("div.panel-body")).Text);
            }
            catch (AssertionException e)
            {
                _testTextReport.LogError("Error not seeing message " + e.Message);
            }

            BackToProfileButton.Click();

            Logout();
        }

        private void Logout()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("dLabel")));
            Driver.FindElement(By.Id("dLabel")).Click();
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Logout")));
            Driver.FindElement(By.LinkText("Logout")).Click();
        }
    }
}