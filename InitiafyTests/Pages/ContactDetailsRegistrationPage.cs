﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class ContactDetailsRegistrationPage : BaseTests
    {
        public ContactDetailsRegistrationPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        public IWebElement ClickPage => Driver.FindElement(By.XPath("html/body//a"));

        public IWebElement Label1 => Driver.FindElement(By.XPath("html/body/div[3]/div/div[2]/div[1]/div[1]/div[1]/label"));

        public IWebElement SMSCodeNextButton => Driver.FindElement(By.XPath("//*[@ng-click='validateConfirmationCode()']"));

        private IWebElement CodeInputBox1 => Driver.FindElement(By.XPath("//*[@id='token-box-1']"));

        private IWebElement CodeInputBox2 => Driver.FindElement(By.XPath("//*[@id='token-box-2']"));

        private IWebElement CodeInputBox3 => Driver.FindElement(By.XPath("//*[@id='token-box-3']"));

        private IWebElement CodeInputBox4 => Driver.FindElement(By.XPath("//*[@id='token-box-4']"));

        private IWebElement CodeInputBox5 => Driver.FindElement(By.XPath("//*[@id='token-box-5']"));

        private IWebElement CodeInputBox6 => Driver.FindElement(By.XPath("//*[@id='token-box-6']"));

        private IWebElement MobileOrEmailTextBox => Driver.FindElement(By.XPath("//*[@ng-model='emailOrPhone']"));

        private IWebElement NextButton => Driver.FindElement(By.XPath("//*[@ng-click='checkIfContactExists()']"));

        internal RegistrationPage RegisterEmail(string EmailOrPhone)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-model='emailOrPhone']")));

            Thread.Sleep(1000);

            MobileOrEmailTextBox.SendKeys(EmailOrPhone);

            NextButton.Click();

            return TestFactory.New<RegistrationPage>();
        }

        internal RegistrationPage RegisterPhoneNumber(string EmailOrPhone)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@ng-model='emailOrPhone']")));

            Thread.Sleep(1000);

            MobileOrEmailTextBox.SendKeys(EmailOrPhone + "\t");

            MobileOrEmailTextBox.Click();

            MobileOrEmailTextBox.SendKeys(OpenQA.Selenium.Keys.Tab);

            MobileOrEmailTextBox.SendKeys(OpenQA.Selenium.Keys.Enter);

            waitForElement.Until(ExpectedConditions.ElementToBeClickable(NextButton));

            NextButton.Click();

            ConfirmToken(waitForElement);

            return TestFactory.New<RegistrationPage>();
        }

        private void ConfirmToken(WebDriverWait waitForElement)
        {
            Thread.Sleep(1500);

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='token-box-1']")));

            Thread.Sleep(1500);

            CodeInputBox1.SendKeys("1");

            Thread.Sleep(500);

            CodeInputBox2.SendKeys("2");

            Thread.Sleep(500);

            CodeInputBox3.SendKeys("3");

            Thread.Sleep(500);

            CodeInputBox4.SendKeys("4");

            Thread.Sleep(500);

            CodeInputBox5.SendKeys("5");

            Thread.Sleep(500);

            CodeInputBox6.SendKeys("6");

            Thread.Sleep(500);

            SMSCodeNextButton.Click();
        }
    }
}