﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    public class SelfRegUsers : BaseTests
    {
        public SelfRegUsers(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement StatusFilter => Driver.FindElement(By.XPath("(//button[@type='button'])[5]"));

        private IWebElement UserIcon => Driver.FindElement(By.XPath("//td[12]/img"));
        private IWebElement StatusAndComments => Driver.FindElement(By.CssSelector("a.undecorated-link.collapsed > strong"));
        private IWebElement CommentField => Driver.FindElement(By.Id("txtPersonalNoteComment"));
        private IWebElement RagStatusRed => Driver.FindElement(By.CssSelector("a > i.rag-status.red"));
        private IWebElement RagStatusAmber => Driver.FindElement(By.CssSelector("a > i.rag-status.amber"));
        private IWebElement AddComment => Driver.FindElement(By.XPath("//div[@id='collapse-notes']/div/div/div[2]/div[2]/div[3]/button"));
        private IWebElement SaveButton => Driver.FindElement(By.XPath("//div[@id='userCustomControlsDialog']/div/div/div[3]/div[2]/div/button"));

        private IWebElement RAGFilterOptionAmber => Driver.FindElement(By.CssSelector("i.rag-status.amber"));

        private IWebElement UnlicensedBadge => Driver.FindElement(By.CssSelector(".badge.danger"));

        private IWebElement UserPopupCancelButton => Driver.FindElement(By.XPath("//*[@class='btn btn-cancel btn-block ng-scope']"));

        private IWebElement SearchBox => Driver.FindElement(By.XPath("//*[@ng-model='userSearch']"));

        private IWebElement ArchiveButton => Driver.FindElement(By.XPath("//button[contains(.,'Archive')]"));

        private IWebElement NoDataMessage => Driver.FindElement(By.XPath("//*[@id='users']//table/tbody/tr[2]/td"));

        private IWebElement IncludeArchivedCheckbox => Driver.FindElement(By.XPath("//*[@ng-model='includeArchivedUsers']"));

        private IWebElement UnarchiveButton => Driver.FindElement(By.XPath("//button[contains(.,'Unarchive')]"));

        private IWebElement SearchBoxButton => Driver.FindElement(By.XPath("//button[@class='btn-filter-search glyphicon glyphicon-search']"));

        private IWebElement AssignButton => Driver.FindElement(By.XPath("//*[@data-toggle='popover']"));

        private IWebElement row => Driver.FindElement(By.XPath("//*[@id='users']//table/tbody/tr[3]/td[4]"));

        private IWebElement CourseSelect => Driver.FindElement(By.XPath("//*[@ng-model='selectedCourse']"));

        //public IWebElement AssignDay => Driver.FindElement(By.XPath("//select[@name='date_[day]'])[13]"));
        //public IWebElement AssignMonth => Driver.FindElement(By.XPath("//select[@name='date_[month]'])[13]"));
        //public IWebElement AssignYear => Driver.FindElement(By.XPath("//select[@name='date_[year]'])[13]"));

        private IWebElement AssignDay => Driver.FindElement(By.CssSelector(".day"));
        private IWebElement AssignMonth => Driver.FindElement(By.CssSelector(".month"));
        private IWebElement AssignYear => Driver.FindElement(By.CssSelector(".year"));

        private IWebElement AssignButtonPopUp => Driver.FindElement(By.XPath("//*[@class='btn btn-assign btn-block']"));

        internal void ChangeUserStatusToAmber(string comment)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            UserIcon.Click();

            Thread.Sleep(1000);

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("a.undecorated-link.collapsed > strong")));
            StatusAndComments.Click();
            Thread.Sleep(1000);

            CommentField.Clear();
            CommentField.SendKeys(comment);

            RagStatusAmber.Click();
            AddComment.Click();

            SaveButton.Click();

            Thread.Sleep(1500);

            //Click on the RAG Drop List - choose red
            StatusFilter.Click();
            Thread.Sleep(500);
            RAGFilterOptionAmber.Click();

            Thread.Sleep(1500);
        }

        internal void ManuallyAssignCourse(string userEmail)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            SearchBox.Clear();
            SearchBox.SendKeys(userEmail);

            Thread.Sleep(1500);

            AssignButton.Click();

            Thread.Sleep(1500);

            try
            {
                new SelectElement(CourseSelect).SelectByText("An External Course");
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Something went wrong " + e.Message);
            }

            Thread.Sleep(1000);

            try
            {
                Driver.FindElement(By.XPath("(//option[@value='02'])[9]")).Click();
            }
            catch (Exception)
            {
            }

            try
            {
                new SelectElement(Driver.FindElement(By.XPath("//*[@class='day']"))).SelectByText("1st");
                new SelectElement(Driver.FindElement(By.XPath("(//select[@name='date_[month]'])[5]"))).SelectByText("June");
                new SelectElement(Driver.FindElement(By.XPath("(//select[@name='date_[year]'])[5]"))).SelectByText("2017");
            }
            catch (Exception)
            {
            }

            AssignButtonPopUp.Click();
        }

        internal ModifiedUserDialog SearchForUserAndShowModifiedUserDialog(string firstName, string lastName)
        {
            SearchBox.Clear();
            SearchBox.SendKeys(lastName);

            Driver.FindElement(By.XPath("//*[@title='See registration info: " + firstName + " " + lastName + "']")).Click();

            return TestFactory.New<ModifiedUserDialog>();
        }

        internal void UnarchiveUser(string userEmail)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            SearchBox.Clear();
            SearchBox.SendKeys(userEmail);

            Driver.FindElement(By.XPath("//*[@title='Unarchive user: George Hines']")).Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(.,'Unarchive')]")));
            UnarchiveButton.Click();

            Thread.Sleep(1000);
        }

        internal void IncludeArchivedSearch(string userEmail)
        {
            SearchBox.Clear();

            IncludeArchivedCheckbox.Click();

            SearchBox.SendKeys(userEmail);
        }

        internal void VerifyUserDoesNotAppearInSearch(string userEmail)
        {
            SearchBox.Clear();
            SearchBox.SendKeys(userEmail);

            Thread.Sleep(1500);
            string value = NoDataMessage.Text;
            Assert.IsTrue(value.Contains("Sorry, there is no data matching your search"), value + " doesn't contain the string");
        }

        internal void SearchForUserAndArchive(string userEmail)
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            SearchBox.SendKeys(userEmail);
            SearchBoxButton.Click();

            Thread.Sleep(1500);

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@title='Archive user: George Hines']")));
            Driver.FindElement(By.XPath("//*[@title='Archive user: George Hines']")).Click();

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//button[contains(.,'Archive')]")));
            ArchiveButton.Click();
        }

        internal void CheckUnlicensedLabel()
        {
            UserIcon.Click();

            Thread.Sleep(1500);

            UnlicensedBadge.Click();

            UserPopupCancelButton.Click();

            Thread.Sleep(1000);
        }

        internal UserProfilePage SearchForUserAndGoToProfile(string userEmail)
        {
            SearchBox.SendKeys(userEmail);
            Driver.FindElement(By.XPath("//td[contains(.,'" + userEmail + "')]")).Click();

            return TestFactory.New<UserProfilePage>();
        }
    }
}