﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;

namespace InitiafyTests
{
    internal class ContractorManagementPage : BaseTests
    {
        public ContractorManagementPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement CompanyEmailSearchField => Driver.FindElement(By.XPath("//*[@ng-search='search']"));

        private IWebElement ReviewButton => Driver.FindElement(By.XPath("//*[@class='btn btn-review btn-block']"));

        internal ReviewDialog SearchForCompanyAndReview(string companyName)
        {
            CompanyEmailSearchField.Clear();
            CompanyEmailSearchField.SendKeys(companyName);

            ReviewButton.Click();
            return TestFactory.New<ReviewDialog>();
        }
    }
}