﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace InitiafyTests
{
    public class CoursesPage : BaseTests
    {
        public CoursesPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement CourseTitle => Driver.FindElement(By.Id("courseTitle"));

        private IWebElement PassRate => Driver.FindElement(By.Name("passRateSelect"));

        private IWebElement QuestionsPerQuiz => Driver.FindElement(By.Id("questionsPerQuiz"));

        private IWebElement SaveButton => Driver.FindElement(By.XPath("//div[8]/button"));

        private IWebElement SearchBox => Driver.FindElement(By.XPath("//*[@ng-model='titleSearch']"));

        private IWebElement SearchResult => Driver.FindElement(By.XPath("//td"));

        private IWebElement ValidityPeriod => Driver.FindElement(By.Name("validPeriodSelect"));

        private IWebElement CourseType => Driver.FindElement(By.XPath("//*[@ng-model='isOfflineCourse']"));

        private IWebElement CourseCost => Driver.FindElement(By.Id("coursePrice"));

        public override string TestName => throw new NotImplementedException();

        internal void CreateCourse(string title, string passRate, string questions, string validPeriod)
        {
            CourseTitle.SendKeys(title);

            new SelectElement(PassRate).SelectByText("80 %");

            QuestionsPerQuiz.SendKeys(questions);

            new SelectElement(ValidityPeriod).SelectByText(validPeriod);

            SaveButton.Click();
        }

        public CourseEditor Search(string courseTitle)
        {
            Thread.Sleep(1000);
            SearchBox.SendKeys(courseTitle);
            Thread.Sleep(1500);
            SearchResult.Click();

            return TestFactory.New<CourseEditor>();
        }

        internal void CreateExternalCourse(string title, string validPeriod)
        {
            CourseTitle.SendKeys(title);

            new SelectElement(CourseType).SelectByText("External Course");

            new SelectElement(ValidityPeriod).SelectByText(validPeriod);

            SaveButton.Click();
        }

        internal void CreateSellerCourse(string title, string passRate, string questions, string validPeriod, string price)
        {
            CourseTitle.SendKeys(title);

            new SelectElement(PassRate).SelectByText("80 %");

            QuestionsPerQuiz.SendKeys(questions);

            new SelectElement(ValidityPeriod).SelectByText(validPeriod);

            CourseCost.Clear();
            CourseCost.SendKeys(price);

            SaveButton.Click();
        }
    }
}