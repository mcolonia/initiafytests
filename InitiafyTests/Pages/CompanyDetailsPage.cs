﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Windows.Forms;

namespace InitiafyTests
{
    public class CompanyDetailsPage : BaseTests
    {
        public CompanyDetailsPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement ChooseAFileButton => Driver.FindElement(By.XPath("//*[@id='fileupload']"));

        private IWebElement ChooseAFileButton2 => Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']"));

        private IWebElement ContactPerson => Driver.FindElement(By.Name("contactPerson"));

        private IWebElement Email => Driver.FindElement(By.Name("email"));

        private IWebElement SaveLogo => Driver.FindElement(By.XPath("//*[@class='btn btn-primary btn-block pull-right']"));

        private IWebElement UploadButton => Driver.FindElement(By.XPath("//*[@ng-click='showUploadDialog()']"));

        private IWebElement Colour1 => Driver.FindElement(By.XPath("//*[@id='color1Input']"));

        private IWebElement Colour2 => Driver.FindElement(By.XPath("//*[@id='color2Input']"));

        private IWebElement SaveButton => Driver.FindElement(By.XPath("//button[contains(.,'Save')]"));

        private IWebElement TrafficLightsCheckbox => Driver.FindElement(By.XPath("//label[@for='trafficLightsSystem']"));

        private IWebElement UserApprovalCheckBox => Driver.FindElement(By.XPath("//label[@for='userApproval']"));

        private IWebElement OptionalContractorRegistrationCheckbox => Driver.FindElement(By.XPath("//label[@for='optionalContractorRegistration']"));

        private IWebElement ContractorPaymentCheckbox => Driver.FindElement(By.XPath("//label[@for='paymentEnabled']"));

        private IWebElement ContractorManagementCheckbox => Driver.FindElement(By.XPath("//label[@for='contractorManagementEnabled']"));

        private IWebElement AutoArchivingCheckBox => Driver.FindElement(By.XPath("//label[@for='autoArchiving']"));

        private IWebElement ArchiveYear => Driver.FindElement(By.XPath("//*[@ng-model='inactiveYearsBeforeArchiving']"));
        private IWebElement ArchiveMonth => Driver.FindElement(By.XPath("//*[@ng-model='inactiveMonthsBeforeArchiving']"));
        private IWebElement KeepArchivedYear => Driver.FindElement(By.XPath("//*[@ng-model='archivalYears']"));

        private IWebElement CreateExternalCoursesCheckbox => Driver.FindElement(By.XPath("//label[@for='createExternalCourses']"));

        private IWebElement CourseResellerCheckBox => Driver.FindElement(By.XPath("//label[@for='courseReseller']"));

        internal void TurnOnExternalCourses()
        {
            CreateExternalCoursesCheckbox.Click();
            SaveButton.Click();
        }

        internal void TurnOnContractorManagement()
        {
            ContractorManagementCheckbox.Click();
            SaveButton.Click();
        }

        internal void AddContactPerson(string contactPerson)
        {
            ContactPerson.Clear();
            ContactPerson.SendKeys(contactPerson);
        }

        internal void TurnOnOptionalContractorRegistration()
        {
            OptionalContractorRegistrationCheckbox.Click();
            SaveButton.Click();
        }

        internal void TurnOnUserApproval()
        {
            UserApprovalCheckBox.Click();
            SaveButton.Click();
        }

        internal void TurnOnCourseReseller()
        {
            CourseResellerCheckBox.Click();
            SaveButton.Click();
        }

        internal void TurnOnUserArchiving()
        {
            var checkbox = Driver.FindElement(By.XPath("//*[@id='autoArchiving']"));

            if (!checkbox.Selected)
            {
                AutoArchivingCheckBox.Click();

                Thread.Sleep(1000);

                new SelectElement(ArchiveYear).SelectByText("1");

                new SelectElement(ArchiveMonth).SelectByText("1");

                new SelectElement(KeepArchivedYear).SelectByText("1");

                SaveButton.Click();
            }           
        }

        internal void AddEmail(string email)
        {
            Email.Clear();
            Email.SendKeys(email);
        }

        internal void UploadLogo(string image)
        {
            UploadButton.Click();

            try
            {
                //Wait for the dialog to appear and then try click on the Choose a File button by Id
                Thread.Sleep(1500);
                ChooseAFileButton.Click();
            }
            catch (Exception)
            {
                //Try clicking on the button again
                //_testTextReport.LogError("Didn't Work");
                Thread.Sleep(1500);
                ChooseAFileButton2.Click();
            }

            try
            {
                Thread.Sleep(2000);
                SendKeys.SendWait(image);
                SendKeys.SendWait(@"{Enter}");
                Thread.Sleep(3000);
            }
            catch (Exception e)
            {
                _testTextReport.LogError("Didn't work :( " + e.Message);
            }

            SaveLogo.Click();
            Thread.Sleep(2000);
        }

        internal void SetColours1(string colour1)
        {
            try
            {
                Colour1.Clear();
                Colour1.SendKeys(colour1);
            }
            catch (Exception e)
            {
                _testTextReport.LogError(e.Message + " , Click enter to continue");
                Console.ReadLine();
            }
        }

        internal void SetColours2(string colour2)
        {
            Colour2.Clear();
            Colour2.SendKeys(colour2);
        }

        internal void SaveChanges()
        {
            SaveButton.Click();
        }

        internal void TurnOnTrafficLights()
        {
            TrafficLightsCheckbox.Click();
            SaveChanges();
        }

        internal void TurnOnContractorPayment()
        {
            Driver.FindElement(By.LinkText("Logging")).SendKeys(OpenQA.Selenium.Keys.PageUp);
            Thread.Sleep(1000);

            ContractorPaymentCheckbox.Click();
            SaveChanges();
        }
    }
}