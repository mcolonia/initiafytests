﻿using InitiafyTests.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Windows.Forms;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace InitiafyTests
{
    internal class RegistrationPage : BaseTests
    {
        public RegistrationPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement EmailField => Driver.FindElement(By.Id("email"));
        private IWebElement FirstNameField => Driver.FindElement(By.Id("firstName"));
        private IWebElement LastNameField => Driver.FindElement(By.Id("lastName"));
        private IWebElement LocationSelect => Driver.FindElement(By.Id("locationSelect"));
        private IWebElement DepartmentSelect => Driver.FindElement(By.Id("departmentSelect"));
        private IWebElement PhoneField => Driver.FindElement(By.Id("phone"));
        private IWebElement Password => Driver.FindElement(By.Id("password"));
        private IWebElement ConfirmPassword => Driver.FindElement(By.Id("confirmedPassword"));
        private IWebElement Agreement => Driver.FindElement(By.XPath("//label[@for='eulaAcceptedCheckbox']"));
        private IWebElement SubmitButton => Driver.FindElement(By.XPath("//button[@type='submit']"));
        private IWebElement TextField => Driver.FindElement(By.XPath(".//*[@placeholder='Some help text']"));
        private IWebElement NumberField => Driver.FindElement(By.XPath(".//*[@ng-number-control='number']"));
        private IWebElement DropList => Driver.FindElement(By.XPath(".//*[@ng-options='i for i in control.parameters.items']"));
        private IWebElement CheckBox1 => Driver.FindElement(By.XPath(".//*[@class='col-xs-12 im-checkbox ng-scope']/div/label[1]"));
        private IWebElement DateDay => Driver.FindElement(By.Name("date_[day]"));
        private IWebElement DateMonth => Driver.FindElement(By.Name("date_[month]"));
        private IWebElement DateYear => Driver.FindElement(By.Name("date_[year]"));
        private IWebElement CharBox => Driver.FindElement(By.XPath(".//*[@class='mr-5 form-control small-box ng-scope ng-pristine ng-invalid ng-invalid-required'][1]"));

        private IWebElement WelcomeBackPassword => Driver.FindElement(By.XPath("//*[@id='newRegistrationDlgPassword']"));

        private IWebElement LoginButton => Driver.FindElement(By.XPath("//*[@id='newRegistrationDialog']//button[2]"));

        private IWebElement GDPRCheckBox => Driver.FindElement(By.XPath("//label[@for='gdprAcceptedCheckbox']"));

        internal UserProfilePage RegisterBasic(string firstName, string lastName, string location, string department, string password, bool rereg = false, bool GDPR = true)
        {
            //WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            //waitForElement.Until(ExpectedConditions.ElementIsVisible(By.Id("firstName"));
            FirstNameField.Clear();
            FirstNameField.SendKeys(firstName);

            LastNameField.Clear();
            LastNameField.SendKeys(lastName);

            //PhoneField.Clear();
            //PhoneField.SendKeys(phone);

            new SelectElement(LocationSelect).SelectByText(location);

            new SelectElement(DepartmentSelect).SelectByText(department);

            if (rereg == false)
            {
                Password.Clear();
                Password.SendKeys(password);

                ConfirmPassword.Clear();
                ConfirmPassword.SendKeys(password);
            }
            else
            {
                _testTextReport.LogError("Re-registration");
            }

            SetAgreement();

            SubmitButton.Click();

            return TestFactory.New<UserProfilePage>();
        }

        internal UserProfilePage RegisterCustomFields1(string firstName, string lastName, string location, string department, string password, string profilePic, string doc)
        {
            FirstNameField.Clear();
            FirstNameField.SendKeys(firstName);

            LastNameField.Clear();
            LastNameField.SendKeys(lastName);

            //EmailField.Clear();
            //EmailField.SendKeys(email);

            //PhoneField.Clear();
            //PhoneField.SendKeys(phone);

            new SelectElement(LocationSelect).SelectByText(location);

            new SelectElement(DepartmentSelect).SelectByText(department);

            //Custom Fields
            try
            {
                TextField.SendKeys("test");

                NumberField.SendKeys("1234");

                new SelectElement(DropList).SelectByText("Drop item 1");

                CheckBox1.Click();

                new SelectElement(DateDay).SelectByText("1st");
                new SelectElement(DateMonth).SelectByText("June");
                new SelectElement(DateYear).SelectByText("2014");

                CharBox.SendKeys("1");
                CharBox.SendKeys("2");
                CharBox.SendKeys("3");
                CharBox.SendKeys("4");
                CharBox.SendKeys("5");
            }
            catch (Exception e)
            {
                _testTextReport.LogError("OOPS! Something went wrong\n" + e.Message);
            }

            //Pic, and doc uploads
            try
            {
                Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']")).Click();
                Thread.Sleep(1000);
                SendKeys.SendWait(profilePic);
                SendKeys.SendWait(@"{Enter}");
            }
            catch (Exception)
            {
                _testTextReport.LogError("Couldn't upload Profile Picture");
            }

            Thread.Sleep(1000);

            try
            {
                Driver.FindElement(By.XPath("(//*[@class='btn btn-selectFile btn-block fileinput-button'])[2]")).Click();

                Thread.Sleep(1000);
                SendKeys.SendWait(doc);
                SendKeys.SendWait(@"{Enter}");
            }
            catch (Exception)
            {
                _testTextReport.LogError("Couldn't upload document");
            }

            Password.Clear();
            Password.SendKeys(password);

            ConfirmPassword.Clear();
            ConfirmPassword.SendKeys(password);

            SetAgreement();

            SubmitButton.Click();

            return TestFactory.New<UserProfilePage>();
        }

        internal void VerifyPage()
        {
            string myTitle = Driver.Title;

            if (myTitle == "Register to Initiafy - Initiafy")
            {
                return;
            }
            else
            {
                _testTextReport.LogError("On profile page");
            }
        }

        internal UserProfilePage RegisterCustomFields2(string firstName, string lastName, string location, string department, string password, string profilePic, string doc)
        {
            FirstNameField.Clear();
            FirstNameField.SendKeys(firstName);

            LastNameField.Clear();
            LastNameField.SendKeys(lastName);

            //PhoneField.Clear();
            //PhoneField.SendKeys(phone);

            new SelectElement(LocationSelect).SelectByText(location);

            new SelectElement(DepartmentSelect).SelectByText(department);

            //Custom Fields
            try
            {
                TextField.SendKeys("test");

                NumberField.SendKeys("1234");

                new SelectElement(DropList).SelectByText("Drop item 1");

                CheckBox1.Click();

                new SelectElement(DateDay).SelectByText("1st");
                new SelectElement(DateMonth).SelectByText("June");
                new SelectElement(DateYear).SelectByText("2014");

                CharBox.SendKeys("1");
                CharBox.SendKeys("2");
                CharBox.SendKeys("3");
                CharBox.SendKeys("4");
                CharBox.SendKeys("5");
            }
            catch (Exception e)
            {
                _testTextReport.LogError("OOPS! Something went wrong\n" + e.Message);
            }

            //Pic, and doc uploads
            try
            {
                Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']")).Click();
                Thread.Sleep(1000);
                SendKeys.SendWait(profilePic);
                SendKeys.SendWait(@"{Enter}");
            }
            catch (Exception)
            {
                _testTextReport.LogError("Couldn't upload Profile Picture");
            }

            Thread.Sleep(1000);

            try
            {
                Driver.FindElement(By.XPath("(//*[@class='btn btn-selectFile btn-block fileinput-button'])[2]")).Click();

                Thread.Sleep(1000);
                SendKeys.SendWait(doc);
                SendKeys.SendWait(@"{Enter}");
            }
            catch (Exception)
            {
                _testTextReport.LogError("Couldn't upload document");
            }

            SetAgreement();

            SubmitButton.Click();

            return TestFactory.New<UserProfilePage>();
        }

        internal UserProfilePage ReRegister()
        {
            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));

            waitForElement.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='newRegistrationDlgPassword']")));

            WelcomeBackPassword.Clear();
            WelcomeBackPassword.SendKeys( _testConfig.UserPassword);

            LoginButton.Click();

            Thread.Sleep(2000);

            new SelectElement(LocationSelect).SelectByText("Liverpool");

            new SelectElement(DepartmentSelect).SelectByText("Accounts");

            SetAgreement();

            SubmitButton.Click();

            return TestFactory.New<UserProfilePage>();
        }

        internal UserProfilePage RegisterCM(string firstName, string lastName, string location, string department, string profilePic, string doc)
        {
            FirstNameField.Clear();
            FirstNameField.SendKeys(firstName);

            LastNameField.Clear();
            LastNameField.SendKeys(lastName);

            //PhoneField.Clear();
            //PhoneField.SendKeys(phone);

            new SelectElement(LocationSelect).SelectByText(location);

            new SelectElement(DepartmentSelect).SelectByText(department);

            //Custom Fields
            try
            {
                TextField.SendKeys("test");

                NumberField.SendKeys("1234");

                new SelectElement(DropList).SelectByText("Drop item 1");

                CheckBox1.Click();

                new SelectElement(DateDay).SelectByText("1st");
                new SelectElement(DateMonth).SelectByText("June");
                new SelectElement(DateYear).SelectByText("2014");

                CharBox.SendKeys("1");
                CharBox.SendKeys("2");
                CharBox.SendKeys("3");
                CharBox.SendKeys("4");
                CharBox.SendKeys("5");
            }
            catch (Exception e)
            {
                _testTextReport.LogError("OOPS! Something went wrong\n" + e.Message);
            }

            //Pic, and doc uploads
            try
            {
                Driver.FindElement(By.XPath("//*[@class='btn btn-selectFile btn-block fileinput-button']")).Click();
                Thread.Sleep(1000);
                SendKeys.SendWait(profilePic);
                SendKeys.SendWait(@"{Enter}");
            }
            catch (Exception)
            {
                _testTextReport.LogError("Couldn't upload Profile Picture");
            }

            Thread.Sleep(1000);

            try
            {
                Driver.FindElement(By.XPath("(//*[@class='btn btn-selectFile btn-block fileinput-button'])[2]")).Click();

                Thread.Sleep(1000);
                SendKeys.SendWait(doc);
                SendKeys.SendWait(@"{Enter}");
            }
            catch (Exception)
            {
                _testTextReport.LogError("Couldn't upload document");
            }

            SetAgreement();

            SubmitButton.Click();

            Thread.Sleep(3500);

            return TestFactory.New<UserProfilePage>();
        }

        private void SetAgreement()
        {
            Agreement.Click();
            GDPRCheckBox.Click();
        }
    }
}