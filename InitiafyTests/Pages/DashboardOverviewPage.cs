﻿using InitiafyTests.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace InitiafyTests
{
    public class DashboardOverviewPage : BaseTests
    {
        public DashboardOverviewPage(IWebDriver driver, TestTextReport testTextReport, TestConfig testConfig) : base(driver, testTextReport, testConfig)
        {
        }

        private IWebElement creditsDisplay => Driver.FindElement(By.CssSelector("div.col-md-12 > div.ng-binding"));

        private IWebElement creditsField => Driver.FindElement(By.Id("balance"));

        private IWebElement updateCreditsButton => Driver.FindElement(By.XPath("//button[contains(.,'Add licenses')]"));

        private IWebElement setCreditsButton => Driver.FindElement(By.LinkText("Add licenses"));

        internal void SetCredits(string number)
        {
            setCreditsButton.Click();

            WebDriverWait waitForElement = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));

            Thread.Sleep(1000);
            creditsField.Clear();
            creditsField.SendKeys(number);

            Thread.Sleep(500);
            updateCreditsButton.Click();
            
            Thread.Sleep(1000);
        }
    }
}